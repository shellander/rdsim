cmake_minimum_required(VERSION 2.8)

project(rdsim)

#set(CMAKE_VERBOSE_MAKEFILE on)
set(hdf5_source "${CMAKE_CURRENT_BINARY_DIR}/hdf5")
set(hdf5_install "${CMAKE_CURRENT_BINARY_DIR}/hdf5-install")
set(hdf5_version "hdf5-1.10.1")
set(JSON "${CMAKE_CURRENT_BINARY_DIR}/JSON")

include(ExternalProject)
ExternalProject_Add(HDF5
	DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}
	URL ../hdf5-1.10.1.tar.gz
	SOURCE_DIR ${hdf5_source}
    INSTALL_DIR ${hdf5_install}
    UPDATE_COMMAND ""
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=<INSTALL_DIR> --enable-cxx
    CMAKE_ARGS
    	-DHDF5_BUILD_CPP_LIB:BOOL=ON
)

ExternalProject_Add(JSON
    GIT_REPOSITORY https://github.com/nlohmann/json.git
    INSTALL_DIR ${JSON}
    CMAKE_ARGS
        -DCMAKE_INSTALL_PREFIX=${JSON} 
)


include_directories(${hdf5_install}/include)
include_directories(${JSON}/include/nlohmann)


include_directories(.)
include_directories(libs/model)
include_directories(libs/solver)
include_directories(libs/solver/wellmixed)
include_directories(libs/solver/spatial)


file(GLOB SOURCE1 "src/*.cpp")
file(GLOB SOURCE2 "libs/model/*.cpp")
file(GLOB SOURCE3 "libs/solver/*.cpp")
file(GLOB SOURCE4 "libs/solver/wellmixed/*.cpp")
file(GLOB SOURCE5 "libs/solver/spatial/*.cpp")

EXEC_PROGRAM(gsl-config ARGS --libs OUTPUT_VARIABLE GSL_LIBRARIES)
EXEC_PROGRAM(gsl-config ARGS --cflags OUTPUT_VARIABLE GSL_FLAGS)
include_directories(${GSL_LIBRARIES})



add_definitions(${GSL_FLAGS})
add_definitions("-std=c++11 -g -O3")


#${hdf5_install}/lib/libhdf5${_LINK_LIBRARY_SUFFIX}
add_executable(rdsim ${SOURCE1} ${SOURCE2} ${SOURCE3})
add_dependencies(rdsim HDF5)
add_dependencies(rdsim JSON)

if(APPLE)
	target_link_libraries( rdsim ${hdf5_install}/lib/libhdf5.a ${hdf5_install}/lib/libhdf5_cpp.a ${hdf5_install}/lib/libhdf5_hl.a ${hdf5_install}/lib/libhdf5_hl_cpp.a ${GSL_LIBRARIES} -lz -ldl -lm)
elseif(UNIX)
#	target_link_libraries( rdsim ${hdf5_install}/lib/libhdf5.a ${hdf5_install}/lib/libhdf5_cpp.a ${hdf5_install}/lib/libhdf5_hl.a ${hdf5_install}/lib/libhdf5_hl_cpp.a ${GSL_LIBRARIES} -lz -ldl -lm -lhdf5)
	target_link_libraries( rdsim -L/${hdf5_install}/lib ${hdf5_install}/lib/libhdf5_hl_cpp.a ${hdf5_install}/lib/libhdf5_cpp.a ${hdf5_install}/lib/libhdf5_hl.a ${hdf5_install}/lib/libhdf5.a -lz -ldl -lm -Wl,-rpath -Wl,${hdf5_install}/lib ${GSL_LIBRARIES})
endif()

install (TARGETS rdsim DESTINATION /usr/local/bin)

