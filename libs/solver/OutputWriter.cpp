#include "OutputWriter.h"

void OutputWriter::add_time_point_to_file(std::unordered_map<long,Particle> particles, std::unordered_map<long,Species>& specs){
    
    
    
    for (auto s=specs.begin();s!=specs.end();++s){
        
        hsize_t  dims[2];
        /* TODO: 10000 limits the number of particles to 10000.*/
        double position_buffer[10000][3];
        int ids_buffer[10000];
        int num_spec_type;
        
        
        num_spec_type=0;
        int spec_type=s->second.get_type();
//        std::cout << "spec_type=" << spec_type << std::endl;
        std::string spec_name = s->second.get_name();
        int j =0;
        for(auto p=particles.begin();p!=particles.end();++p){
            int type = p->second.get_type();
//            std::cout << "ptype=" << type << std::endl;
            if(type==spec_type){
                auto ppos = p->second.get_pos();
                position_buffer[j][0] = ppos[0];
                position_buffer[j][1] = ppos[1];
                position_buffer[j][2] = ppos[2];
                ids_buffer[j] = p->second.get_id();
                ++num_spec_type;
            }
            ++j;
        }
        dims[0] = num_spec_type;
        dims[1] = 3;
        H5::DataSpace position_dataspace = H5::DataSpace(2, dims);
        dims[1]=1;
        H5::DataSpace ids_dataspace = H5::DataSpace(2, dims);
//        std::cout << "/Trajectories/"+std::to_string(traj)+"/Type_"+spec_name+"/positions_"+std::to_string(time_index) << std::endl;
        H5::DataSet positions = file.createDataSet("/Trajectories/"+std::to_string(traj)+"/Type_"+spec_name+"/positions_"+std::to_string(time_index), H5::PredType::NATIVE_DOUBLE,position_dataspace);
        H5::DataSet ids = file.createDataSet("/Trajectories/"+std::to_string(traj)+"/Type_"+spec_name+"/unique_ids_"+std::to_string(time_index), H5::PredType::NATIVE_INT,ids_dataspace);
        positions.write(position_buffer, H5::PredType::NATIVE_DOUBLE);
        ids.write(ids_buffer, H5::PredType::NATIVE_INT);
    }
}

void OutputWriter::new_trajectory(std::unordered_map<long,Species> specs){
    ++traj;
    time_index = -1;
    //H5::Group trajectory_group = file.createGroup( "/Trajectories" );
    std::string prefix = "/Trajectories/"+std::to_string(traj);
    H5::Group trajectory_0 = file.createGroup( prefix );
    for(auto s=specs.begin();s!=specs.end();++s){
        H5::Group trajectory_0 = file.createGroup( prefix +"/Type_"+s->second.get_name());
    }
    
    
//    for (int spec=0; spec<num_specs;spec++){
//        H5::Group trajectory_0 = file.createGroup( prefix +"/Type_"+std::to_string(spec));
//    }
//    ++traj;
}

void OutputWriter::write_spatial(std::unordered_map<long,Particle> particles, std::unordered_map<long,Species> specs){
    ++time_index;
    add_time_point_to_file(particles,specs);
    
}
