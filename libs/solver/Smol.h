#ifndef RDSIM_LIBS_SOLVER_SMOL
#define RDSIM_LIBS_SOLVER_SMOL

#include "Solver.h"
#include "Mesh.h"
#include "Spatial_model.h"
#include "Id_generator.h"
#include "Smollinalg.h"
#include "Smol_rng.h"
#include "Event.h"
#include "SolverError.h"
#include "OutputWriter.h"

class Smol: public Solver{
private:
    
    
    
    /* TODO: This struct should be replaced by unordered maps id->distance. */
    
    typedef struct{
        int id;
        double d;
        double dtime;
    }neighbor;
    
    typedef struct{
        /* Id:s of particles in subset. */
		std::vector<long> p_ids;
        /* Time to molecule closest to subset. */
		double d_closest;
        /* Id of molecule closest to subset. */
		int ind_closest;
	}subset;
    
    typedef struct{
        double time;
        long reaction_id;
        int type;
        std::vector<long> pids;
    }tent_event;
    
//    double dt,shortest_d;
    
    
    double K = 20.0;
    static constexpr double pi=3.141592653589793;
    /* 'nbors' maps from a unique particle id(long) to a vector of that
     * particles neighbors(neighbor). All molecules are neighbors, and 
     * each neighbor struct contains information about the particles
     * unique id and the distance to it. This vector is not sorted.*/
    
    /* Problem here is that std::vector<neighbor> is not always in order. Need to convert it to an unordered map. */
    
//    std::unordered_map<long,std::unordered_map<long,double>> nbors;
    
    /* 'nn' maps from a unique particle id(long) to an array of length 
     * 2, holding the particles nearest neighbor, and the particles 
     * second nearest neighbor. */
//    std::unordered_map<long,std::array<long,2>> nn;
    
    /* The following functions are provided for convenience, and should
     * be more intuitive than using the above vectors directly. */
    
    typedef std::unordered_map<long,std::unordered_map<long,double>> nborsmap;
    typedef std::unordered_map<long,std::array<long,2>> nnmap;
    
    long get_nn(long pid,nborsmap& nbors,nnmap& nn){return nn.at(pid)[0];}
    double get_d_nn(long pid,nborsmap& nbors,nnmap& nn){return nbors.at(pid).at(get_nn(pid,nbors,nn));}
    long get_snn(long pid,nborsmap& nbors,nnmap& nn){return nn.at(pid)[1];}
    double get_d_snn(long pid,nborsmap& nbors,nnmap& nn){return nbors.at(pid).at(get_snn(pid,nbors,nn));}
    long get_gn(long pid,int n,nborsmap& nbors,nnmap& nn){return nn.at(pid)[n];}
    double get_d_gn(long pid,int n,nborsmap& nbors,nnmap& nn){return nbors.at(pid).at(get_gn(pid,n,nbors,nn));}
    
    
    //std::vector<std::vector<subset>> subsets;
    
    
    Spatial_model spm;
    std::unordered_map<long,Particle> particles;
    std::unordered_map<long,Particle> dead_particles;
    
    OutputWriter outw;
    
    
    
    Id_generator& idg;
    
    void randomize_cube(std::array<double,3>& pin,double width);
    
    long add_particle(std::array<double,3> pin,Species& sp);
    
    void diffuse_particle(double dt,long id);
    double dist(Particle p1,Particle p2);
    
    void sort_particles();
    void subset_d_nn(subset *subs,nborsmap& nbors,nnmap& nn);
    double sort_particles_bf2(std::vector<subset>& subsets,std::vector<long> ids);
    
    void reset(std::vector<subset>& subsets);
    
    
    
	void update_pair(int pid1,int pid2,double dt_loc,double k_r,bool reacted);
	
    double simulate_subset(subset *sbst,double dt_loc);
    
    double get_time_step(Particle p1,Particle p2);
//    void set_time_step(double dtin);
    /* Returns the n^th nearest neighbor of particle with id 'id'. n=0 or n=1. */
    long get_nn(long id,int n,nnmap& nn){return nn.at(id)[n];}
    double get_rate(tent_event rtimes);
    
    std::array<double,3> reflect_in_plane(int pid,FEM_mesh::plane pl);
    void reflect_all_planes(int pid);
    
    double get_sigma_pair(int pid1,int pid2);
    double get_D_pair(int pid1,int pid2);
    
    
public:

    double get_distance(int pid1,int pid2);
    
    std::unordered_map<long,Particle>& get_particles(){return particles;}
    void set_particles(std::unordered_map<long,Particle>& particles_in){particles = particles_in;}
    
    /* Returns tentative reaction times. Maps reaction_id->time for reaction event. */
    std::unordered_map<long,double> get_tentative_rtimes(std::vector<long> pids,double dt);
    double reaction_time(std::vector<long> pids,Reaction r,double dt);
    tent_event get_first_reaction(std::vector<long> pids,double dt,bool look_for_bimol);
	std::vector<long> doDissociation(Smol::tent_event reac);
	std::vector<long> doAssociation(Smol::tent_event reac);
	

    int num_voxels(){return spm.num_voxels();}
    unsigned int num_particles(){return particles.size();}
    
    
    std::array<double,3> meso2micro(Particle& p);
 //   void meso2micro(long id);
 //   int micro2meso(Particle& p);
    
//    Smol(Spatial_model spm_in,std::string output_file_name):spm{spm_in},idg{Id_generator::get_instance()}{
//        particles = spm_in.get_particles();
//        outw = OutputWriter(output_file_name);
//        
//    }
    
    Smol(Spatial_model spm_in,std::string output_file_name):spm{spm_in},idg{Id_generator::get_instance()}{
        particles = spm_in.get_particles();
        outw = OutputWriter(output_file_name);
    
    }
    
    Smol(Spatial_model spm_in,Id_generator& idg_in):spm{spm_in},idg{idg_in}{
        particles = spm_in.get_particles();
    }
    
    void simulate(double T,double tloc,std::vector<subset>& sbsts,std::vector<long>& pids_in);
    
    void hybrid_initialize(std::unordered_map<long,Particle>& particles_in);
    
    virtual void initialize();
    virtual void run(double T);
    virtual void print_state();
    
    void print_groups(std::vector<subset>& subsets);
    void print_avgs();
    void print_nbors(nborsmap& nbors,nnmap& nn);
};



#endif
