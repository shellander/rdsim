#include "Solver.h"

void Solver::initialize_rng(){
#ifdef __MACH__
    uint64_t seed;
    seed = mach_absolute_time();
#elif __linux__
    timespec t;
    
    clock_gettime(CLOCK_REALTIME,&t);
    
    unsigned long seed = (unsigned long)(1e9*t.tv_sec+t.tv_nsec);
    srand48(seed);
#endif
    rng = gsl_rng_alloc(gsl_rng_taus);
    gsl_rng_set(rng,seed);
}

void Solver::initialize_rng(unsigned int seed){
    rng = gsl_rng_alloc(gsl_rng_taus);
    gsl_rng_set(rng,seed);
}
