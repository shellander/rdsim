#ifndef RDSIM_LIBS_SOLVER_RDME
#define RDSIM_LIBS_SOLVER_RDME

#include "Solver.h"
#include "Id_generator.h"

#include "Spatial_model.h"

#include "Particle.h"
#include "OutputWriter.h"

#include "RDMEbinheap.h"

class RDME: public Solver{
	private:

		static constexpr double pi=3.141592653589793;
		static constexpr double C_alpha_3=1.5164;
		static constexpr double C_alpha_2=0.1951;
	
		Id_generator& idg;
		
		Spatial_model spm;
		std::unordered_map<long,Particle> particles;
		std::unordered_map<long,Particle> dead_particles;
		
		OutputWriter outw;
		
		RDMEbinheap events;
		
		bool in_same_voxel(int p1,int p2);
		
		static double G(double h,double sigma,int dim);
		
		
		void add_diff_event(long pid);
		void get_diff_events();
		void update_diss(int pid);
		void get_diss_events();
		void check_association(int pid1,int pid2);
		void get_assoc_events();
        void update_assoc(int index);
        int diffuse(int pos);
        void doDissociation(int reaction_id,int reactant_id);
        void doAssociation(int reaction_id,int reactant1_id,int reactant2_id);

        int meso_simulator(double T);
    
        std::vector<FEM_mesh::voxel> voxels;
    
    
        std::unordered_map<long,Species> specs;
        std::unordered_map<long,Reaction> reactions;
//        double t;
    
        long num_diff_events;
    
	public:
    
    
    
        RDME(Spatial_model spm_in,std::string output_file_name):spm{spm_in},idg{Id_generator::get_instance()}{
            particles = spm_in.get_particles();
            outw = OutputWriter(output_file_name);
        }
    
		RDME(Spatial_model spm_in,Id_generator& idg_in):spm{spm_in},idg{idg_in}{
            particles = spm_in.get_particles();
        }
		
        int num_voxels(){return voxels.size();}
        int num_particles(){return particles.size();}

		void print_avgs();
    
		void set_output_type(int ot){output_type=ot;}
		void set_input_type(int it){input_type=it;}
		int get_output_type(){return output_type;}
		int get_input_type(){return input_type;}
    
        long get_num_diff_events(){return num_diff_events;}
    
		void hybrid_initialize(std::unordered_map<long,Particle>& particles_in);
    
		virtual void initialize();
		virtual void run(double T);
		virtual void print_state();
		
        static double k_r_meso(double k_r,double h,double sigma,double D,int dim);
        static double k_d_meso(double k_r,double k_d,double h,double sigma,double D,int dim);
    
		std::unordered_map<long,Particle>& get_particles(){return particles;}
		void set_particles(std::unordered_map<long,Particle>& particles_in){particles = particles_in;}
    
};


#endif
