#ifndef RDSIM_LIBS_SOLVERS_SSA
#define RDSIM_LIBS_SOLVERS_SSA

#include <iostream>
#include <stdexcept>
#include <vector>
#include <unordered_map>
#include <math.h>

#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"

#include "Solver.h"
#include "Model.h"
#include "OutputWriter.h"
#include "Particle.h"
#include "Id_generator.h"

class SSA: public Solver{
private:
    
    Model m;
    
    
//    int *state;
    std::unordered_map<std::string,int> state;
    std::unordered_map<std::string,double> kmeso;
    double prop(int r_num);
    
    unsigned int nreacs,nspecs; /* Variables that hold number of reactions and species. */
    
    static constexpr double pi = 3.14159265359;
    
    double prop(Reaction r);
    
    OutputWriter outw;
    /* Only used to output in same format as spatial models. */
    Id_generator& idg;
    std::unordered_map<long,Particle> particles;
    
public:
    SSA(Model min,std::string output_file_name): m{min},idg{Id_generator::get_instance()}{
        outw = OutputWriter(output_file_name);
    }
    virtual void initialize();
    virtual void run(double T);
    virtual void print_state();
    
    
};



#endif
