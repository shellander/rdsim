#include <math.h>

#include "Smollinalg.h"

double SmolLinAlg::dot(double *v1,double *v2){
    return v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2];
}

double SmolLinAlg::dot(std::array<double,3>& v1,std::array<double,3>& v2){
    return v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2];
}

double SmolLinAlg::dist3(double *v1,double *v2){
    return sqrt(pow(v1[0]-v2[0],2)+pow(v1[1]-v2[1],2)+pow(v1[2]-v2[2],2));
}

double SmolLinAlg::dist3(std::array<double,3>& v1,std::array<double,3>& v2){
    return sqrt(pow(v1[0]-v2[0],2)+pow(v1[1]-v2[1],2)+pow(v1[2]-v2[2],2));
}

double SmolLinAlg::norm(double *v){
    return sqrt(dot(v,v));
}

double SmolLinAlg::norm(std::array<double,3> v){
	return sqrt(dot(v,v));
}

void SmolLinAlg::normalize(double *v){
    double nv = norm(v);
    v[0] = v[0]/nv;
    v[1] = v[1]/nv;
    v[2] = v[2]/nv;
}

void SmolLinAlg::normalize(std::array<double,3>& v){
    double nv = norm(v);
    v[0] = v[0]/nv;
    v[1] = v[1]/nv;
    v[2] = v[2]/nv;
}

double* SmolLinAlg::cross(double *v,double *w){
    double *out = new double[3];
    out[0] = v[1]*w[2]-v[2]*w[1];
    out[1] = -(v[0]*w[2]-v[2]*w[0]);
    out[2] = v[0]*w[1]-v[1]*w[0];
    return out;
}

/* Rotates and scales the vector r in 3D an angle theta about the axis n.
 q is the new length of the vector. */
void SmolLinAlg::rotation(double *r,double *n,double theta,double q)
{
    //    theta = 6.283185307179586-theta;
    double *v_rot = new double[3];
    normalize(n);
    double ct = cos(theta);
    double st = sin(theta);
    double temp = dot(n,r)*(1-ct);
    double *cp = cross(n,r);
    v_rot[0] = n[0]*temp+r[0]*ct+cp[0]*st;
    v_rot[1] = n[1]*temp+r[1]*ct+cp[1]*st;
    v_rot[2] = n[2]*temp+r[2]*ct+cp[2]*st;
    delete cp;
    normalize(v_rot);
    r[0] = q*v_rot[0];
    r[1] = q*v_rot[1];
    r[2] = q*v_rot[2];
    delete[] v_rot;
    
}

std::array<double,3> SmolLinAlg::vec_diff(std::array<double,3> v1,std::array<double,3> v2){
	std::array<double,3> res = {v1[0]-v2[0],v1[1]-v2[1],v1[2]-v2[2]};
	return res;
}
