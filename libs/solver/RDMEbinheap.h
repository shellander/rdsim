#ifndef RDSIM_LIBS_SOLVER_RDMEBINHEAP
#define RDSIM_LIBS_SOLVER_RDMEBINHEAP

#include <vector>
#include <string>

class RDMEbinheap{
	
	public:
		typedef struct{
			double t;
			long index;
			int type;
			std::vector<long> reactants;
		}tent_event;
	
	private:
		int first_child(int index);
		int second_child(int index);
		int parent(int index);
	    void heap_swap_elems(int index1,int index2);
		void heap_move_up(int index);
		void heap_sift_down(int index,bool (*compare)(tent_event*,tent_event*));
		void heap_insert(tent_event new_elem);
		void heap_delete(int index,bool (*compare)(tent_event*,tent_event*));
		void print_heap();
		void unroll_heap(bool (*compare)(tent_event*,tent_event*));
		static bool compare_events2 (tent_event *e1,tent_event *e2);
		std::vector <tent_event> elems;

	public:
		
	
		void add_event(tent_event te){heap_insert(te);}
		tent_event pop_event();
		int get_size(){return elems.size();}
		void clear_all(){elems.clear();}
		
		void clear_diff(int index);
		void clear_diss(int index);
		void clear_assoc(int index);
//		void adjust_index(int index);

		void print_summary();
};


#endif
