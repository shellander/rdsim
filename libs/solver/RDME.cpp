#include "RDME.h"


void RDME::hybrid_initialize(std::unordered_map<long,Particle>& particles_in){
    
//    particles = particles_in;
    set_particles(particles_in);
    
    /* TODO: This is not neccessary, as no mesoscopic molecules disappear during the microscopic step. */
//    events.clear_all();
    /* Initialize random number generator. */
    initialize_rng();
    
    voxels = spm.get_voxels();
    specs = spm.get_species();
    reactions = spm.get_reactions();
    
    t = 0.0;
    
    num_diff_events = 0;
    
    output_type = HDF5;
    debug = D_OFF;
}

void RDME::initialize(){
	
	input_type = PYURDME;
	output_type = HDF5;
	debug = D_OFF;
	
	outw.new_trajectory(spm.get_species());
	
    /* Initialize random number generator. */
    initialize_rng();
    
    voxels = spm.get_voxels();
    specs = spm.get_species();
    reactions = spm.get_reactions();
    
    /* Set timer to 0. */
    t = spm.get_init_t();
    
    num_diff_events = 0;
    
    /* Generate particles. */
    int voxin;
    long pid;
    particles.clear();
    spm.clear_particles();
    events.clear_all();
    /* Should we re-initialize particles in spatial model, or do we want
     * the same initial condition for each trajectory?
     * 
     * For now, we re-initialize.*/
    if(input_type==RDSIM){
		for(auto sp=specs.begin();sp!=specs.end();++sp){
			if(debug>=D_MED){
				std::cout << "Initializing molecules of type " << sp->second.get_name() << std::endl;
			}
			for(int i=0;i<sp->second.get_initial_value();++i){
				/* Sample initial voxel. */
				voxin = spm.sample_voxel_uniform(rng);
				pid = spm.add_particle({0.0,0.0,0.0},sp->second);
				spm.set_particle_voxel(pid,voxin);
				/* Set all particles to mesoparticles. */
				spm.set_meso_micro(Particle::meso,pid);
				if(debug==D_MAX){
					std::cout << "One Particle initialized to voxel: " << voxin << std::endl;
				}
			}
		}
	}    
    else if(input_type==PYURDME){
		std::array<double,3> pos;
		for(int i=0;i<spm.num_voxels();++i){
			//for(int j=0;j<spm.get_nspecs();++j){
            for(auto sp=specs.begin();sp!=specs.end();++sp){
                int j = sp->second.get_urdme_dof_nr();
				for(int k=0;k<spm.get_init_voxel(j,i);++k){
					pid = spm.add_particle({0.0,0.0,0.0},sp->second);
					spm.set_particle_voxel(pid,i);
					pos = spm.meso2micro(pid);
					spm.set_particle_pos(pos,pid);
					/* Set all particles to mesoparticles. */
					spm.set_meso_micro(Particle::meso,pid);
				}
			}
		}
	}
    
    particles = spm.get_particles();

	if(debug>0){
		spm.print();
	}
}

void RDME::run(double T){
	
	if(debug==D_MAX){
		std::cout << "Starting mesoscopic simulation." << std::endl;
		std::cout << "t=" << t << std::endl;
		std::cout << "T=" << T << std::endl;
	}
	
    meso_simulator(T);
    
    std::array<double,3> pos;
    for(auto p=particles.begin();p!=particles.end();++p){
		if(p->second.get_meso_micro()==Particle::meso){
            if(p->second.is_updated()){
                pos = spm.meso2micro(p->second);
//                int voxtemp = p->second.get_voxel();
//                pos = spm.get_node(voxtemp);
                
                
                particles.at(p->first).set_pos(pos);
                p->second.set_updated(false);
            }
			
			/* Note that this will overestimate the time a particles has existed,
			 * for all particles that were created during the time step. */
			particles.at(p->first).add_to_time(T);
			
		}
	}
}

void RDME::print_state(){
    //
    if(output_type==HDF5){
		outw.write_spatial(particles,spm.get_species());
	}
	else if(output_type==AVG){
		print_avgs();
	}
	else if(output_type==POSITION){
        for(auto p=particles.begin();p!=particles.end();++p){
            p->second.print();
        }
	}
}


bool RDME::in_same_voxel(int p1,int p2){

    if(particles.at(p1).get_voxel()!=particles.at(p2).get_voxel()){
		 return false;
    }
    return true;
}

void RDME::add_diff_event(long pid){
	
//	auto voxels = spm.get_voxels();
//	auto specs = spm.get_species();
	
    double totalD = voxels[particles.at(pid).get_voxel()].totalD*specs.at(particles.at(pid).get_type()).get_D();
//    std::cout << "Total D is " << voxels[particles.at(pid).get_voxel()].totalD << std::endl;
   // std::cout << "Species meso D " << specs.at(particles.at(pid).get_type()).get_meso_D() << std::endl;
    RDMEbinheap::tent_event temp_event;
    temp_event.type = -1;
    temp_event.reactants.resize(1);
    temp_event.t = -log(gsl_rng_uniform(rng))/totalD+t;
    temp_event.reactants[0] = pid;
    
    events.add_event(temp_event);
}

void RDME::get_diff_events(){
    int N = (int)(particles.size());
    for(auto p=particles.begin();p!=particles.end();++p){
        if(p->second.get_meso_micro()==Particle::meso){
			if(debug==D_MAX){
				std::cout << "Adding diffusion event. pid=" << p->first << std::endl;
			}
            add_diff_event(p->first);
        }
    }
}

double RDME::G(double h,double sigma,int dim){
    if(dim==3){
        return C_alpha_3/(6.0*h)-1.0/(4*pi*sigma);
    }
    else if(dim==2){
        return 0.25*(3/(2*pi)+C_alpha_2)-1/(2*pi)*log(1/sqrt(pi)*h/sigma);
    }
    return -1;
}

double RDME::k_r_meso(double k_r,double h,double sigma,double D,int dim){
    return k_r/pow(h,dim)*pow(1-k_r/D*G(h,sigma,dim),-1);
}

double RDME::k_d_meso(double k_r,double k_d,double h,double sigma,double D,int dim){
    return pow(h,dim)*k_d*k_r_meso(k_r,h,sigma,D,dim)/k_r;
}


void RDME::update_diss(int pid){

//	auto voxels = spm.get_voxels();
//	auto specs = spm.get_species();
//	auto reactions = spm.get_reactions();

    //int M = (int)(dissociations.size());
	RDMEbinheap::tent_event temp_event;
    temp_event.type = 0;
    double k = 0.0,ktemp=0.0;
    
    /* TODO: Getting the mesoscopic rate should be a function that can be used by many classes. */
    /* This needs to be done quite often. */
    
    
    for(auto reac=reactions.begin();reac!=reactions.end();++reac){
		Reaction r=reac->second;
		int rid = reac->first;
		
		if(r.get_type()==Reaction::unimol){
			temp_event.index = rid;
			auto reactants = r.get_reactants();
			if(reactants[0].get_type()==particles.at(pid).get_type()){
			
				k = r.get_rate();//TODO: This returns microscopic rate. */
				/* For now assume that HHP rates are good enough. */
				auto products = r.get_products();
				
				if(products.size()>1 && spm.reversible(r)){
					// **************************************
					// Compute HHP rates for bimol reaction *
					// Preprocess step instead?             * 
					// **************************************
					
					double htemp = pow(voxels.at(particles.at(pid).get_voxel()).vol,1.0/3.0);
					double sigmatemp = specs.at(products[0].get_type()).get_sigma()+specs.at(products[1].get_type()).get_sigma();
					double Dtemp = specs.at(products[0].get_type()).get_D()+specs.at(products[1].get_type()).get_D();
					
					double krev = spm.get_rev_rate(r);
					if(krev>0.0){
						k = k_d_meso(krev,k,htemp,sigmatemp,Dtemp,specs.at(products[0].get_type()).get_dim());
					}
					
					if(debug==D_MAX){
						std::cout << "Micro assoc. rate is " << krev << std::endl;
					}
					// **************************************
					// **************************************
				}
				if(debug==D_MAX){
					std::cout << "Meso dissociation rate is " << k << std::endl;
				}
				
				temp_event.t = -log(gsl_rng_uniform(rng))/k+t;
				if(debug==D_MAX){
					std::cout << "Sampled time is " << temp_event.t << ", t=" << t << std::endl;
				}
				temp_event.reactants.push_back(pid);
                events.add_event(temp_event);

			}
				
		}
		
	}
}

void RDME::get_diss_events(){
	
//	auto voxels = spm.get_voxels();
//	auto reactions = spm.get_reactions();
	
	RDMEbinheap::tent_event temp_event;
	temp_event.type = 0;
    temp_event.reactants.resize(1);
	for(auto p=particles.begin();p!=particles.end();++p){
		if(p->second.get_meso_micro()==Particle::meso){
			update_diss(p->first);
		}
	}
}

void RDME::check_association(int pid1,int pid2){

//	auto reactions = spm.get_reactions();
//	auto specs = spm.get_species();
//	auto voxels = spm.get_voxels();
    
    
    RDMEbinheap::tent_event temp_event;
    temp_event.type = 1;
    temp_event.reactants.resize(2);
    
    
    double rrate = 0.0;
    
    for(auto r=reactions.begin();r!=reactions.end();++r){
		
		long rid = r->first;
		Reaction rtemp = r->second;
		if(rtemp.get_type()==Reaction::bimol){
			auto reactants = rtemp.get_reactants();
			if(((reactants.at(0).get_type()==particles.at(pid1).get_type() && reactants.at(1).get_type()==particles.at(pid2).get_type()) || (reactants.at(1).get_type()==particles.at(pid1).get_type() && reactants.at(0).get_type()==particles.at(pid2).get_type()))){

				/* TODO: We need to give the reaction id here. */
				temp_event.index = rid;
				temp_event.reactants[0] = pid1;
				temp_event.reactants[1] = pid2;
				
				// **************************************
				// Compute HHP rates for bimol reaction *
				// Preprocess step instead?             * 
				// **************************************
				
				auto reactants = rtemp.get_reactants();
				double htemp = pow(voxels.at(particles.at(pid1).get_voxel()).vol,1.0/3.0);
                
				//std::cout << "Products size; " << products.size() << std::endl;
				//std::cout << "Type1: " << products[0].get_type() << ", Type2: " << products[1].get_type() << std::endl;
				double sigmatemp = specs.at(reactants[0].get_type()).get_sigma()+specs.at(reactants[1].get_type()).get_sigma();
				double Dtemp = specs.at(reactants[0].get_type()).get_D()+specs.at(reactants[1].get_type()).get_D();
				
				double rrate = k_r_meso(rtemp.get_rate(),htemp,sigmatemp,Dtemp,particles.at(pid1).get_dim());
				//std::cout << "sigma=" << sigmatemp << ", D=" << Dtemp << std::endl;
				// **************************************
				// **************************************
				
				temp_event.t = -log(gsl_rng_uniform(rng))/rrate+t;
				events.add_event(temp_event);
				if(debug==D_MAX){
					std::cout << "Adding association event." << std::endl;
					std::cout << "Bimol rate is " << rrate << std::endl;
                    std::cout << "Volume of voxel: " << voxels.at(particles.at(pid1).get_voxel()).vol << std::endl;
					std::cout << "Time sampled is " << temp_event.t << ", t=" << t << std::endl;
				}
			}
		}
    }
}

void RDME::get_assoc_events(){

    int N = (int)(particles.size());
    
    int vox_id;
    
    
    int dist_temp;
    
    for(auto p=particles.begin();p!=particles.end();++p){
    //for(int i=0;i<N;i++){
        auto ptemp = p->second;
        int pid = p->first;
        vox_id = ptemp.get_voxel();
        
        
        //for(int j=i+1;j<N;j++){
        for(auto p2=next(p);p2!=particles.end();++p2){
            dist_temp = 2;
            auto p2temp = p2->second;
            int pid2 = p2->first;
            if(p2temp.get_voxel()==vox_id){
                dist_temp = 0;
            }
            if(dist_temp==0 && (ptemp.get_meso_micro()==Particle::meso || p2temp.get_meso_micro()==Particle::meso)){
                check_association(pid,pid2);
            }
        }
    }
}

void RDME::update_assoc(int pid){
    
    int vox_id;
    
//    int dist_temp = 0;
    
    vox_id = particles.at(pid).get_voxel();
    
    for(auto p=particles.begin();p!=particles.end();++p){
//        int pid2 = p->first;
//        if(pid2!=pid){
            //Get associations
//            dist_temp = 2;
            if(p->second.get_voxel()==vox_id && p->first != pid){
//                dist_temp = 0;
                check_association(pid,p->first);
            }
//            if(dist_temp==0){//&& (particles.at(pid).get_meso_micro()==Particle::meso || particles.at(pid2).get_meso_micro()==Particle::meso)
//                check_association(pid2,pid);
//            }
//        }
    }
}

int RDME::diffuse(int pos){
    
//    auto voxels = spm.get_voxels();
    
    double totalD = voxels[pos].totalD;
    
    double temp = voxels[pos].neighbors[0].D;
    double rtemp = gsl_rng_uniform(rng);
    int i = 0;
	/*std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
    std::cout << "Main voxel: " << pos << std::endl;
    std::cout << "neighbors: ";*/
    while(rtemp>temp/totalD){
        ++i;
        temp += voxels[pos].neighbors[i].D;
		//std::cout << voxels[pos].neighbors[i].vox << ", ";
    }
    //std::cout << " nn=" << voxels[pos].neighbors.size() << std::endl;
    //std::cout << std::endl << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
    return voxels[pos].neighbors[i].vox;
}

void RDME::doDissociation(int reaction_id,int reactant_id){
 //   std::cout << "Doing dissociation..." << std::endl;
    
//    auto voxels = spm.get_voxels();
//    auto specs = spm.get_species();
//    auto reactions = spm.get_reactions();
    
    auto r = reactions.at(reaction_id);
    auto products = r.get_products();
    int nprods = r.get_nprods();
    
    int voxid = particles.at(reactant_id).get_voxel();
    dead_particles.insert({reactant_id,particles.at(reactant_id)});
    particles.erase(reactant_id);
    
    events.clear_assoc(reactant_id);
    //std::cerr << "Stack before " << events.get_size() << std::endl;
    events.clear_diff(reactant_id);
   // std::cerr << "Stack after " << events.get_size() << std::endl;
    events.clear_diss(reactant_id);
//    events.adjust_index(reactant_id);
    
    int newid;

    for(int i=0;i<nprods;++i){
        Particle new_part{idg};
        new_part.set_clock(-t);
        new_part.set_meso_micro(Particle::meso);
        new_part.set_type(products[i].get_type());
        new_part.set_name(products[i].get_name());
        
        newid = new_part.get_id();
        new_part.set_voxel(voxid);
        particles.insert({newid,new_part});
        
//
        update_assoc(newid);
        update_diss(newid);
      //  std::cerr << "Adding diff event for id " << newid << std::endl;
     //    std::cerr << "Stack before " << events.get_size() << std::endl;
        add_diff_event(newid);
   //      std::cerr << "Stack after " << events.get_size() << std::endl;
    }
}

void RDME::doAssociation(int reaction_id,int reactant1_id,int reactant2_id){
	if(debug>=D_MED){
		std::cout << "Doing association..." << std::endl;
	}
    auto reactions = spm.get_reactions();
    
    int voxid = particles.at(reactant1_id).get_voxel();
    dead_particles.insert({reactant1_id,particles.at(reactant1_id)});
    dead_particles.insert({reactant2_id,particles.at(reactant2_id)});
    particles.erase(reactant1_id);
    particles.erase(reactant2_id);
    events.clear_assoc(reactant1_id);
    events.clear_assoc(reactant2_id);
    events.clear_diff(reactant1_id);
    events.clear_diff(reactant2_id);
    events.clear_diss(reactant1_id);
    events.clear_diss(reactant2_id);
    
    auto products = reactions.at(reaction_id).get_products();
    
    int newid;
    for(auto p=products.begin();p!=products.end();++p){
        Particle new_part{idg};
        new_part.set_voxel(voxid);
        new_part.set_clock(-t);
        new_part.set_meso_micro(Particle::meso);
        new_part.set_type(p->get_type());
        new_part.set_name(p->get_name());
        newid = new_part.get_id();
        particles.insert({newid,new_part});
        update_assoc(newid);
        update_diss(newid);
   //      std::cerr << "Stack before " << events.get_size() << std::endl;
  //      std::cerr << "Adding diff event for id " << newid << std::endl;
  //       std::cerr << "Stack after " << events.get_size() << std::endl;
        add_diff_event(newid);
    }
}

int RDME::meso_simulator(double T){
    
//    auto specs = spm.get_species();
//    auto reactions = spm.get_reactions();
//    auto voxels = spm.get_voxels();
    
    
    //First check that we have some mesoscopic particles.
    int MESO=0;
 
    for(auto p=particles.begin();p!=particles.end();++p){
		if(p->second.get_meso_micro()==Particle::meso){
			++MESO;
			break;
		}
	}
    if(MESO==0){
	/* TODO: We could have creation of mesoscopic molecules, even if
	 * we have zero mesoscopic molecules here.*/
		if(debug>=D_MED){
			std::cout << "No mesoscopic molecules. Done..." << std::endl;
		}
        return -1;
    }
    
    /* TODO: Is there a better way to do this? Clearing all events probably only makes
     * sense for the hybrid method? No reason to do this for purely mesoscopic 
     * simulations. */
    events.clear_all();
    
    if(debug>=D_MED){
		std::cout << "*******BEFORE*********" << std::endl;
		events.print_summary();
		std::cout << "Number of particles: " << particles.size() << std::endl;
		
	}
    get_diff_events();
    get_assoc_events();
    get_diss_events();
    if(debug>=D_MED){
		std::cout << "*******AFTER**********" << std::endl;
		events.print_summary();
		std::cout << "Number of particles: " << particles.size() << std::endl;
		std::cout << "************************" << std::endl;
	}
    
    double t_next_event;
    int type_next_event;
    int reactant1,reactant2;
    int new_pos;
    
    RDMEbinheap::tent_event next_event;
    
    while(t<T){
		
		if(debug==D_MAX){
			std::cout << "************************" << std::endl;
			events.print_summary();
			std::cout << "Number of particles: " << particles.size() << std::endl;
			std::cout << "************************" << std::endl;
		}
		
        //Get time for next event.
        next_event = events.pop_event();
        t_next_event = next_event.t;
        t = t_next_event;
        if(t_next_event>T){
            break;
        }
        type_next_event = next_event.type;
        reactant1 = next_event.reactants[0];

//		std::cerr << "Size of stack: " << events.get_size() << std::endl;

//        std::cout << "Next event is " << type_next_event << std::endl;
//        std::cout << "At time " << t_next_event << std::endl;
        
        if(type_next_event==-1){
            
            ++num_diff_events;
            
            //Next event diffusion.
          /*  auto test_exists = particles.find(reactant1);
            if(test_exists==particles.end()){
				std::cout << "can't find particle with id " << reactant1 << std::endl;
			}*/
			try{
				auto tp = particles.at(reactant1);
				new_pos = diffuse(tp.get_voxel());
				
			}catch(const std::out_of_range& oor){
		//		std::cerr << "Out of Range error: " << oor.what() << '\n';
	//			std::cerr << "can't find particle with id " << reactant1 << std::endl;
//std::cerr << "Time: " << t_next_event << std::endl;
	//			std::cerr << "Size of stack: " << events.get_size() << std::endl;
				dead_particles.at(reactant1).print();
				exit(0);
			}
			
			
//			int vn = particles.at(reactant1).get_voxel();
			
			/*std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
			
			std::cout << "Node before: " << voxels[vn].node[0] << "," << voxels[vn].node[1] << "," << voxels[vn].node[2] << std::endl;
			std::cout << "Voxel before: " << vn << std::endl;
            */
            particles.at(reactant1).set_voxel(new_pos);
			
//			vn = new_pos;
			
			/*std::cout << "Voxel after: " << vn << std::endl;
			std::cout << "Node after: " << voxels[vn].node[0] << "," << voxels[vn].node[1] << "," << voxels[vn].node[2] << std::endl;
			std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
*/
            events.clear_assoc(reactant1);
            update_assoc(reactant1);
            
//            events.clear_diss(reactant1);
//            update_diss(reactant1);
            
            if(debug==D_MAX){
				std::cerr << "Adding diff event for id " << reactant1 << std::endl;
				std::cerr << "Stack size before " << events.get_size() << std::endl;
			}
            add_diff_event(reactant1);
            if(debug==D_MAX){
				std::cerr << "Stack size after " << events.get_size() << std::endl;
			}
      
      
			t = t_next_event;
        }
        else if(type_next_event==0){
			//Next event unimolecular.
//            t = t_next_event;
            doDissociation(next_event.index,reactant1);
        }
        else{
			//Next event bimolecular.
            reactant2 = next_event.reactants[1];
//            t = t_next_event;
            doAssociation(next_event.index,reactant1,reactant2);
        }
        
        
    }
    //print_avgs();
    return 1;
}

void RDME::print_avgs(){
    /* TODO: This function is not safe (can we guarantee that species types will be integers starting at 0? I don't think so.) */
//    auto specs = spm.get_species();
    int nspecs = specs.size();
//    for(auto sp=specs.begin();sp!=specs.end();++sp){
//        std::cout << sp->second.get_name() << "   ";
//    }
//    std::cout << std::endl;
    int *nmols = (int *)calloc(nspecs,sizeof(int));
    for(auto p=particles.begin();p!=particles.end();++p){
        ++nmols[p->second.get_type()];
    }
    for(int j=0;j<nspecs;++j){
        std::cout << nmols[j] << " ";
    }
    std::cout << std::endl;
    free(nmols);
    
}

