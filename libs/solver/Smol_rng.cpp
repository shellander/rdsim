#include <iostream>

#include "Smol_rng.h"


double Smol_rng::gen_ran_theta(double D,double t,double r, const int N,gsl_rng *rng)
{
	
	double rand_r = gsl_rng_uniform(rng);
	double dtr = D*t/(r*r);
	int M = round(sqrt(8/dtr));
	if(M==0)
	{
		M = 1;
	}
	/*
	 if(M>10000)
	 {
	 printf("M = %d\n",M);
	 }
	 */
	double *theta = (double *)malloc(N*sizeof(double));
	double right = (15*sqrt(2*D*t))/(pi*r);//sqrt(2*D*t/(r_0*r_0))/(pi*r_0*r_0);
	if(right>pi)
		right = pi;
	double h = right/N;
	for(int i = 0;i<N;i++)
	{
		theta[i] = (i+1)*h;
	}
	//double Dt = D*t;
	
	
	//double sum = 0.0;
	//double sumprev = sum;
	
	double *E1 = (double *)malloc(M*sizeof(double));
	double *T = (double *)malloc(M*sizeof(double));
	
	double cons;
	double cdf = 0;
	double cdf_prev;
	for(int i = 0;i<M;i++)
	{
		E1[i] = exp(-i*(i+1)*dtr)*(2*i+1);
	}
	
	double res,res_prev;
	
	int j = 0;
	res = 0.0;
	cons = 0.25*h*sin(theta[j]);
	/*for(int i = 0;i<M;i++)
	 {
	 res += gsl_sf_legendre_Pl(i,cos(theta[j]))*E1[i];
	 }
	 res *= cons;*/
	
	for(j = 1;j<N;j++)
	{
		res_prev = res;
		res = 0.0;
		cons = 0.25*h*sin(theta[j]);
		
		gsl_sf_legendre_Pl_array(M-1,cos(theta[j]),T);
		for(int i = 0;i<M;i++)
		{
			res += T[i]*E1[i];
		}
		res *= cons;
		cdf_prev = cdf;
		cdf += res_prev+res;
		if(cdf>rand_r)
		{
			free(theta);
			free(E1);
			free(T);
			return h*(j-1)+(cdf-rand_r)/(cdf-cdf_prev)*h;
		}
	}
	free(theta);
	free(E1);
	free(T);
	return right;
	return h*(j-1)+(cdf-rand_r)/(cdf-cdf_prev)*h;
	return right-h+(rand_r-cdf)/(cdf-cdf_prev)*h;
}


std::array<double,3> Smol_rng::random_sphere(gsl_rng *rng,double r,int dimension){
	
    std::array<double,3> rand;
    if(dimension==3){
        double r1 = gsl_ran_gaussian_ziggurat(rng,1.0);
        double r2 = gsl_ran_gaussian_ziggurat(rng,1.0);
        double r3 = gsl_ran_gaussian_ziggurat(rng,1.0);
        double norm = 1/sqrt(r1*r1+r2*r2+r3*r3);
        rand[0] = r*norm*r1;
        rand[1] = r*norm*r2;
        rand[2] = r*norm*r3;

    }
    else{
        double r1 = 2*gsl_rng_uniform(rng)-1;
        double r2 = 2*gsl_rng_uniform(rng)-1;
        double p2 = r1*r1+r2*r2;
        while(p2>=1){
            r1 = 2*gsl_rng_uniform(rng)-1;
            r2 = 2*gsl_rng_uniform(rng)-1;
            p2 = r1*r1+r2*r2;
        }
        
        rand[0] = r*(r1*r1-r2*r2)/p2;
        rand[1] = r*2*r1*r2/p2;
        rand[2] = 0.0;
    }
    return rand;
}


double Smol_rng::W2_real(double a,double b)
{
	
	if(a+b>4)
	{
		//printf("a=%10g,b=%g\n",a,b);
		double x = a+b;
		double temp = exp(-a*a)/(x*sqrt(pi));
		double sum = 0;
		int i = 1;
		double term = 0;
        int NT = round(pow(a+b,2)-0.5);
        if(NT>4){
            NT = 5;
        }
		while(i<NT)
		{
            // printf("q = %.10g\n",fabs(term/sum));
            // printf("sum=%.10g\n",sum);
            //printf("i=%i\n",i);
			term = pow(-1,i)*gsl_sf_doublefact(2*i-1)/(pow(2*x*x,i));
			sum += term;
			i++;
		}
		return temp*(1+sum);
	}
	else
	{
        ////     //   printf(":::\n");
		double res = exp(2*a*b+b*b)*erfc(a+b);
		return res;
	}
}


double Smol_rng::cdf_exact_refl(double t,double r_0,double r,double sigma,double D)
{
	double term1 = -(sqrt(D*t))/(r_0*sqrt(pi))*(exp(-pow(r-r_0,2)/(4*D*t))-
                                                exp(-pow(r+r_0-2*sigma,2)/(4*D*t)))+
    0.5*(erf((r-r_0)/sqrt(4*D*t))+erf((r+r_0-2*sigma)/sqrt(4*D*t)));
	
	
	double alpha_irr = sqrt(D)/sigma;
	double K = (r+r_0-2*sigma)/sqrt(4*D*t);
	double term2 = 1/r_0*(r-sqrt(D)/alpha_irr)*W2_real(K,alpha_irr*sqrt(t));
	return term1-term2;
}


double Smol_rng::p_irr(double t,double r_0,double r,double k_r,double sigma,double D){
    double a = 1.0/(4*pi*r*r_0*sqrt(D));
    double b = 1.0/sqrt(4*pi*t);
    double k_D = 4*pi*sigma*D;
    double alpha_irr = (1+k_r/k_D)*sqrt(D)/sigma;
    double arg1 = -pow((r-r_0),2)/(4*D*t);
    double arg2 = -pow((r+r_0-2*sigma),2)/(4*D*t);
    double p_irr = b*(exp(arg1)+exp(arg2))-alpha_irr*W2_real((r+r_0-2*sigma)/sqrt(4*D*t),alpha_irr*sqrt(t));
    return a*p_irr;
}

double Smol_rng::p_irr_helper(double h,void *params){
	double *temp = (double *)params;
    
	return 4*pi*h*h*p_irr(temp[0],temp[1],h,temp[2],temp[3],temp[4]);
}

double Smol_rng::cdf_exact_irr(double t,double r_0,double r,double k_r,
                     double sigma,double D)
{
	double term1 = -(sqrt(D*t))/(r_0*sqrt(pi))*(exp(-pow(r-r_0,2)/(4*D*t))-exp(-pow(r+r_0-2*sigma,2)/(4*D*t)))+0.5*(erf((r-r_0)/sqrt(4*D*t))+erf((r+r_0-2*sigma)/sqrt(4*D*t)));
	//printf("term1_irr = %.15f\n",term1);
	double k_D = 4*pi*sigma*D;
	double alpha_irr = (1+k_r/k_D)*sqrt(D)/sigma;
	//printf("alpha_irr = %.15f\n",alpha_irr);
	double K = (r+r_0-2*sigma)/sqrt(4*D*t);
	
	//double w2_test = W2_real(K,alpha_irr*sqrt(t));
	//complex temp = Complex(alpha_irr*sqrt(t),0.0);
	//complex w2_test2 = W2(K,&temp);
	//printf("W2_real = %.15f	 W2_complex = %.15f\n",w2_test,w2_test2.real);
	double term2 = 1/r_0*(r-sqrt(D)/alpha_irr)*W2_real(K,alpha_irr*sqrt(t));
	double term3 = k_r/(4*pi*r_0*sigma*sqrt(D))*(-1.0/alpha_irr*W2_real((r_0-sigma)/sqrt(4*D*t),alpha_irr*sqrt(t)));
    //	printf("term2_irr = %.15f\n",term2);
	return term1-term2-term3;
}


double Smol_rng::cdf_irr(double t,double r_0,double r,double k_r,double sigma,double D){
    gsl_set_error_handler_off();
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
	double result, error;
  	double params[5] = {t,r_0,k_r,sigma,D};
	gsl_function F;
  	F.function = &p_irr_helper;
  	F.params = &params;
  	gsl_integration_qag(&F, sigma, r, 0, 1e-4, 1000,6,w, &result, &error);
    gsl_integration_workspace_free (w);
  	//printf("result=%.10g, error=%.10g\n",result,error);
  	return result;
}

double Smol_rng::survivalA_irr(double t,double r_0,double k_r,double sigma,double D)
{
	double k_D = 4*pi*sigma*D;
	double alpha_irr = (1+k_r/k_D)*sqrt(D)/sigma;
	//printf("x=%.15g\n",(r_0-sigma)/sqrt(4*D*t));
    double erfc_term = 0.0;
    if((r_0-sigma)/sqrt(4*D*t)>4){
        double x = (r_0-sigma)/sqrt(4*D*t);
        double sum = 0;
		int i = 1;
		double term = 0;
        int NT = round(pow(x,2)-0.5);
        if(NT>3){
            NT = 4;
        }
//        if(NT>2){
//            NT=3;
//        }
        while(i<NT)
		{
            // printf("q = %.10g\n",fabs(term/sum));
            // printf("sum=%.10g\n",sum);
            //printf("i=%i\n",i);
			term = pow(-1,i)*gsl_sf_doublefact(2*i-1)/(pow(2*x*x,i));
			sum += term;
			i++;
		}
        erfc_term = exp(-x*x)/(x*sqrt(pi))*(1+sum);
    }
    else{
        erfc_term = erfc((r_0-sigma)/sqrt(4*D*t));
    }
    
	double c1 = erfc_term-W2_real((r_0-sigma)/sqrt(4*D*t),sqrt(t)*alpha_irr);
	return 1-(sigma/r_0)*(k_r/(k_r+k_D))*c1;
}

double Smol_rng::survivalA_1D(double x_0,double k_r,double k_d,double t,double D)
{
	double K = x_0/sqrt(4*D*t);
	double Delta = sqrt(k_r*k_r-4*D*k_d);
	double lambda_plus = (k_r+Delta)/2.0;
	double lambda_minus = (k_r-Delta)/2.0;
	double sqdt = sqrt(t/D);
	double B_minus = lambda_minus*sqdt;
	double B_plus  = lambda_plus*sqdt;
	double k1=W2_real(K,B_minus);
	double k2=W2_real(K,B_plus);
	//printf("k1: %.16f k2: %.16f\n",k1,k2);
	//(W2_real(K,B_minus)-W2_real(K,B_plus))
	double temp = k_r/Delta*(k1-k2);
	if(k_r<1e-30)
	{
		temp = 0.0;
	}
	if(1-temp<0)
		printf("S<0\n");
	return 1-temp;
}

double Smol_rng::random_time(double endt,double r_0,double k_r,double sigma,double D, gsl_rng *rn, int dimension)
{
	
	
	//std::cout << "r_0=" << r_0 << ", kr=" << k_r << std::endl;
	
	int J = 1000;
	int i = 0;
	double TOL = 1e-13;
	double Sexact = 0.0;
	if(dimension == 3)
	{
		Sexact = survivalA_irr(endt,r_0,k_r,sigma,D);//cdf_irr(endt,r_0,r_0+10*sqrt(2*D*endt),k_r,sigma,D);//
        
        
//        if(fabs(sigma-r_0)<0.0001){
//            std::cout << "S_exact=" << Sexact << std::endl;
//        }
        
		/*if(k_r < 1e-30)
		 {
		 printf("Sexact = %.15f\n",Sexact);
		 }*/
	}
	else if(dimension == 1)
	{
		Sexact = survivalA_1D(r_0,k_r,0.0,endt,D);
	}
	else
	{
		printf("Invalid dimension.");
	}
	double r = gsl_rng_uniform(rn);
	if(r>(1-Sexact))
	{
		return endt+1;
	}
	double T = 0.0;
	
	double h = endt/4.0;
	double tt = endt/2.0;
	double ttprev = tt;
	
	double T_prev;
	
	
	while(fabs(r-(1.0-T))>TOL && i<J)
	{
		ttprev = tt;
		T_prev = T;
		if(dimension == 3)
			T = survivalA_irr(tt,r_0,k_r,sigma,D);//cdf_irr(tt,r_0,r_0+10*sqrt(2*D*tt),k_r,sigma,D);//
		else if(dimension == 1)
			T = survivalA_1D(r_0,k_r,0.0,tt,D);
		if(r-(1.0-T)<0)
		{
			tt -= h;
		}
		else
		{
			tt += h;
		}
		h = h/2.0;
		i++;
	}
	if(tt<(1e-10))
	{
		return 1e-10;
	}
	return tt;
	
	
	if(i==J)
	{
		return tt+(r-Sexact-T)/(T-T_prev)*h;
		return (tt+ttprev)/2.0;
	}
	
	return ttprev+h*((r-Sexact)-T_prev)/(T-T_prev);
}

double Smol_rng::random_r_irr(double t,double r_0,double k_r,double sigma,
                              double D,gsl_rng *rn)
{
	double r;
	double R=0.0;
	double leftB,rightB;
	double TOL;
	int i;
	double surv_exact;
	double integ,F;
	
	
	leftB = sigma;
    rightB = r_0+7*sqrt(6*D*t);//endr;
	i = 0;
	TOL = 1e-10;//sigma/1e6;
	if(k_r<(1e-30))
	{
		surv_exact = 1;
	}
	else
	{
        //	surv_exact = cdf_irr(t,r_0,rightB,k_r,sigma,D);//
        
        surv_exact = survivalA_irr(t,r_0,k_r,sigma,D);
        /*  if(surv_exact<0.9){
         printf("surv_exact=%.10g, test=%.10g\n",surv_exact,test);
         }*/
        
	}
	r = gsl_rng_uniform(rn)*surv_exact;
	int counter = 0;
	while(fabs(rightB-leftB)/rightB>TOL && i<150)
	{
		R = (leftB+rightB)/2.0;
		if(k_r>(1e-30))
		{
            //SHOULD TRY THIS WITH CDF_IRR!!!
            
            
			integ = cdf_irr(t,r_0,R,k_r,sigma,D);
//            integ = cdf_exact_irr(t,r_0,R,k_r,sigma,D);
//            printf("integ=%.10g, exact=%.10g\n",integ,integ2);
            //printf("surv_exact=%.5g, integ=%.10g,  r=%.10g, iter=%d\n",surv_exact,integ,r,counter);
            //   printf("r_0=%.5g, r_new=%.5g\n",r_0,R);
		}
		else
		{
            //This case does not apply here.
			integ = cdf_exact_refl(t,r_0,R,sigma,D);//-(1-surv_exact);
            
		}
        counter++;
		F = integ;//-(1-surv_exact);
		
		//Intervall-halvering....
		if(F>r)
		{
			rightB = R;
		}
		else
		{
			leftB = R;
		}
		i++;
	}
	return R;
	
}
