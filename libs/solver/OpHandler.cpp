#include "OpHandler.h"

void OpHandler::redirect_std_out(std::string filename){
    std::ofstream out(filename);
    coutbuf = std::cout.rdbuf(); //save old buf
    std::cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
}

void OpHandler::flush_stream(){
    std::cout.flush();
}

void OpHandler::reset_std_out(){
    std::cout.rdbuf(coutbuf);
}
