#ifndef RDSIM_LIBS_SOLVERS_SMOL_RNG
#define RDSIM_LIBS_SOLVERS_SMOL_RNG

#include <math.h>
#include <array>

#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"

#include "gsl/gsl_sf_legendre.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_linalg.h"
#include <gsl/gsl_integration.h>
#include "gsl/gsl_poly.h"
#include "gsl/gsl_complex.h"
#include "gsl/gsl_complex_math.h"
#include "gsl/gsl_sf_gamma.h"

#include "Smollinalg.h"

class Smol_rng{
	private:
		static double W2_real(double a,double b);
		static double cdf_exact_refl(double t,double r_0,double r,double sigma,double D);
		static double p_irr(double t,double r_0,double r,double k_r,double sigma,double D);
		static double p_irr_helper(double h,void *params);
		static double cdf_exact_irr(double t,double r_0,double r,double k_r,double sigma,double D);
        static double cdf_irr(double t,double r_0,double r,double k_r,double sigma,double D);
        static double survivalA_irr(double t,double r_0,double k_r,double sigma,double D);
        static double survivalA_1D(double x_0,double k_r,double k_d,double t,double D);
        static constexpr double pi=3.141592653589793;
	public:
		
		static double gen_ran_theta(double D,double t,double r, const int N,gsl_rng *rng);
        static std::array<double,3> random_sphere(gsl_rng *rng,double r,int dimension);
		static double random_time(double endt,double r_0,double k_r,double sigma,double D, gsl_rng *rn, int dimension);
		static double random_r_irr(double t,double r_0,double k_r,double sigma,double D,gsl_rng *rn);
};


#endif
