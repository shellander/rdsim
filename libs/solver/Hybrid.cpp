#include "Hybrid.h"

void Hybrid::initialize(){
    input_type = PYURDME;
    output_type = HDF5;
    debug = D_OFF;
    
    outw.new_trajectory(spm.get_species());
    
    t = 0.0;
    
    /* Initialize random number generator. */
    initialize_rng();
    
    int Nvox,vox_num;
    Nvox = spm.num_voxels();
    double tot_vol = spm.get_volume();
    
    double pos_temp[3];
    /* Set up initial condition. First add particles to model's list of particles, then copy list to solver's list of particles.*/
    std::array<double,3> pin;
    
    spm.clear_particles();
    particles.clear();
    
    reacs = spm.get_reactions();
    specs = spm.get_species();
    
    
    if(input_type==RDSIM){
        for(auto sp=specs.begin();sp!=specs.end();++sp){
            for(int i=0;i<sp->second.get_initial_value();++i){
                pin = spm.sample_mesh_uniform(rng,sp->second.get_dim());
                
                /* Now add the molecule. */
                int pid = spm.add_particle(pin,sp->second);
                pos_temp[0] = pin[0];
                pos_temp[1] = pin[1];
                pos_temp[2] = pin[2];
                int voxel = spm.micro2meso(pos_temp,0);
                spm.set_particle_voxel(pid,voxel);
                
                /* Set particles to meso or micro. */
                spm.set_meso_micro(Particle::meso,pid);
                
            }
        }
    }
    else if(input_type==PYURDME){
        std::array<double,3> pos;
        for(int i=0;i<spm.num_voxels();++i){
            for(auto sp=specs.begin();sp!=specs.end();++sp){
                int j = sp->second.get_urdme_dof_nr();
                for(int k=0;k<spm.get_init_voxel(j,i);++k){
                    int pid = spm.add_particle({0.0,0.0,0.0},sp->second);
                    
                    // Hard coded for now.
                   pos = spm.sample_mesh_uniform(rng,sp->second.get_dim());
                    pos_temp[0] = pos[0];
                    pos_temp[1] = pos[1];
                    pos_temp[2] = pos[2];
                    int voxel = spm.micro2meso(pos_temp,0);
                    spm.set_particle_voxel(pid,voxel);
                    // *******************
                    
                    /* Temporarily commented out. */
//                    spm.set_particle_voxel(pid,i);
//                    pos = spm.meso2micro(pid);
                    spm.set_particle_pos(pos,pid);
                    /* ************************** */
                    
                    /* Set particles to meso or micro. */
                    spm.set_meso_micro(Particle::meso,pid);
                }
            }
        }
    }
    particles = spm.get_particles();
    
    
    /* Compute W. */
    
    for(auto r=reacs.begin();r!=reacs.end();++r){
        /* TODO: Hard coded dimension here. */
        double Wtemp = W(r->first,3,spm.num_voxels());
//        std::cout << "W for reaction " << r->second.get_name() << "=" << Wtemp << std::endl;
        if(Wtemp>=epsilon){
//            std::cout << "Reaction diffusion-limited." << std::endl;
            find_matching_reactions(r->second,micro_particles);
        }
    }
    
    // std::cout << "Microscopic particles: " << std::endl;
    // for(int i=0;i<micro_particles.size();++i){
    //     std::cout << micro_particles[i] << std::endl;
    // }
}

void Hybrid::mesh_sweep(int Nmax){
    std::vector<std::string> micro_p;
    int prevnum;
    bool printout = true;
    for(int i=1;i<Nmax;++i){
        for(auto r=reacs.begin();r!=reacs.end();++r){
            double Wtemp = W(r->first,3,i);
            if(Wtemp>=epsilon){
                find_matching_reactions(r->second,micro_p);
            }
        }
        int nmicrotemp = micro_p.size();
        if(i>1){
            if(nmicrotemp!=prevnum){
                printout = true;
            }
            else{
                printout = false;
            }
        }
        if(printout){
            std::cout << "Num vox: " << i << ", Num microp: " << nmicrotemp << std::endl;
        }
        if(nmicrotemp==0){
            break;
        }
        
        micro_p.clear();
        prevnum = nmicrotemp;
    }
}

void Hybrid::find_matching_reactions(Reaction rin,std::vector<std::string>& micro_p){
    auto prod_in = rin.get_products();
    for(auto r2=reacs.begin();r2!=reacs.end();++r2){
        if(spm.compare_species(rin.get_reactants(),r2->second.get_products())){
            /* TODO: We should look recursively. */
            if(r2->second.get_type()==Reaction::unimol){
                
                auto rtemp = r2->second.get_reactants();
                if(rin.get_nprods()!=1 || rtemp[0].get_name()!=prod_in[0].get_name()){
                    
//                    std::cout << "Found unimolecular reaction that matches. ";
//                    std::cout << "Reaction is:" << std::endl;
//                    r2->second.print();
                    
//                    std::cout << "We recommend that species " << rtemp.at(0).get_name() << " is simulated on the microscopic scale." << std::endl;
                    if(std::find(micro_p.begin(),micro_p.end(),rtemp.at(0).get_name())==micro_p.end()){
                        micro_p.push_back(rtemp.at(0).get_name());
                    }
                }
            }
            else{
                if(W(r2->first,3,spm.num_voxels())>=epsilon){
                    find_matching_reactions(r2->second,micro_p);
                }
            }
        }
    }
}

void Hybrid::run(double T){
    
    /* Initialize solvers. */
    micro_sol.hybrid_initialize(particles);
    meso_sol.hybrid_initialize(particles);
    
    
    

    /* Determine the splitting time step. */
    double dt = dt_split;
    /* ********************************** */
    
    
    /* ***************************************************************** */
    /* ***************************************************************** */
    /* Determine minimum time a new microscopic molecule is microscopic. */
    /* ***************************************************************** */
    
    /* Get the average voxel size. */
    
    double avg_voxel_size = spm.get_vol()/spm.num_voxels();
    
    /* A molecule should have time to get well-mixed on spatial length 
     * scale of a single voxel. */
    
    double dt_min = pow(avg_voxel_size,2.0/3.0)*C_dt*C_dt/6;
    dt = dt_min;
   // std::cout << "dt_min=" << dt_min << std::endl;
    /* ******************************************************************** */
    /* Still need to divide this by the diffusion constant of the molecule. */
    /* Note that we do that below.                                          */
    /* ******************************************************************** */
    
    /* ******************************************************************** */
    /* ******************************************************************** */
    /* ******************************************************************** */
    
    
//    std::cout << "Starting at t=" << t << std::endl;
    
    int num_meso=0,num_micro=0;
    
    while(t<T*0.9999999999){
//        std::cout << "t=" << t << std::endl;
        if(t+dt>T){
            dt = T-t;
        }
        
//        std::cout << "Running solvers..." << std::endl;
//        std::cout << "Number of particles: " << particles.size() << std::endl;
       
        /* *********************************** */
		/* * Count meso and micro particles. * */
		/* *********************************** */
		num_meso=0;
		num_micro=0;
		for(auto pt=particles.begin();pt!=particles.end();++pt){
			if(pt->second.get_meso_micro()==Particle::meso) ++num_meso;
			if(pt->second.get_meso_micro()==Particle::micro) ++num_micro;
		}
		
		/* ********************************* */
		/* ********************************* */
		/* ********************************* */
       
       
		/* ********************************* */
		/* ******* Split molecules. ******** */
		/* ********************************* */
		
		for(auto pt=particles.begin();pt!=particles.end();++pt){
            if(std::find(micro_particles.begin(),micro_particles.end(),pt->second.get_name())!=micro_particles.end()){
				pt->second.set_meso_micro(Particle::micro);
			}
            else{
                if(pt->second.get_meso_micro()==Particle::micro && pt->second.get_clock()>dt_min/pt->second.get_D()){
					pt->second.set_meso_micro(Particle::meso);
				}
                else if(pt->second.get_meso_micro()==Particle::meso){
                    pt->second.set_meso_micro(Particle::meso);
                }
				else{
					pt->second.set_meso_micro(Particle::micro);
				}
            }
            
       
		}

       
		/* ********************************* */
		/* ********************************* */
		/* ********************************* */
//
        microtimer.start();
        if(micro_particles.size()>0){ 
            micro_sol.set_particles(particles);
            micro_sol.run(dt);
            particles = micro_sol.get_particles();
            micro_sol.set_t(0.0);
        }
        microtimer.stop();
        
        mesotimer.start();

        meso_sol.set_particles(particles);
        meso_sol.run(dt);
        particles = meso_sol.get_particles();
        meso_sol.set_t(0.0);
        
        mesotimer.stop();
        
        t+=dt;

    }

}
void Hybrid::print_avgs(){
    /* TODO: This function is not safe (can we guarantee that species types will be integers starting at 0? I don't think so.) */
    //    auto specs = spm.get_species();
    int nspecs = specs.size();
    //    for(auto sp=specs.begin();sp!=specs.end();++sp){
    //        std::cout << sp->second.get_name() << "   ";
    //    }
    //    std::cout << std::endl;
    int *nmols = (int *)calloc(nspecs,sizeof(int));
    for(auto p=particles.begin();p!=particles.end();++p){
        ++nmols[p->second.get_type()];
    }
    for(int j=0;j<nspecs;++j){
        std::cout << nmols[j] << " ";
    }
    std::cout << std::endl;
    
}


void Hybrid::print_state(){
    if(output_type==HDF5){
        outw.write_spatial(particles,spm.get_species());
    }
    else if(output_type==POSITION){
        for(auto p=particles.begin();p!=particles.end();++p){
            p->second.print();
        }
    }
}


/* Returns the mesoscopic reaction rate for the reaction r in a voxel with volume vox_vol. */
double Hybrid::kmeso(Reaction r,double vox_vol,int dim){

    auto reactants = r.get_reactants();
    
    double k = r.get_rate();
    
    if(r.get_type()==Reaction::bimol){
        
        double htemp = pow(vox_vol,1.0/(double)dim);
        double sigmatemp = specs.at(reactants[0].get_type()).get_sigma()+specs.at(reactants[1].get_type()).get_sigma();
        double Dtemp = specs.at(reactants[0].get_type()).get_D()+specs.at(reactants[1].get_type()).get_D();
//        std::cout << "sigma=" << sigmatemp << ", D=" << Dtemp << std::endl;
//        std::cout << "htemp=" << htemp << std::endl;
        k = RDME::k_r_meso(k,htemp,sigmatemp,Dtemp,dim);
    }
    
    return k;
}

double Hybrid::find_h(long rid,double tol,int dim){
    
    
    auto r = reacs.at(rid);
    if(r.get_type()==Reaction::bimol){
        double k = r.get_rate();
        auto reactants = r.get_reactants();
        
       
        double sigmatemp = specs.at(reactants[0].get_type()).get_sigma()+specs.at(reactants[1].get_type()).get_sigma();
        double Dtemp = specs.at(reactants[0].get_type()).get_D()+specs.at(reactants[1].get_type()).get_D();
        
        return C_alpha_3/(6.0*Dtemp)*pow(1.0/(4*pi*sigmatemp*Dtemp)-tol/k,-1);
    }
    return -1;
    
}

double Hybrid::find_num_vox(long rid,double tol,int dim){
    
    double htemp = find_h(rid,tol,dim);
    double tot_vol = spm.get_vol();
    return tot_vol/pow(htemp,3);
    
}

/* Returns the value of the function W for the reaction with id 'rid'. */
double Hybrid::W(long rid,int dim,int Nvox){
    
    auto r = reacs.at(rid);
    
//    int Nvox = spm.num_voxels();
    double tot_vol = spm.get_vol();
    double avg_vox_vol = tot_vol/Nvox;
    
    if(r.get_type()==Reaction::bimol){
        double k = r.get_rate();
        double km = kmeso(r,avg_vox_vol,dim);
//        std::cout << "dim=" << dim << " avg_vox_vol=" << avg_vox_vol << ", k=" << k << ", km=" << km*avg_vox_vol << std::endl;
        return k/(avg_vox_vol*km)-1.0;
    }
    else{
        /* W only makes sense for bimolecular association reactions. */
        return -1.0;
    }
    
    return 0.0;
}

void Hybrid::set_voxel_meso_micro(int voxel_id,int species_id,int meso_micro){
    voxel_meso_micro[voxel_id][species_id] = meso_micro;
}

void Hybrid::print_time_stats(){
    double microt = microtimer.total_time();
    double mesot = mesotimer.total_time();
    std::cout << "Time on microscale (% of total): " << microt << "(" << microt/(microt+mesot) << "%)" << std::endl;
    std::cout << "Time on mesoscale (% of total): " << mesotimer.total_time() << "(" << mesot/(microt+mesot) << "%)" << std::endl;
    
}

void Hybrid::print_time_stats_clean(){
    double microt = microtimer.total_time();
    double mesot = mesotimer.total_time();
    std::cout <<  microt << std::endl;
    std::cout << mesot << std::endl;
    
}
