#ifndef RDSIM_LIBS_SOLVER_GEOMETRY
#define RDSIM_LIBS_SOLVER_GEOMETRY

class Event{
	private:
	
		
		double tevent; //Time that this event executes.
		std::vector<long> p_ids; //Particle id:s of reactant molecules. 
		int rid; //Reaction id.
		int type;//-1 diffusion, 0 dissociation, 1 association
		
	public:
		Event(double t_in,int type_in,std::vector<long> p_ids_in,int rid_in){
			tevent = t_in;
			type = type_in;
			p_ids = p_ids_in;
			rid = rid_in;
		}
		
		int get_type(){return type;}
		int get_reaction_id(){return rid;}
		std::vector<long> get_p_ids(){return p_ids;}
	
};

#endif
