#ifndef RDSIM_LIBS_SOLVER_HYBRID
#define RDSIM_LIBS_SOLVER_HYBRID

#include <string>

#include "Solver.h"
#include "Id_generator.h"

#include "Spatial_model.h"

#include "Particle.h"
#include "OutputWriter.h"

#include "RDME.h"
#include "Smol.h"

#include "Timer.h"

class Hybrid: public Solver{
    
private:
    
//    int input_type;
//    int output_type;
//    int debug;//To turn off, set to 0.
    
    Spatial_model spm;
    OutputWriter outw;
    
    std::unordered_map<long,Particle> particles;
    std::unordered_map<long,Particle> dead_particles;
    
    std::vector<std::string> micro_particles;
    
    std::unordered_map<long,Species> specs;
    std::unordered_map<long,Reaction> reacs;
    double t;
    
    std::vector< std::unordered_map<long,int>> voxel_meso_micro;
    void set_voxel_meso_micro(int voxel_id,int species_id,int meso_micro);
    int get_voxel_meso_micro(int voxel_id,int species_id){return voxel_meso_micro[voxel_id].at(species_id);}
    
    const double pi=3.141592653589793;
    const double C_alpha_3=1.5164;
    const double C_alpha_2=0.1951;
    
    Id_generator& idg;
    
    double find_h(long rid,double tol,int dim);
    double find_num_vox(long rid,double tol,int dim);
    double W(long rid,int dim,int Nvox);
    double kmeso(Reaction r,double vox_vol,int dim);
    void find_matching_reactions(Reaction rin,std::vector<std::string>& micro_p);
    
    
    
    Smol micro_sol;
    RDME meso_sol;
    
    Timer microtimer = Timer();
    Timer mesotimer = Timer();
    
    double epsilon = 0.025;
    double C_dt = 6.0;

    double dt_split = 0.005;

    
public:
    
    Hybrid(Spatial_model spm_in,std::string output_file_name):spm{spm_in},idg{Id_generator::get_instance()},micro_sol{spm_in,idg},meso_sol{spm_in,idg}{
        particles = spm_in.get_particles();
        outw = OutputWriter(output_file_name);
        //outw.new_trajectory(spm.get_species());
    }
	
    
    void set_dt_split(double dt_split_in){dt_split = dt_split_in;}
    double get_C_dt(){return C_dt;}
    
    void run(double T); /* Advances the state to time T. */
    void initialize(); /* Initializes the solver. */
    void print_state(); /* Prints the state of the system. */
    void print_avgs();
    
    void print_time_stats();
    void print_time_stats_clean();

    
    void mesh_sweep(int Nmax);

};


#endif
