#ifndef RDSIM_LIBS_SOLVERS_OUTPUTWRITER
#define RDSIM_LIBS_SOLVERS_OUTPUTWRITER

#include <string>
#include <unordered_map>

#include "H5Cpp.h"

#include "Particle.h"
#include "Spatial_model.h"


class OutputWriter{
    private:
    
    H5::H5File file;
    int traj;
    int time_index;
    
    void add_time_point_to_file(std::unordered_map<long,Particle> particles, std::unordered_map<long,Species>& specs);
    
    public:
    
    void new_trajectory(std::unordered_map<long,Species> specs);
    void write_spatial(std::unordered_map<long,Particle> particles, std::unordered_map<long,Species> specs);
    
    OutputWriter(){}
    
    OutputWriter(std::string filename){
       // std::cout << "Opening file: " << filename << std::endl;
        file = H5::H5File(filename,H5F_ACC_TRUNC);
        H5::Group trajectory_group = file.createGroup( "/Trajectories" );
        traj = -1;
        time_index = -1;
//        new_trajectory();
    }
    
    
};

#endif
