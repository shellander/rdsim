#include "SSA.h"

/* Set state according to initial values. */
void SSA::initialize(){
    name = "SSA";
    initialize_rng();
    nspecs = m.get_nspecs();
    nreacs = m.get_nreacs();
    t=0.0;
    Tfinal = 0.0;
    
    auto specs = m.get_species();
    outw.new_trajectory(specs);

    state.clear();
    for(auto s=specs.begin();s!=specs.end();++s){
        state.insert({s->second.get_name(),s->second.get_initial_value()});
    }
    
    try{
        /* Pre-compute the well-mixed rates from the microscopic rates. */
        int type;
        double sigma,D;
        
        auto reactions = m.get_reactions();
//        auto specs = m.get_species();
        
        for(auto r=reactions.begin();r!=reactions.end();++r){
//
            type = r->second.get_type();
            if(type==0){
                kmeso.insert({r->second.get_name(),r->second.get_rate()});
            }
            else if(type==1){
//
//                bool rev = ;
                double rev_rate = m.get_rev_rate(r->second);
                if(r->second.get_nprods()==2 && rev_rate>0){
                    auto prods = r->second.get_products();
                    sigma = prods[0].get_sigma()+prods[1].get_sigma();
                    D = prods[0].get_D()+prods[1].get_D();
                    kmeso.insert({r->second.get_name(),4*pi*sigma*r->second.get_rate()/(4*pi*sigma*D+rev_rate)});
                }
                else{
                    kmeso.insert({r->second.get_name(),r->second.get_rate()});
                }
            }
            else{
//
                auto reacts = r->second.get_reactants();
                sigma = reacts[0].get_sigma()+reacts[1].get_sigma();
                D = reacts[0].get_D()+reacts[1].get_D();
                kmeso.insert({r->second.get_name(),4*pi*sigma*r->second.get_rate()/(4*pi*sigma*D+r->second.get_rate())});
            }
        }
//        free(s);
    }catch(ModelError err){
        err.print_msg();
        std::cerr << "Initialization failed. Cannot recover." << std::endl;
        exit(1);
    }
}

/* Return the propensity of a given reaction for a given state of the system. */
double SSA::prop(Reaction r){
    if(!r.isactive()) return 0.0;
    
    int type = r.get_type();
    std::string rname = r.get_name();
    auto reactants = r.get_reactants();
    double rrate;
    try{
        if(type==0){
            return kmeso[rname];
        }
        else if(type==1){
            return kmeso[rname]*state.at(reactants[0].get_name())/m.get_volume();
        }
        else{
            return kmeso[rname]*state.at(reactants[0].get_name())*state.at(reactants[1].get_name())/m.get_volume();
        }
        
    }
    catch (const std::out_of_range& err) {
        std::cerr << "Out of Range error in function prop: " << err.what() << std::endl;
        std::cerr << "Could not compute propensity. Cannot recover." << std::endl;
        exit(1);
    }
    
    return rrate;
}

void SSA::run(double T){
    if(T<Tfinal){
        std::cout << "Time has to be in the future." << std::endl;
        exit(1);
    }
    Tfinal = T;
//
    unsigned int nspecs = m.get_nspecs();
    unsigned int nreacs = m.get_nreacs();
//
    double t_ne,r_ne; /* Time until next event. */
    int r_index;
    double total_prop;
//
    double *prop_vector = new double[nreacs];
//
    
    
    auto reacs = m.get_reactions();
    while(t<Tfinal){
//
//        /* Get total propensity. */
        total_prop = 0.0;
        int i=0;
        for(int i=0;i<nreacs;i++){
            prop_vector[i] = prop(reacs.at(i));
            total_prop += prop_vector[i];
            if(i>0) prop_vector[i] += prop_vector[i-1]; /* Get cumulative sum. */
        }
//        //        std::cout << total_prop << std::endl;
//        
//        /* Get next event time. */
        t_ne = -log(gsl_rng_uniform(rng))/total_prop;
//
        r_ne = -log(gsl_rng_uniform(rng));
        r_index = -1;
//
//        /* Find the next reaction. */
        for(int i=0;i<nreacs;i++){
            if(r_ne<prop_vector[i]){
                r_index = i;
                break; /* Break when we find the next reaction. */
            }
        }
//
//        /* This can happen if r_ne=1, or because of small rounding errors in prop_vector. */
        if(r_index == -1){
            r_index = nreacs-1;
        }
//
//        /* Execute reaction r_index by updating the state. */
//        /* First check if the reaction executes before T. */
        if(t+t_ne>T){
//            /* We have reached the end of the simulation. */
            t = T;
            break;
        }
        else{
//            /* Get reactants and products. */
            auto reactants = reacs.at(r_index).get_reactants();
            auto products = reacs.at(r_index).get_products();
            /* Update the state. Subtract 1 for each reactant. */
            for(auto rs=reactants.begin();rs!=reactants.end();++rs){
                --state[rs->get_name()];
            }
//            /* Add 1 for each product. */
            for(auto ps=products.begin();ps!=products.end();++ps){
                ++state[ps->get_name()];
            }
//            free(reactants);
//            free(products);
        }
//        /* Add time t_ne to time t. */
        t += t_ne;
//
//        
    }
//
    free(prop_vector);
//    
}

void SSA::print_state(){
    
    particles.clear();
    std::array<double,3> pos{0.0,0.0,0.0};
    
    auto specs = m.get_species();
    for(auto s=specs.begin();s!=specs.end();++s){
        for(int j=0;j<state.at(s->second.get_name());++j){
            Particle temp{pos,s->second,idg};
            particles.insert({temp.get_id(),temp});
        }
    }
    outw.write_spatial(particles,specs);
}
