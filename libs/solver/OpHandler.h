#ifndef RDSIM_LIBS_SOLVERS_OPHANDLER
#define RDSIM_LIBS_SOLVERS_OPHANDLER

#include <iostream>
#include <fstream>
#include <string>

class OpHandler{
private:
    std::streambuf *coutbuf;
    OpHandler(){}
    
public:
    static OpHandler& get_instance(){
        static OpHandler oph;
        return oph;
    }
    void redirect_std_out(std::string filename);
    void reset_std_out();
    void flush_stream();
};

#endif
