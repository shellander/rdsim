

#include "Smol.h"

#define DEBUG 1

double Smol::get_sigma_pair(int pid1,int pid2){
    return particles.at(pid1).get_sigma()+particles.at(pid2).get_sigma();
}

double Smol::get_D_pair(int pid1,int pid2){
    return particles.at(pid1).get_D()+particles.at(pid2).get_D();
}


void Smol::randomize_cube(std::array<double,3>& pin,double width){
    for(int i=0;i<3;++i){
        pin[i] = width*gsl_rng_uniform(rng);
    }
}

void Smol::hybrid_initialize(std::unordered_map<long,Particle>& particles_in){
    particles = particles_in;
//    reactions = reacs_in;
//    specs = specs_in;
    
    t = 0.0;
    
    /* Initialize random number generator. */
    initialize_rng();
    
    
    output_type = HDF5;  
    debug = D_OFF;
}


void Smol::initialize(){
    
    input_type = PYURDME;
    output_type = HDF5;
    debug = D_OFF;
    
    outw.new_trajectory(spm.get_species());
    
    
    t = 0.0;
    
    /* Initialize random number generator. */
    initialize_rng();
    
    
    
    auto specs = spm.get_species();
    
    int Nvox,vox_num;
    Nvox = spm.num_voxels();
    double tot_vol = spm.get_volume();
    
    double pos_temp[3];
    /* Set up initial condition. First add particles to model's list of particles, then copy list to solver's list of particles.*/
    std::array<double,3> pin;
    
    spm.clear_particles();
    particles.clear();
    
    
    if(input_type==RDSIM){
        for(auto sp=specs.begin();sp!=specs.end();++sp){
            for(int i=0;i<sp->second.get_initial_value();++i){
                
                pin = spm.sample_mesh_uniform(rng,sp->second.get_dim());
                
                /* Now add the molecule. */
                int pid = spm.add_particle(pin,sp->second);
                pos_temp[0] = pin[0];
                pos_temp[1] = pin[1];
                pos_temp[2] = pin[2];
                int voxel = spm.micro2meso(pos_temp,0);
                spm.set_particle_voxel(pid,voxel);
                
            }
        }
    }
    else if(input_type==PYURDME){
        std::array<double,3> pos;
        for(int i=0;i<spm.num_voxels();++i){
            for(auto sp=specs.begin();sp!=specs.end();++sp){
                int j = sp->second.get_urdme_dof_nr();
                for(int k=0;k<spm.get_init_voxel(j,i);++k){
                    int pid = spm.add_particle({0.0,0.0,0.0},sp->second);
                    spm.set_particle_voxel(pid,i);
                    pos = spm.meso2micro(pid);
                    spm.set_particle_pos(pos,pid);
                    /* Set all particles to mesoparticles. */
                    spm.set_meso_micro(Particle::micro,pid);
                }
            }
        }
    }
    particles = spm.get_particles();
   // subsets.resize(0);
   // std::vector<subset> subsets_v1;
   // subsets.push_back(subsets_v1);
//    dt = sort_particles_bf(subsets[0]);
}

double Smol::get_time_step(Particle p1,Particle p2){
    double dst = dist(p1,p2);//-(p1.get_sigma()+p2.get_sigma());
    double D = p1.get_D()+p2.get_D();
    return pow(dst,2)/(K*D);
}







void Smol::reflect_all_planes(int pid){
	auto planes = spm.get_planes();
	//std::cout << "In reflect_all_planes..." << std::endl;
	for(auto plane=planes.begin();plane!=planes.end();++plane){
        if(plane->isbnd==1){
            particles.at(pid).set_pos(reflect_in_plane(pid,*plane));
        }
	}
}


std::array<double,3> Smol::reflect_in_plane(int pid,FEM_mesh::plane pl){
	auto pos = particles.at(pid).get_pos();
	double *n = pl.n;
	double *pp = pl.p;
	double dp = 0.0;
    for(int i=0;i<3;++i){ dp += n[i]*(pos[i]-pp[i]);}
	if(dp<0){
//        std::cout << "****************************" << std::endl;
//		std::cout << "Position before reflection: " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
//        std::cout << "Plane point: " << pp[0] << " " << pp[1] << " " << pp[2] << std::endl;
//		std::cout << "Plane normal: " << n[0] << " " << n[1] << " " << n[2] << std::endl;
//		std::cout << "dp = " << dp << std::endl;
        for(int i=0;i<3;++i){ pos[i] += 2*fabs(dp)*n[i];}
//		std::cout << "Position after reflection: " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
//        std::cout << "****************************" << std::endl;

	}
	return pos;
}

double Smol::reaction_time(std::vector<long> pids,Reaction r,double dt){
    int npids = (int)(pids.size());
    double k = 0.0;
    double t = INFINITY;
    if(npids==0 || npids==1){
        k = r.get_rate();
        t = -log(gsl_rng_uniform(rng))/k;
        return t;
    }
    else if(npids==2){
        k = r.get_rate();
        int pid1 = pids[0];
        int pid2 = pids[1];
        double D = particles.at(pid1).get_D()+particles.at(pid2).get_D();
        double sigma = particles.at(pid1).get_sigma()+particles.at(pid2).get_sigma();
        auto pos1 = particles.at(pid1).get_pos();
        auto pos2 = particles.at(pid2).get_pos();
        double rd = SmolLinAlg::dist3(pos1,pos2);
//        auto relative_pos = SmolLinAlg::vec_diff(particles.at(pid1).get_pos(),particles.at(pid2).get_pos());
//        double rd = SmolLinAlg::norm(relative_pos);
        //std::cout << "D=" << D << ", sigma=" << sigma << ", rel. dist.=" << rd;
        t = Smol_rng::random_time(dt,rd,k,sigma,D,rng,particles.at(pid1).get_dim());
//        if(t<dt){
//            std::cout << "----------------------------" << std::endl;
//            std::cout << "pid1=" << pids[0] << ", pid2=" << pids[1] << std::endl;
//            std::cout << "rd=" << rd << ", t=" << t << std::endl;
//            std::cout << "----------------------------" << std::endl;
//        }
        //std::cout << ", t=" << t << std::endl;
        return t;
    }
    else{
        throw SolverError("Wrong size of 'pids' in Smol::reaction_time\n");
    }
}

/* TODO: Rewrite this function so that it can return all tentative reactions involving
 either all molecules in stypes, or individual molecules. */

std::unordered_map<long,double> Smol::get_tentative_rtimes(std::vector<long> pids,double dt){
	
    std::unordered_map<long,double> tent_rtimes;
    
    std::vector<long> stypes;
    int tp;
    //std::cout << "Number of pids: " << pids.size() << std::endl;
    for(auto pid=pids.begin();pid!=pids.end();++pid){
        tp = particles.at(*pid).get_type();
        stypes.push_back(tp);
        //std::cout << "Species type: " << tp << std::endl;
    }
    //std::cout << "Number of stypes: " << stypes.size() << std::endl;
    auto treacs = spm.get_tentative_reactions(stypes);
    auto reacs = spm.get_reactions();
    for(auto r=treacs.begin();r!=treacs.end();++r){
		double timetemp = reaction_time(pids,reacs.at(*r),dt);
		/*if((int)(pids.size()==2)){
			std::cout << "Tentative reaction: " << reacs.at(*r).get_name() << std::endl;
			std::cout << "Tentative time: " << timetemp << std::endl;
		}*/
        tent_rtimes.insert({*r,timetemp});
        
    }
    
//    auto reacs = spm.get_reactions();
//    for(auto r=reacs.begin();r!=reacs.end();++r){
////        if(
//    }
    
    
    return tent_rtimes;
}

Smol::tent_event Smol::get_first_reaction(std::vector<long> pids,double dt,bool look_for_bimol=true){
	
	/* pids should be a vector of length 0,1, or 2. */
    int npids = (int)(pids.size());
    std::unordered_map<long,double> tr1,tr2,tr3;
    if(npids>2){
        throw SolverError("Wrong size of vector 'pids' in get_first_reaction.");
    }
    if(npids==2){
        /* We should check for reactions involving either one or both molecules. */
        tr1 = get_tentative_rtimes({pids[0]},dt);
        tr2 = get_tentative_rtimes({pids[1]},dt);
        if(look_for_bimol){
            tr3 = get_tentative_rtimes(pids,dt);
        }
    }
    else{
        tr1 = get_tentative_rtimes(pids,dt);
    }
    
    
    //auto tent_reacs = get_tentative_rtimes(pids,dt);
    
	int rtemp = -1;
	double ttemp = INFINITY;
	
	int pidtemp = -1;
	
	/* WTF. Should work though. */
	for(auto r=tr1.begin();r!=tr1.end();++r){
		if(r->second<ttemp){
			rtemp = r->first;
			ttemp = r->second;
			pidtemp = 0;
		}
	}
	for(auto r=tr2.begin();r!=tr2.end();++r){
		if(r->second<ttemp){
			rtemp = r->first;
			ttemp = r->second;
			pidtemp = 1;
		}
	}
	for(auto r=tr3.begin();r!=tr3.end();++r){
		if(r->second<ttemp){
			rtemp = r->first;
			ttemp = r->second;
			pidtemp = 2;
		}
	}
    
    
    
	//std::unordered_map<long,double> first_reac;
	tent_event te;
	te.time = ttemp;
	te.reaction_id = rtemp;
	auto reactions = spm.get_reactions();
    
    
    if(rtemp!=-1){
        /* rtemp==-1 means that the molecule/s doesn't react. */
        te.type = reactions.at(rtemp).get_type();
    }
	
	if(pidtemp==0){
		te.pids = {pids[0]};
	}
	else if(pidtemp==1){
		te.pids = {pids[1]};
	}
	else if(pidtemp==2){
		te.pids = pids;
	}
	else{
		te.pids.resize(0);
	}
	//first_reac.insert({rtemp,ttemp});
	return te;
}

long Smol::add_particle(std::array<double,3> pin,Species& sp){
    Particle p{pin,sp,idg};
    particles.insert({p.get_id(),p});
    return p.get_id();
}

std::vector<long> Smol::doDissociation(Smol::tent_event reac){
	
	int pid1 = reac.pids[0];
	
	std::vector<long> newpids;
	
	auto reactions = spm.get_reactions();
	Reaction r = reactions.at(reac.reaction_id);
	int nprods = r.get_nprods();
	auto products = r.get_products();
    
    auto curr_pos = particles.at(pid1).get_pos();
    int voxel = particles.at(pid1).get_voxel();
    double sigma = 0.0;
    int dimension;
    
    if(nprods==0){
    }
    else if(nprods==1){
        sigma = 0.0;
        dimension = products[0].get_dim();
    }
    else if(nprods==2){
        sigma = products[0].get_sigma()+products[1].get_sigma();
        dimension = products[0].get_dim();
    }
    else{
        throw SolverError("Incorrect number of products in dissociation. Maximum is 2. Reaction has "+std::to_string(nprods));
    }
    
    int i=0;
    std::array<double,3> pnew;
	/* Create all products. */
	for(auto p=products.begin();p!=products.end();++p){
		
        
        /* Create new particle. */
        
        
        auto rsph = Smol_rng::random_sphere(rng,sigma,dimension);
        //std::cout << "rsph=(" << rsph[0] << "," << rsph[1] << "," << rsph[2] << ")" << std::endl;
        if(i==0){
            pnew = {curr_pos[0]+rsph[0],curr_pos[1]+rsph[1],curr_pos[2]+rsph[2]};
            i=1;
        }
        else{
            pnew = {curr_pos[0],curr_pos[1],curr_pos[2]};
        }
		
//        p->print();
        long newid = add_particle(pnew,*p);
        particles.at(newid).set_voxel(voxel);
        particles.at(newid).set_clock(-t);
        newpids.push_back(newid);
        
        
        
//        particles.at(newid).print();
	}
    /* Delete reactant. */
    dead_particles.insert({pid1,particles.at(pid1)});
    particles.erase(pid1);
	return newpids;
}

std::vector<long> Smol::doAssociation(Smol::tent_event reac){
	
	int pid1 = reac.pids[0];
	int pid2 = reac.pids[1];
	
	std::vector<long> newpids;
	
	/* Fetch reaction details. */
	auto reactions = spm.get_reactions();
	Reaction r = reactions.at(reac.reaction_id);
	int nprods = r.get_nprods();
	auto products = r.get_products();
    
    
    /* Get parameters. */
    double sigma1,sigma2,sigma,dimension;
    sigma1 = particles.at(pid1).get_sigma();
    sigma2 = particles.at(pid2).get_sigma();
    sigma = sigma1+sigma2;
    dimension  = particles.at(pid1).get_dim();
    
    auto curr_pos = particles.at(pid1).get_pos();
    int voxel = particles.at(pid1).get_voxel();
    /* Do association. */
    int i=0;
    std::array<double,3> pnew;
    auto rsph = Smol_rng::random_sphere(rng,sigma,dimension);
    for(auto p=products.begin();p!=products.end();++p){
        
        
        /* Create new particle. */
        
        if(i==0){
            pnew = {curr_pos[0]+rsph[0],curr_pos[1]+rsph[1],curr_pos[2]+rsph[2]};
            i=1;
        }
        else{
            pnew = {-pnew[0],-pnew[1],-pnew[2]};
        }
        
        //        p->print();
        long newid = add_particle(pnew,*p);
        newpids.push_back(newid);
        particles.at(newid).set_voxel(voxel);
        particles.at(newid).set_clock(-t);
        
        
        //        particles.at(newid).print();
    }
    dead_particles.insert({pid1,particles.at(pid1)});
    dead_particles.insert({pid2,particles.at(pid2)});
    particles.erase(pid1);
    particles.erase(pid2);
	
	return newpids;
}

//void Smol::sample_r_irr(int pid1,int pid2,double dt_loc,double k_r){
//    /* Get parameters. */
//    double sigma1,sigma2,sigma,dimension,D1,D2,D;
//    D1 = particles.at(pid1).get_D();
//    D2 = particles.at(pid2).get_D();
//    D = D1+D2;
//    sigma1 = particles.at(pid1).get_sigma();
//    sigma2 = particles.at(pid2).get_sigma();
//    sigma = sigma1+sigma2;
//    dimension  = particles.at(pid1).get_dim();
//    
//    std::array<double,3> rel_pos;
//    
//    pos1 = particles.at(pid1).get_pos();
//    pos2 = particles.at(pid2).get_pos();
//    
//    for(int j=0;j<3;++j){
//        rel_pos[j] = pos1[j]-pos2[j];
//    }
//    
//    r = Smol_rng::random_r_irr(dt_loc,r_0,k_r,sigma,D,rng);
//}


void Smol::update_pair(int pid1,int pid2,double dt_loc,double k_r,bool reacted){

	
	
    double theta,phi,r;
    double endr;
    
    
    /* Get parameters. */
    double sigma1,sigma2,sigma,dimension,D1,D2,D;
    D1 = particles.at(pid1).get_D();
    D2 = particles.at(pid2).get_D();
    D = D1+D2;
    sigma1 = particles.at(pid1).get_sigma();
    sigma2 = particles.at(pid2).get_sigma();
    sigma = sigma1+sigma2;
    dimension  = particles.at(pid1).get_dim();
    
    /* Get weighted relative and weighted absolute positions of the pair. */
    std::array<double,3> pos1,pos2;
    
    std::array<double,3> abs_pos,rel_pos;
    double w1,w2;
    
    w1 = sqrt(D2/D1);
    w2 = 1.0/w1;
    
    pos1 = particles.at(pid1).get_pos();
    pos2 = particles.at(pid2).get_pos();
    
    for(int j=0;j<3;++j){
        abs_pos[j] = w1*pos1[j]+w2*pos2[j];
        rel_pos[j] = pos1[j]-pos2[j];
    }
    
    
    
    /* Sample new theta, phi, and distance r. */
    double r_0 = SmolLinAlg::norm(rel_pos);
    
    /* TODO: N should be set in Smol_rng. */
    /* TODO: r should be sampled sometimes and sometimes not (reaction => r=0). */
    int N = 10;
    if(reacted){
        r = 0.0;
    }
    else{
        r = Smol_rng::random_r_irr(dt_loc,r_0,k_r,sigma,D,rng);
    }
    theta = Smol_rng::gen_ran_theta(D,dt_loc,r,N,rng);
    phi = 2*pi*gsl_rng_uniform(rng);
    
    
    
    
    /* We should now diffuse abs_pos and scale rel_pos. */
    double temp[3];
    double c = sqrt(2*D*dt_loc);
    temp[0] = c*gsl_ran_gaussian_ziggurat(rng,1.0);
    temp[1] = c*gsl_ran_gaussian_ziggurat(rng,1.0);
    temp[2] = c*gsl_ran_gaussian_ziggurat(rng,1.0);
    SmolLinAlg::normalize(rel_pos);
    std::array<double,3> vec1,vec2,vec3;
    vec1 = particles.at(pid1).get_vec1();
    vec2 = particles.at(pid1).get_vec2();
    vec3 = particles.at(pid1).get_vec3();
    for(int j=0;j<3;++j){
        abs_pos[j] += temp[0]*vec1[j]+temp[1]*vec2[j]+temp[2]*vec3[j];
        rel_pos[j] *= r;
    }
    /* Set new positions. */
    
    std::array<double,3> new_pos1,new_pos2;
    for(int j=0;j<3;++j){
        new_pos1[j] = (w1*abs_pos[j]-rel_pos[j])/(1.0+w1*w1);
        new_pos2[j] = new_pos1[j]+rel_pos[j];
    }
    
    particles.at(pid1).set_pos(new_pos1);
    particles.at(pid2).set_pos(new_pos2);
    
}

double Smol::get_rate(tent_event rtimes){
    auto r = spm.get_reaction(rtimes.reaction_id);
    return r.get_rate();
}

double Smol::get_distance(int pid1,int pid2){
    auto pos1 = particles.at(pid1).get_pos();
    auto pos2 = particles.at(pid2).get_pos();
    return SmolLinAlg::dist3(pos1,pos2);
}

/* Propagate a subset of 1 or 2 molecules. */
double Smol::simulate_subset(subset *sbst,double T){

    
    double t = 0.0;
    
    while(t<T){
     //   print_avgs();
//        std::cout << "t=" << t << std::endl;
        double dt_loc = T-t;
        
        int num_mol = sbst->p_ids.size();

        if(num_mol==0){
            /* Empty subset. We're done. */
        }
        else if(num_mol==1){
            int pid = sbst->p_ids[0];
            
            /* Get tentative reaction times. */
            auto rtimes = get_first_reaction({pid},dt_loc);
           //std::cout << "Subset has only one molecule." << std::endl;
            //std::cout << "Time until reaction is " << rtimes.time << std::endl;
            if(rtimes.time<dt_loc){
                /* Set time step. */
                dt_loc = rtimes.time;
                /* First diffuse molecule until dissociation fires. */
                particles.at(pid).diffuse(dt_loc,rng);
                /* Then fire dissociation reaction. */
                
                
                
                sbst->p_ids = doDissociation(rtimes);
				
				double tdist = get_distance(sbst->p_ids[0],sbst->p_ids[1]);
			//	std::cout << "dist after dissoc: " << tdist << std::endl;
			//	particles.at(sbst->p_ids[0]).print();
			//	particles.at(sbst->p_ids[1]).print();
				
            }
            else{
                /* No reaction. Pure diffusion. */
                particles.at(pid).diffuse(dt_loc,rng);
            }
        }
        else if(num_mol==2){

            
            int pid1 = sbst->p_ids[0];
            int pid2 = sbst->p_ids[1];
            
            
            /* First get distance between particles. */
            double dtemp = get_distance(pid1,pid2);
            /* Get the reaction radius. */
            double sigma_temp = particles.at(pid1).get_sigma()+particles.at(pid2).get_sigma();
            double Dtemp = particles.at(pid1).get_D()+particles.at(pid2).get_D();
            
            
            /* If the distance is significantly larger than the reaction radius, do free diffusion
             * for a small time step instead. This will be more efficient, as we don't need to sample
             * from the Smoluchowski PDF. */
            
            bool look_for_bimol;
            if(dtemp>1.1*sigma_temp){
                look_for_bimol=false;
                double dt_temp = pow(dtemp-sigma_temp,2)/(6*Dtemp*K);
                if(dt_temp<dt_loc){
					dt_loc = dt_temp;
				}
            }
            else{
                look_for_bimol=true;
            }
            
            /* Get tentative reaction times for all reactions involving both
             *  molecules or  either one of the molecules. */
            auto rtimes = get_first_reaction({pid1,pid2},dt_loc,look_for_bimol);
//              std::cout << "rtimes=" << rtimes.time << ", dt_loc=" << dt_loc << std::endl;
            if(rtimes.time<dt_loc){
                /* Set time step. */
                
//                std::cout << "dt_loc=" << dt_loc << ", rtimes.time=" << rtimes.time << ", type: " << rtimes.type << std::endl;
                double dt_orig = dt_loc;
                dt_loc = rtimes.time;
                
                /* First determine if reaction is bimolecular or unimolecular. */
                if(rtimes.type == Reaction::unimol){
                    
                    /* First diffuse molecules until dissociation fires. */
                    
                    /* TODO: Should be diffused as a pair, not independently. */
                    
                    particles.at(pid1).diffuse(dt_loc,rng);
                    particles.at(pid2).diffuse(dt_loc,rng);
                    
                    /* Fire dissociation reaction. */
                    /* We need to update the pids correctly after this dissociation event.
                     * It is likely now a group of 3 molecules. */
                     
                     /* We first do the actual reaction.*/
                     auto pidtemp = doDissociation(rtimes);
                     
                     double tdist = get_distance(pidtemp[0],pidtemp[1]);
//					std::cout << "dist after dissoc: " << tdist << std::endl;
                     
                     
                     /* Then delete the pid from the subset. */
                     if(sbst->p_ids[0] == rtimes.pids[0]){
                         sbst->p_ids.erase(sbst->p_ids.begin());
                     }
                     else{
                         sbst->p_ids.erase(sbst->p_ids.begin()+1);
                     }
                     
                     /* If the dissociation resulted in two new molecules, then
                      * then the new subset will have three molecules.*/
                      if(pidtemp.size()==2){
                          sbst->p_ids.push_back(pidtemp[0]);
                          sbst->p_ids.push_back(pidtemp[1]);
                      }
                      else if(pidtemp.size()==1){
                          sbst->p_ids.push_back(pidtemp[0]);
                      }
                }
                else if(rtimes.type == Reaction::bimol){
                    /* The two molecules in the subset react. */
                    double k_r = get_rate(rtimes);
//                    std::cout << "Bimol reaction " << std::endl;
                    
                    
//                    double r_before = get_distance(pid1,pid2);
//                    std::cout << "****************************************" << std::endl;
//                    std::cout << "pid1=" << pid1 << ", pid2=" << pid2 << std::endl;
//                    std::cout << "dt_loc=" << dt_orig << ", rtimes.time=" << rtimes.time << ", type: " << rtimes.type << std::endl;
//                    std::cout << "r_before=" << r_before << std::endl;
                   
                    
                    
                    
                    update_pair(pid1,pid2,dt_loc,k_r,true);

                    
                    
                    
                    //                    double r_after = get_distance(pid1,pid2);
//                    std::cout << "r_after=" << r_after << std::endl;
//                    std::cout << "****************************************" << std::endl;
                    /* Do the association step. */
                    sbst->p_ids = doAssociation(rtimes);
                    
                }
                else{
                    throw SolverError("Wrong reaction type in simulation of subset in micro solver. Should be unimol or bimol");
                }
                
            }
            else{
                /* No reaction. Pure diffusion. */
                /* Diffusion should take into account that no reaction happened. */
                /* This may serve as a sufficiently accurate approximation, and will be 
                 * more efficient. */
//                if(!look_for_bimol){
                    particles.at(pid1).diffuse(dt_loc,rng);
                    particles.at(pid2).diffuse(dt_loc,rng);
//                }
//                else{
//                    double k_r = 1.0;
//                    update_pair(pid1,pid2,dt_loc,k_r,false);
//                }
                
                /* Update pair of molecules given that no reaction occured. */
                
                
                /*TODO: If the molecules can associate, we need to find the k_r here. */
                /* It is a possibility that the molecules can react bimoleculary in several ways. How do we deal with that possibility? */
               
                
                
//                std::cout << "Updating pair..." << std::endl;
//                std::cout << "Positions before:" << std::endl;
//                particles.at(pid1).print();
//                particles.at(pid2).print();
                
//                double r_before = get_distance(pid1,pid2);
//                std::cout << "r_before=" << r_before << std::endl;
//                update_pair(pid1,pid2,dt_loc,k_r,false);
//                double r_after = get_distance(pid1,pid2);
//                std::cout << "r_after=" << r_after << std::endl;
                
                
//                std::cout << "Positions after: " << std::endl;
//                particles.at(pid1).print();
//                particles.at(pid2).print();
                
            }
            
        
        }
        else if(num_mol==3){
			
			//std::cout << "Number of molecules in..." << num_mol << std::endl;
			
            std::vector<subset> stemp;
           // stemp.resize(0);
            //stemp.push_back(*sbst);
            std::vector<long> pids_in;
            
            //for(auto sb=sbst->begin();sb!=sbst->end();++sb){
                //int nmol=(*sb).p_ids.size();
               // for(int k=0;k<nmol;++k){
               
               pids_in = sbst->p_ids;
              // stemp[0].p_ids = pids_in;
               /*
               for(auto pidsm=sbst->p_ids.begin();pidsm!=sbst->p_ids.end();++pidsm){
                    pids_in.push_back(*pidsm);
				}*/
                //}
           // }
            
            simulate(dt_loc,0.0,stemp,pids_in);
            /* Transfer particle ids back to original subset object. */
            int pcount = 0;
            sbst->p_ids.clear();
            //std::cout << "Size of stemp: " << stemp.size() << std::endl;
            //for(auto sb=stemp.begin();sb!=stemp.end();++sb){
				//int nmol=(*sb).p_ids.size();
				//for(int k=0;k<nmol;++k){
				for(auto pidsm=pids_in.begin();pidsm!=pids_in.end();++pidsm){
					sbst->p_ids.push_back(*pidsm);
					++pcount;
				}
				//}
			//}
			//std::cout << "Particles out: " << pcount << std::endl;
        }
        else{
            /* The subset is too big. This is an error. */
            throw SolverError("Subset is too big in simulate_subsystem. Should be fewer than two molecules in a subset. This subset has "+std::to_string(num_mol)+" molecules.");
        }
        
        t += dt_loc;
    }
	return T;
}


void Smol::simulate(double T,double tloc,std::vector<subset>& sbsts,std::vector<long>& pids_in){
	//std::cout << "Number of pids beginning: " << pids_in.size() << std::endl;
//	double tloc = 0.0;
	auto planes = spm.get_planes();
//	std::cout << "In Simulate: tloc=" << tloc << std::endl;
	double dt_loc = T;
	
	reset(sbsts);
//	dt_loc = sort_particles_bf(sbsts);
	
    int count = 0;
    
	while(tloc<T){

        dt_loc = sort_particles_bf2(sbsts,pids_in);

        /* To prevent that molecules get stuck in an "infinite" loop, just getting closer
         * and closer, we need to set a smallest allowed time step. */
        if(dt_loc<1e-8){
			dt_loc = 1e-8;
		}
        
        if(tloc+dt_loc>T){
			dt_loc = T-tloc;
		}

		for(unsigned int i=0;i<sbsts.size();++i){
			simulate_subset(&sbsts[i],dt_loc);
		}
		
        

		tloc += dt_loc;
        

        int num_mol_in_sbsts = 0;
        
        pids_in.clear();
        for(auto sb=sbsts.begin();sb!=sbsts.end();++sb){
            int nmol=(*sb).p_ids.size();
            for(int k=0;k<nmol;++k){
				if(particles.at((*sb).p_ids[k]).get_meso_micro()==Particle::micro){
					pids_in.push_back((*sb).p_ids[k]);
                    particles.at((*sb).p_ids[k]).add_to_time(dt_loc);
				}
				++num_mol_in_sbsts;
            }
        }
        
        
        
        

        int vox_temp;
        /* Reflect all particles that are outside the domain. */
        int pcount = 0;
        reset(sbsts);
    
        
        for(auto sb=sbsts.begin();sb!=sbsts.end();++sb){
            for(auto pid=sb->p_ids.begin();pid!=sb->p_ids.end();++pid){
                vox_temp = particles.at(*pid).get_voxel();
                if(spm.voxel_bnd(vox_temp)){
                    auto postemp = particles.at(*pid).get_pos();
                    int check = 1;
                    ++pcount;
                    particles.at(*pid).set_pos(reflect_in_plane(particles.at(*pid).get_id(),planes[vox_temp]));
                    int vox_new = spm.micro2meso(particles.at(*pid).get_pos(),particles.at(*pid).get_voxel());
                    particles.at(*pid).set_voxel(vox_new);
                    
                    int num_refl = 0;
                    while(vox_new!=vox_temp && num_refl<5){
                        
                        vox_temp = vox_new;
                        if(spm.voxel_bnd(vox_temp)){
                            particles.at(*pid).set_pos(reflect_in_plane(particles.at(*pid).get_id(),planes[vox_temp]));
                            vox_new = spm.micro2meso(particles.at(*pid).get_pos(),particles.at(*pid).get_voxel());
                        }
                        particles.at(*pid).set_voxel(vox_new);
                        ++num_refl;
                    }
                    if(num_refl==5){
                        std::cout << "DID NOT REFLECT PARTICLE BACK INTO DOMAIN." << std::endl;
//                        /* We diffuse the particle a little to get it out of the loop. */
                    }
                    
                }
            }
        }
        sbsts.clear();
        
	}
	
}



/* Propagates the system T seconds. */
void Smol::run(double T){
    
    double dt_loc = 0.0;
    double epsilon = 1e-9;
    double dt = 0.0;
    std::vector<subset> subsets;
    std::vector<long> pids_in;
    for(auto p=particles.begin();p!=particles.end();++p){
		if(p->second.get_meso_micro()==Particle::micro){
			pids_in.push_back(p->first);
		}
    }
    
 /*   int num_meso=0, num_micro=0;
    for(auto p=particles.begin();p!=particles.end();++p){
		if(p->second.get_meso_micro()==Particle::micro){
			num_micro++;
		}
		else{
			num_meso++;
		}
	}
	
    
    for(auto p=particles.begin();p!=particles.end();++p){
		if(p->second.get_meso_micro()==Particle::micro){
			std::cout << "Num meso: " << num_meso << ", Num micro: " << num_micro << std::endl;
			std::cout << "id " << p->second.get_id() << ", time: " << p->second.get_clock() << std::endl;
		}
	}*/
    
    if(pids_in.size()>0){ 
        simulate(T,t,subsets,pids_in);
    }
    t=T;
    
}

void Smol::reset(std::vector<subset>& subsets){
	int voxtemp;
	
    for(auto s=subsets.begin();s!=subsets.end();++s){
        /* Mark all particles as unsorted, and update their current voxel. */
        for(auto p=s->p_ids.begin();p!=s->p_ids.end();++p){
			voxtemp = spm.micro2meso(particles.at(*p).get_pos(),particles.at(*p).get_voxel());
            particles.at(*p).set_voxel(voxtemp);
            particles.at(*p).unsort();
        }
    }
    
    

}

void Smol::print_groups(std::vector<subset>& subsets){
    int i=1;
    std::cout << "p = cell(" << subsets.size() << ",1);" << std::endl;
    for(auto s=subsets.begin();s!=subsets.end();++s){
        std::cout << "p{" << i << "}=";
//        std::cout << "******GROUP******" << std::endl;
        std::cout << "{";
        int j=0;
        for(auto pid=s->p_ids.begin();pid!=s->p_ids.end();++pid){
            if(j>0){
                std::cout << ",";
            }
            std::cout << "[";
            particles.at(*pid).print();
            std::cout << "]";
            ++j;
        }
        std::cout << "};" << std::endl;
        
        ++i;
//        std::cout << "*****************" << std::endl;
    }
    std::cout << "plot_pairs(p);" << std::endl;
    std::cout << "pause(0.3);" << std::endl;
    std::cout << "hold off;" << std::endl;
}


void Smol::print_state(){
    if(output_type==HDF5){
        outw.write_spatial(particles,spm.get_species());
    }
    else if(output_type==AVG){
        print_avgs();
    }
    else if(output_type==POSITION){
        for(auto p=particles.begin();p!=particles.end();++p){
            p->second.print();
        }
    }
}

void Smol::print_avgs(){
    /* TODO: This function is not safe (can we guarantee that species types will be integers starting at 0? I don't think so.) */
    auto specs = spm.get_species();
    int nspecs = spm.get_nspecs();
    /*for(auto sp=specs.begin();sp!=specs.end();++sp){
        std::cout << sp->second.get_name() << "   ";
    }
    std::cout << std::endl;*/
    int *nmols = (int *)calloc(nspecs,sizeof(int));
    for(auto p=particles.begin();p!=particles.end();++p){
        ++nmols[p->second.get_type()];
    }
    for(int j=0;j<nspecs;++j){
        std::cout << nmols[j] << "   ";
    }
    std::cout << std::endl;
    
}


void Smol::diffuse_particle(double dt,long id){
    particles.at(id).diffuse(dt,rng);
}

double Smol::dist(Particle p1,Particle p2){
    std::array<double,3> pos1 = p1.get_pos();
    std::array<double,3> pos2 = p2.get_pos();
    double sigma = p1.get_sigma()+p2.get_sigma();
    return sqrt(pow(pos1[0]-pos2[0],2)+pow(pos1[1]-pos2[1],2)+pow(pos1[2]-pos2[2],2))-sigma;
}

/* Computes distance and index of closest molecule to a subset. */
void Smol::subset_d_nn(subset *subs,nborsmap& nbors,nnmap& nn){
	subs->d_closest = INFINITY;
	double dtemp_t;
	auto nnvec = subs->p_ids;
	int nind,nind_t;
	for(long i=0;i<nnvec.size();++i){
		nind_t = nnvec[i];

        dtemp_t = get_d_gn(nind_t,nnvec.size()-1,nbors,nn);
		if(dtemp_t<subs->d_closest){
			subs->d_closest = dtemp_t;
			subs->ind_closest = get_gn(nind_t,nnvec.size()-1,nbors,nn);
		}
	}
}


/* Sorts particles into subsets. Each subset will be an element in the vector 'subsets'.
 *
 */
double Smol::sort_particles_bf2(std::vector<subset>& subsets,std::vector<long> ids){
    
    double **dists = (double **)malloc(ids.size()*sizeof(double*));
    for(int i=0;i<ids.size();++i){
        dists[i] = (double *)malloc(ids.size()*sizeof(double));
    }
    
    double **d12 = (double **)malloc(ids.size()*sizeof(double*));
    for(int i=0;i<ids.size();++i){
        d12[i] = (double *)malloc(2*sizeof(double));
    }
    
    int **ind = (int **)malloc(ids.size()*sizeof(int*));
    for(int i=0;i<ids.size();++i){
        ind[i] = (int *)malloc(2*sizeof(int));
    }
    
    /* Compute all distances.*/
    for(int i=0;i<ids.size();++i){
        for(int j=i+1;j<ids.size();++j){
            dists[i][j] = get_time_step(particles.at(ids[i]),particles.at(ids[j]));
            dists[j][i] = dists[i][j];
        }
    }
    
     /* Save the distance to the nearest and second-nearest neighbors in
     * d12. Indices in ind. */
    
    for(int i=0;i<ids.size();++i){
        double d1=INFINITY,d2=INFINITY;
        int i1=-1,i2=-1;
        double dtemp;
        for(int j=0;j<ids.size();++j){
            if(j!=i){
                dtemp = dists[i][j];
                if(dtemp<d1){
                    i2 = i1;
                    i1 = j;
                    d2 = d1;
                    d1 = dtemp;
                }
                else if(dtemp<d2){
                    i2 = j;
                    d2 = dtemp;
                }
            }
        }
        d12[i][0] = d1;
        d12[i][1] = d2;
        ind[i][0] = i1;
        ind[i][1] = i2;
       // std::cout << i1 << " " << i2 << " " << d1 << " " << d2 << std::endl;
    }
    
    
    int *already_sorted = (int *)calloc(ids.size(),sizeof(int));
    double closest_temp = INFINITY;
    /* Find pairs of nearest neighbors and molecules that are updated independently. */
    subset subs_temp;
    for(int i=0;i<ids.size();++i){
        if(already_sorted[i]==0){
            for(int j=i+1;j<ids.size();++j){
                if(already_sorted[j]==0){
					//std::cout << "N_" << i << "=" << ind[i][1] << ", N_" << j << "=" << ind[j][1] << std::endl;
                    /* Molecules are nearest neighbors. */
                    if(ind[i][0]==j && ind[j][0]==i){
						//std::cout << "Nearest neighbors..." << std::endl;
                        subs_temp.p_ids = {ids[i],ids[j]};
                        if(d12[i][1]<d12[j][1]){
                            subs_temp.d_closest = d12[i][1];
                            subs_temp.ind_closest = ind[i][1];
                        }
                        else{
                            subs_temp.d_closest = d12[j][1];
                            subs_temp.ind_closest = ind[j][1];
                        }
                        
                        already_sorted[i] = 1;
                        already_sorted[j] = 1;
                    }
                }
            }
            if(already_sorted[i]==0){
                /* Molecule with id 'i' is updated as a single molecule. */
                subs_temp.p_ids = {ids[i]};
                subs_temp.d_closest = d12[i][0];
                subs_temp.ind_closest = ind[i][0];
                already_sorted[i] = 1;
            }
            if(subs_temp.d_closest<closest_temp){
                closest_temp = subs_temp.d_closest;
            }
            subsets.push_back(subs_temp);
        }
    }
    
    for(int i=0;i<ids.size();++i){
        free(dists[i]);
    }
    free(dists);
    
    for(int i=0;i<ids.size();++i){
        free(d12[i]);
    }
    free(d12);
    
    for(int i=0;i<ids.size();++i){
        free(ind[i]);
    }
    free(ind);
    
    free(already_sorted);
    
    return closest_temp;
}





std::array<double,3> Smol::meso2micro(Particle& p){
    return spm.meso2micro(p);
}



void Smol::print_nbors(nborsmap& nbors,nnmap& nn){
    std::cout << "-------------" << std::endl;
    for(auto s1=nbors.begin();s1!=nbors.end();++s1){
        std::cout << "Id: " << s1->first << std::endl;
        for(auto s2=s1->second.begin();s2!=s1->second.end();++s2){
            std::cout << "---->Id: " << s2->first << ", distance: " << s2->second << std::endl;
        }
        std::cout << "Closest neighbor: " << nn.at(s1->first)[0] << std::endl;// " at distance: " << nbors.at(s1->first)[nn[0]].d << std::endl;
        std::cout << "Second closest neighbor: " << nn.at(s1->first)[1] << std::endl;
    }
}
