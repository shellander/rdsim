#ifndef RDSIM_LIBS_SOLVERS_SOLVER
#define RDSIM_LIBS_SOLVERS_SOLVER

#include <iostream>
#include <stdexcept>
#include <vector>
#include <math.h>

#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"

#ifdef __MACH__
#include <mach/mach_time.h>
#endif




class Solver{
protected:
    
    int input_type;
    int output_type;
    int debug;//To turn off, set to 0.
    
    std::string name;
    
    double t; /* Current time. */
    double Tfinal; /* Final time. */
    
    gsl_rng *rng;
    
    
    
    
    
public:

	enum input{RDSIM=0,PYURDME=1};
	enum output{AVG=0,POSITION=1,HDF5=2};
	enum DEBUG{D_OFF=0,D_MED=2,D_MAX=3};

    Solver(){}
    void initialize_rng();
    void initialize_rng(unsigned int seed);
    
    virtual void run(double T) = 0; /* Advances the state to time T. */
    virtual void initialize() = 0; /* Initializes the solver. */
    virtual void print_state() = 0; /* Prints the state of the system. */
    
    void set_Tfinal(double T);
    double get_t(){return t;}
    void set_t(double t_in){t=t_in;}
    double get_Tfinal();
    
};



#endif
