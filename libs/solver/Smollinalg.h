#ifndef STOCHSIM_LIBS_SOLVERS_SMOL_SMOLLINALG
#define STOCHSIM_LIBS_SOLVERS_SMOL_SMOLLINALG

#include <array>

class SmolLinAlg{
public:
    static void rotation(double *v,double *z,double theta,double r);
    static double dot(double *v1,double *v2);
    static double dot(std::array<double,3>& v1,std::array<double,3>& v2);
    static double dist3(double *v1,double *v2);
    static double dist3(std::array<double,3>& v1,std::array<double,3>& v2);
    static double* cross(double *v,double *w);
    static double norm(double *v);
    static double norm(std::array<double,3> v);
    static void normalize(double *v);
    static void normalize(std::array<double,3>& v);
    static std::array<double,3> vec_diff(std::array<double,3> v1,std::array<double,3> v2);
};

#endif
