#ifndef RDSIM_LIBS_SOLVER_ERROR
#define RDSIM_LIBS_SOLVER_ERROR

#include <iostream>

class SolverError{
    
    std::string msg;
    
public:
    
    SolverError(){msg = "Unknown solver error.";};
    SolverError(std::string msg_err){msg = msg_err;print_msg();}
    void print_msg(){std::cerr << msg << std::endl;}
};




#endif
