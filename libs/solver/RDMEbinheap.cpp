#include "RDMEbinheap.h"
#include <cmath>
#include <iostream>

int RDMEbinheap::first_child(int index){
    return 2*index+1;
}

int RDMEbinheap::second_child(int index){
    return 2*index+2;
}

int RDMEbinheap::parent(int index){
    return floor(((double)index-1.0)/2.0);
}

void RDMEbinheap::heap_swap_elems(int index1,int index2){
    iter_swap(elems.begin()+index1,elems.begin()+index2);
}

void RDMEbinheap::heap_move_up(int index){
    heap_swap_elems(index,parent(index));
}

void RDMEbinheap::heap_sift_down(int index,bool (*compare)(tent_event*,tent_event*)){
    int N = (int)(elems.size());
    int c1,c2,minindex;
    c1 = first_child(index);
    c2 = second_child(index);
    if(c2>N-1){
        if(c1>N-1){
            return;
        }
        else{
            minindex = c1;
        }
    }
    else{
        if(compare(&elems[c1],&elems[c2])){
            minindex = c1;
        }
        else{
            minindex = c2;
        }
    }
    if(compare(&elems[minindex],&elems[index])){
        heap_swap_elems(index,minindex);
        heap_sift_down(minindex,compare);
    }
    
}

void RDMEbinheap::heap_insert(tent_event new_elem){
    elems.push_back(new_elem);
    int N = (int)(elems.size());
    int index = N-1;
    while(index>0 && compare_events2(&new_elem,&elems[parent(index)])){
        heap_move_up(index);
        index = parent(index);
    }
}

void RDMEbinheap::heap_delete(int index,bool (*compare)(tent_event*,tent_event*)){
    int N = (int)(elems.size());
    elems[index] = elems[N-1];
    elems.erase(elems.end()-1);
    heap_sift_down(index,compare);
}

void RDMEbinheap::print_heap(){
    int N = (int)(elems.size());
    for(int i=0;i<N;i++){
        std::cout << elems[i].t << std::endl;
    }
}

void RDMEbinheap::unroll_heap(bool (*compare)(tent_event*,tent_event*)){
    while((int)(elems.size())>0){
       std::cout << elems[0].t << std::endl;
        heap_delete(0,compare);
    }
    
}

bool RDMEbinheap::compare_events2 (tent_event *e1,tent_event *e2) {
    return e1->t<e2->t;
}

void RDMEbinheap::clear_diff(int index){
    int N = (int)(elems.size());
    for(int i = 0;i<N;i++){
        if(elems[i].type==-1){
            if(elems[i].reactants[0]==index){
                heap_delete(i,compare_events2);                
                i--;
                N--;
            }
        }
    }
}

void RDMEbinheap::clear_diss(int index){
    int N = (int)(elems.size());
    for(int i = 0;i<N;i++){

        if(elems[i].type==0){
            if(elems[i].reactants[0]==index){
                heap_delete(i,compare_events2);
                i--;
                N--;
            }
        }

    }
}

void RDMEbinheap::clear_assoc(int index){
    int N = (int)(elems.size());
    for(int i = 0;i<N;i++){
        if(elems[i].type==1){
            if(elems[i].reactants[0]==index || elems[i].reactants[1]==index){
                heap_delete(i,compare_events2);
                i--;
                N--;
            }
        }
    }
}

RDMEbinheap::tent_event RDMEbinheap::pop_event(){
	tent_event temp = elems[0];
	heap_delete(0,compare_events2);
	return temp;
}

void RDMEbinheap::print_summary(){
	int temp[] = {0,0,0};
	for(int type=-1;type<2;++type){
		for(auto elem=elems.begin();elem!=elems.end();++elem){
			if(elem->type==type){
				temp[type+1]++;
			}
		}
	}
	std::cout << "Diffusion events on stack: " << temp[0] << std::endl;
	std::cout << "Dissociation events on stack: " << temp[1] << std::endl;
	std::cout << "Association events on stack: " << temp[2] << std::endl; 
}

//void RDMEbinheap::adjust_index(int index){
//    int N = (int)(elems.size());
//    for(int i = 0;i<N;i++){
//        if(elems[i].reactants[0]>index){
//            elems[i].reactants[0]--;
//        }
//        if(elems[i].type==1){
//            if(elems[i].reactants[1]>index){
//                elems[i].reactants[1]--;
//            }
//        }
//    }
//}





