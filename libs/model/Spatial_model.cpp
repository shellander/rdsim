#include <iostream>
#include <array>

#include "Spatial_model.h"

Spatial_model::Spatial_model(std::string name_in,std::vector<Reaction> reacs_in,std::string mesh_file_in,char* urdme_input_file): Model(name_in,reacs_in,0.0),idg{Id_generator::get_instance()},mesh{mesh_file_in}{
  
    model = read_model(urdme_input_file);
    model->infile = urdme_input_file;
    
    if (model == NULL){
        printf("Fatal error. Couldn't load model file or currupt model file.");
    }
    
    mesh.voxel_boundaries(model);
    mesh.set_connectivity_matrix(model);
    mesh.set_meso_mesh(model);
    
    set_volume(mesh.get_vol());
}



double Spatial_model::get_init_t(){
    return model->tspan[0];
}


std::vector<double> Spatial_model::get_time_points(){
	std::vector<double> sample_t;
	for(int j=0;j<model->tlen;++j){
		sample_t.push_back(model->tspan[j]);
	}
	return sample_t;
}


long Spatial_model::add_particle(std::array<double,3> pin,double sigma,double D,std::string name_in){
    Particle p{pin,sigma,D,name_in,idg};
    particles.insert({p.get_id(),p});
    return p.get_id();
}


long Spatial_model::add_particle(std::array<double,3> pin,Species& sin){
//    Particle p{pin,sin.get_sigma(),sin.get_D(),sin.get_name(),idg};
    Particle p{pin,sin,idg};
    //std::cout << "Inserting particle with pid: " << p.get_id() << std::endl;
    particles.insert({p.get_id(),p});
    return p.get_id();
}


std::array<double,3> Spatial_model::meso2micro(Particle& p){
    return mesh.meso2micro(p.get_voxel(),p.get_dim());
}

std::array<double,3> Spatial_model::meso2micro(int voxel,int dim){
    return mesh.meso2micro(voxel,dim);
}

//int Spatial_model::micro2meso(Particle& p){
//    return mesh.micro2meso(p.get_pos(),p.get_voxel(),get_nspecs());
//}


void Spatial_model::print_particles(){
    for(auto p=particles.begin();p!=particles.end();++p){
        p->second.print();
    }
}

int Spatial_model::sample_voxel_uniform(gsl_rng *rng){
//    std::cout << "Volume total: " << get_volume() << std::endl;
    double r = gsl_rng_uniform(rng);
    while(r==1){
        r = gsl_rng_uniform(rng);
    }
    double s_vol = gsl_rng_uniform(rng)*get_volume();
    double test_vol = 0.0;
    
    auto voxels = mesh.get_voxels();
    
//    for(auto v=voxels.begin();v!=voxels.end();++v){
//        test_vol += v->vol;
//    }
//    std::cout << "Volume total: " << get_volume() << ", test_vol=" << test_vol <<  std::endl;
    
    double acc_vol = 0.0;
    int index = 0;
//    return floor(r*voxels.size());
    for(auto v=voxels.begin();v!=voxels.end();++v){
//        std::cout << "Sampled vol: " << s_vol << ", acc_vol=" << acc_vol << std::endl;
        
        acc_vol += v->vol;
        
        if(acc_vol>=s_vol){
//            std::cout << index << "  " <<  v->vol << std::endl;
            return index;
        }
        ++index;
    }
    return -1;//Search failed.
}

std::array<double,3> Spatial_model::sample_mesh_uniform(gsl_rng *rng,int dim){
    
    return mesh.sample_mesh_uniform(rng,dim);
}



