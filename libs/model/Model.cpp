#include <iostream>
#include <vector>


#include "Model.h"

/* Creates model with reaction in reacs_in, and adds the necessary species. */

Model::Model(std::string name_in,std::vector<Reaction> reacs_in,double vol){
    name = name_in;
    volume = vol;
    reaction_id = 0;
    specs_id = 0;
    for(auto it = reacs_in.begin();it!=reacs_in.end();++it){
        add_reaction(*it);
    }
    
}


std::vector<long> Model::add_species(std::vector<Species> s){
    bool duplicate;
    std::vector<long> spids;
    int temp_id;
    for(auto ss=s.begin();ss!=s.end();++ss){
        duplicate=false;
        for(auto spec=specs.begin();spec!=specs.end();++spec){
            if(spec->second==*ss){
                temp_id = spec->second.get_type();
                duplicate = true;
                break;
            }
        }
        if(!duplicate){
            ss->set_type(specs_id);
            specs.insert({specs_id,*ss});
            temp_id = specs_id;
            ++specs_id;
        }
        spids.push_back(temp_id);
    }
    return spids;
}


void Model::add_reaction(Reaction r){
    
    /* Check if reaction with that name already exists. */
    for(auto reac=reactions.begin();reac!=reactions.end();++reac){
        if(r.get_name().compare(reac->second.get_name())==0){
            throw ModelError("Reaction error: Trying to add duplicate reaction.");
        }
    }
    
    /* Check if species exist. If not, add them. */
    std::vector<Species> reactants = r.get_reactants();
    std::vector<Species> products = r.get_products();
    auto nids = add_species(reactants);
    for(int i=0;i<(int)(nids.size());++i){
        if(nids[i] != -1){
            r.set_reactant_type(i,nids[i]);
        }
    }
    nids.resize(0);
    nids = add_species(products);
    for(int i=0;i<(int)(nids.size());++i){
        if(nids[i] != -1){
            r.set_product_type(i,nids[i]);
        }
    }
    
//    r->set_type(reaction_id);
    reactions.insert({reaction_id,r});
    ++reaction_id;
}

void Model::add_reaction(std::vector<Reaction> reacs_in){
    for(auto r=reacs_in.begin();r!=reacs_in.end();++r){
        add_reaction(*r);
    }
}


/* O(N^2) algorithm for comparing two vectors of species. */ 


bool Model::compare_species(std::vector<Species> s1,std::vector<Species> s2){
	
	/* First check that vectors are of the same length. */
	if(s1.size()!=s2.size()) return false;
	
    bool el_exists;
    int i=0,j=0;
    for(auto se1=s1.begin();se1!=s1.end();++se1){
        el_exists = false;
        for(auto se2=s2.begin();se2!=s2.end();++se2){
            if(*se1==*se2){
                el_exists = true;
                s1.erase(se1);
                s2.erase(se2);
                --se1;
                --se2;
                break;
            }
            ++j;
        }
        if(!el_exists) return false;
		++i;
		j = 0;
    }
    return true;
}

/* If the reaction is reversible, return true. */
bool Model::reversible(Reaction rin){
    for(auto rs=reactions.begin();rs!=reactions.end();++rs){
        if(compare_species(rin.get_products(),rs->second.get_reactants()) && compare_species(rin.get_reactants(),rs->second.get_products())){
            return true;
        }
    }
    return false;
}


/* TODO: FIX THIS FUNCTION. */

/* Check if vector of species 'spin' match reaction 'rin' */
bool Model::rmatch(std::vector<Species> spin,Reaction rin){
	
//	if(spin.size()!=rin.get_nreacs()) return false;
	
    return compare_species(spin,rin.get_reactants());
    
//	return false;
}



std::vector<long> Model::get_tentative_reactions(std::vector<long> stypes){
//    auto spcs = spm.get_species();
    std::vector<Species> spcs_subs;
    unsigned int nst = stypes.size();
    if(nst<1 || nst>2){
        std::cout << "Length of species vector must be 1 or 2." << std::endl;
        /* TODO: Throw error here. */
    }
    int type_temp;
    for(unsigned int i=0;i<nst;++i){
        spcs_subs.push_back(specs.at(stypes[i]));
    }

    bool match = false;
    
    std::vector<long> rids;
    
    for(auto r=reactions.begin();r!=reactions.end();++r){
        match = rmatch(spcs_subs,r->second);
        if(match){
            rids.push_back(r->first);
        }
    }
    return rids;
}


/* Model does not know anything about particle ids. */
//bool Model::rmatch(std::vector<long> pids){
//    std::vector<Species> specs_subs;
//    for(int i=0;i<(int)(pids.size());++i){
//        specs_subs.push_back(specs.at(pids[i]));
//    }
//    
//    return false;
//}


/* Return the reaction rate of the opposite reaction for a reversible reaction. If the reaction is not reversible, return -1. */
double Model::get_rev_rate(Reaction rin){
    for(auto rs=reactions.begin();rs!=reactions.end();++rs){
        if(compare_species(rin.get_products(),rs->second.get_reactants()) && compare_species(rin.get_reactants(),rs->second.get_products())){
            return rs->second.get_rate();
        }
    }
    return -1;
}

void Model::print(){
    std::cout << "***************" << std::endl;
    std::cout << "Species: " << std::endl;
    std::cout << "***************" << std::endl;
    for(auto s=specs.begin();s!=specs.end();++s){
        s->second.print();
        std::cout << "---------------" << std::endl;
    }
    std::cout << "***************" << std::endl;
    std::cout << "Reactions: " << std::endl;
    std::cout << "***************" << std::endl;
    for(auto r=reactions.begin();r!=reactions.end();++r){
        r->second.print();
        if(reversible(r->second)) std::cout << "Reaction is reversible." << std::endl;
        else std::cout << "Reaction is irreversible." << std::endl;
        std::cout << "---------------" << std::endl;
    }
}



///* Initializes basic species. */
///* Sets the parameters needed for pure microscale simulation. Note that mesoscopic parameters can only be set together with information about the mesh. */
//void Model::add_species(double D,double sigma,int init_val,int dim,std::string name){
//    Species spec = Species(D,sigma,init_val,dim,name);
//    specs.push_back(spec);
//}

