#include <iostream>

#include "Particle.h"

Particle::Particle(std::array<double,3> pin,Species sp,Id_generator& idg){
    set_vec1(1.0,0.0,0.0);
    set_vec2(0.0,1.0,0.0);
    set_vec3(0.0,0.0,1.0);
    active = true;
    set_pos(pin[0],pin[1],pin[2]);
    sigma = sp.get_sigma();
    D = sp.get_D();
    name = sp.get_name();
    dim=sp.get_dim();
    type = sp.get_type();
    sorted_flag = false;
    meso_micro = sp.get_scale();
    clock = 0.0;
    unique_id = idg.get_id();
    updated = true;
}

void Particle::diffuse(double dt,gsl_rng *rng){
    prev_pos[0] = pos[0];
    prev_pos[1] = pos[1];
    prev_pos[2] = pos[2];
    double sd = sqrt(2*D*dt);
    double r1,r2,r3;
    r1 = sd*gsl_ran_gaussian_ziggurat(rng,1.0);
    r2 = sd*gsl_ran_gaussian_ziggurat(rng,1.0);
    r3 = sd*gsl_ran_gaussian_ziggurat(rng,1.0);
    pos[0] += r1*vec1[0]+r2*vec2[0]+r3*vec3[0];
    pos[1] += r1*vec1[1]+r2*vec2[1]+r3*vec3[1];
    pos[2] += r1*vec1[2]+r2*vec2[2]+r3*vec3[2];
}


void Particle::print(){
    std::cout << "" << pos[0] << "," << pos[1] << "," << pos[2] << "," << unique_id << "," << type << "," << clock << "," << meso_micro << std::endl;//"), " << ", id=" << unique_id << std::endl;
}
