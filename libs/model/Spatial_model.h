#ifndef RDSIM_LIBS_MODEL_SPATIALMODEL
#define RDSIM_LIBS_MODEL_SPATIALMODEL

#include <unordered_map>
#include <fstream>

#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"

#include "Model.h"
#include "Id_generator.h"
#include "Particle.h"
#include "Mesh.h"

#include "urdmemodel.h"
#include "read_matfile.h"
#include "json.hpp"

class Spatial_model: public Model{
private:
    /* The model class saves the initial state. This doesn't change, but is merely copied to the solver. It is useful to keep the initial state here fixed, as we may want to run multiple simulations with the same initial conditions. */
    std::unordered_map<long,Particle> particles;
    Id_generator& idg;
//    void print_particle(long id);
    FEM_mesh mesh;
    urdme_model *model;
    
public:
    
    
    
    std::vector<FEM_mesh::plane>& get_planes(){return mesh.get_planes();}
    std::vector<FEM_mesh::voxel>& get_voxels(){return mesh.get_voxels();}
   
    double get_init_t();
    std::vector<double> get_time_points();
    double get_vol(){return mesh.get_vol();}
    
    Spatial_model(std::string name_in,std::vector<Reaction> reacs_in,std::string mesh_file_in,char* urdme_input_file);
    
    int get_init_voxel(int spec_id,int voxel_id){return model->u0[voxel_id*model->Mspecies+spec_id];}
    
    int num_voxels(){return mesh.num_voxels();}
    int voxel_bnd(int i){return mesh.is_bnd(i);}

    
    
    unsigned int num_particles(){return particles.size();}
    void clear_particles(){particles.clear();}
    
    void set_particle_voxel(long id,int voxel){particles.at(id).set_voxel(voxel);}
    int get_particle_voxel(long id){return particles.at(id).get_voxel();}
    
    void set_meso_micro(int meso_micro,int id){particles.at(id).set_meso_micro(meso_micro);}
    
    long add_particle(std::array<double,3> pin,double sigma,double D,std::string name_in);
    long add_particle(std::array<double,3> pin,Species& sin);
    void print_particles();
    
    void print_dual_voxel_matlab(int voxel){mesh.print_dual_voxel_matlab(voxel);}
    
    void print_tet(int i){mesh.print_tet(i);}
    void tetrahedron_randunif(int i, double *ru){mesh.tetrahedron_randunif(i,ru);}
    int tet_which_macro_element(int tetindex,double *x){return mesh.tet_which_macro_element(tetindex,x);}
    
    
    
    std::unordered_map<long,Particle> get_particles(){return particles;}
    void set_particle_pos(std::array<double,3> pin,long id){
        particles.at(id).set_pos(pin);
    }
    
    std::array<double,3> meso2micro(Particle& p);
    std::array<double,3> meso2micro(long pid){return meso2micro(particles.at(pid));}
    std::array<double,3> meso2micro(int voxel,int dim);
//    int micro2meso(Particle &p);
    int micro2meso(double *pos,int voxin){return mesh.micro2meso2(pos,voxin);}
    int micro2meso(std::array<double,3> pos,int voxin){double temp[]={pos[0],pos[1],pos[2]};return micro2meso(temp,voxin);}
    
    int sample_voxel_uniform(gsl_rng *rng);
    std::array<double,3> sample_mesh_uniform(gsl_rng *rng,int dim);
    
    void initial_value_add_one(int id){specs.at(id).initial_value_add_one();}
};

#endif
