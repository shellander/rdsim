#include "Timer.h"

void Timer::start(){
    if(!running){
        running = true;
        double temp = clock();
        last_start = temp;
        tstart.push_back(temp);
    }
    else{
        std::cerr << "Timer is already running." << std::endl;
    }
    
}

void Timer::stop(){
    if(!running){
        std::cerr << "Timer has not been started yet. Cannot stop." << std::endl;
    }
    else{
        running = false;
        double temp = clock();
        tstopped.push_back(temp);
        t_total += (temp-last_start)/(double)CLOCKS_PER_SEC;
        
    }
    
}

void Timer::lap(){
    clock_t temp = clock();
    tstopped.push_back(temp);
    tstart.push_back(temp);
}

double Timer::total_time(){
    if(!running){
        return t_total;
    }
    else{
        double temp = clock();
        return t_total+(temp-last_start)/(double)CLOCKS_PER_SEC;
    }
    
}
