#ifndef RDSIM_LIBS_SOLVER_MESH
#define RDSIM_LIBS_SOLVER_MESH


#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <vector>
#include <array>
#include <iostream>
#include <unordered_map>

#include "H5Cpp.h"


#include "Particle.h"
#include "urdmemodel.h"


//#include "mesh.h"
//#include "structs.h"
//#include "utils.h"
#define tr 0.33333333333333333333

/* The following structures represent (extented) triangles and tetrahedra.
 It contains information about the primal mesh as well as the dual
 cells. */

class FEM_mesh{
    
public:
    
    typedef struct neighbor{
        int vox;
        double D;
    }neighbor;
    
    typedef struct voxel {
        int id;
        double node[3];
        int num_conn;
        std::vector <neighbor> neighbors;
        double vol;
        int sd;
        double totalD;
        int isbnd;
    }voxel;
    
    
    typedef struct plane{
        int id;
        double p[3];
        double v1[3];
        double v2[3];
        double n[3];
        int cn;
        int isbnd;
        std::vector<int> type;
        
    }plane;
    
private:
    typedef struct{
        int id;
        std::vector<int> cells;
    }vertex;

    typedef struct{
        
        /* Points in xx order. */
        int v1p;
        double v1[3];
        
        int v2p;
        double v2[3];
        
        int v3p;
        double v3[3];
        
        /* Area of primal triangle*/
        double area;
        
        /* Centroid */
        double c[3];
        
        /* The volume of the contribution to the dual element
         associated with the vertex. */
        double a1;
        double a2;
        double a3;
        
        /* The normals and length of the edges in the dual element. The normals point
         from point 1 towards 2 etc (dictated by the name of the normal). */
        double n12[3];
        double l12;
        
        double n13[3];
        double l13;
        
        double n23[3];
        double l23;
        
    }triangle;


    typedef struct{
        
        /* Points in xx order (primal mesh). */
        int v1p;
        double v1[3];
        int v2p;
        double v2[3];
        int v3p;
        double v3[3];
        int v4p;
        double v4[3];
        
        /* Volume of primal tetrahedron */
        double vol;
        
        /* Centroid of tetrahedron. This is a point in all the planes
         defining the dual element. */
        double c[3];
        
        /* The volume of the contribution to the dual element
         associated with the vertex. */
        
        double vol1;
        double vol2;
        double vol3;
        double vol4;
        
        /* The normals and area of the facets in the dual element. The normals point
         from point 1 towards 2 etc (dictated by the name of the normal). */
        double n12[3];
        double a12;
        
        double n13[3];
        double a13;
        
        double n14[3];
        double a14;
        
        double n23[3];
        double a23;
        
        double n24[3];
        double a24;
        
        double n34[3];
        double a34;
        
        /* Frequently, we will need to calulate the barycentric coordinates of
         a point inside the tetrahedron. Thus, we precompute and store
         the inverse of the matrix. */
        double T[3][3];
        
        
    }tetrahedron;
    
    
    
    

    
    std::vector<plane> boundaries;
    std::vector<voxel> voxels;
    
    /* Mesh file in HDF5 format. */
    H5::H5File mesh_file;
    
    /* Vertex coordinates (primal mesh) (3xNcells). */
    int Ncells;
    double *p;
    /* Boundary elements (primal mesh) (3xntri) */
    int ntri;
    int *e;
    /* Tetrahedra (index into p in primal mesh) (4xntet) */
    int ntet;
    int *t;
    
    int *vertex2dof;
    int *dof2vertex;
    /* Total volume of the domain. */
    double vol;
    
    /* Internal mesh format */
    vertex **vertices;
//    std::array<tetrahedron> tets;
    tetrahedron **tets;
    triangle **bnd;
    
    
    /* Connectivity matrix (needed by micro2meso). */
    size_t *jcK;
    size_t *irK;
    
    size_t *jcD;
    size_t *irD;
    
    /* Which tetrahedra does a vertex belong to? */
    double *prv2t;
    size_t *irv2t;
    size_t *jcv2t;
    
    /* Which boundary elements does a vertex belong to?*/
    double *prv2e;
    size_t *irv2e;
    size_t *jcv2e;
    
    /* Local mesh size */
    double *h;
    
   
    
    /* Simple geometry */
    double norm(double *x);
    void normalize(double *y);

    /* Initialize primal/dual mesh format */
    void mesh_primal2dual();
    void tet_index_to_internal();
    void tri_index_to_internal();
    void primal2dualtet(tetrahedron *tet);
    void primal2dualtri(triangle *tri);
    void tet_initialize_T(tetrahedron *tet);

    
    
    
    /* Utility routines related to the geometry of tetrahedra/triangles */
    void bary2tetpoint(double *a, tetrahedron *tet, double *p);
    void bary2tripoint(double *a, triangle *tri, double *p);
    int  tet_inside(tetrahedron *tet, double *x,double *minbary);
    int  tri_inside(triangle *tri,double *x,double *minbary);


    void tetpoint2bary(tetrahedron *tet,double *x,double *xb);
    void tripoint2bary(triangle *tri,double *p,double *pb);
    void triangle_randunif(triangle *tri, double *ru);
    int tri_which_macro_element(triangle *tri,double *x);


//    double triangle_area(tetrahedron *tet);
    double triangle_area(triangle *tri);
    double tetrahedron_vol(tetrahedron *tet);

    double dot(double *x,double *y);
    void vec_diff(double *v1,double *v2,double *out);
    void cross_mesh(double *v,double *x, double *y);
    double quadarea(double *v1,double *v2,double *v3, double *v4,double *n);
    double triarea(double *v1,double *v2,double *v3,double *n);

    /* These functions could be static. */
    double* tri_surfnormal(int *tri,double *p);
    int cmp_point(double *v1,double *v2);
    void project(double *p,triangle *tri);
    int nn(double *pos, int pv,int spec,int Mspecies);
    double distance(double *x,double *y);
    void trinormal(triangle *tri,double *n);
    double* tet_randunif(int *tet,double *p);
    void vec_in_plane(double *normal, double *v1, double *v2);
    
    /* Random sampling */
    
    void tetrahedron_randunif(tetrahedron *tet, double *ru);
    
    void read_p(H5::H5File mesh_file);
    void read_t(H5::H5File mesh_file);
    void read_bnd(H5::H5File mesh_file);
    void read_vertex_to_cell(H5::H5File *mesh_file,H5::DataSet *dataset);
    void read_vertex_to_dof(H5::H5File mesh_file);
    void read_dof_to_vertex(H5::H5File mesh_file);
    
    void vertex_to_dof_p();
    void vertex_to_dof_t();
    void vertex_to_dof_vertices();
    void vertex_to_dof_e();
    
    
    void set_n(double *n,int i,int j,int tetindex);
    void reverse_vec(double *v);
//    void vec_diff(double *v1,double *v2,double *res)
    
    void check_inside_surrounding_cells(double *pos, vertex *vtx, int *out);
public:
    
    void tetrahedron_randunif(int i, double *ru);
    int tet_which_macro_element(int tetindex,double *x);
    bool inside_domain(double *pos);
    
    void print_tet(int i);
    void print_tri(triangle *tri);
    void print_vertex(vertex *vtx);
    void print_T(tetrahedron *tet);
    void print_dual_voxel_matlab(int voxel);
    
    
    FEM_mesh(std::string mesh_file_name);
    
    
    void voxel_boundaries(urdme_model *model);
    void set_meso_mesh(urdme_model *model);
    void set_connectivity_matrix(urdme_model *model);
    
    int is_bnd(int i){return voxels[i].isbnd;}
    
//    void meso2micro(int voxel_id);
    std::array<double,3> meso2micro(int voxin,int dimin);
    std::array<double,3> sample_mesh_uniform(gsl_rng *rng,int dimin);
    //int micro2meso(std::array<double,3> pos,int vox_init,int Mspecies);
    int micro2meso2(double* pos,int voxel_in);//Particle *particle, int Mspecies);
    
    /* Returns vector containing adjacent voxels. */
    std::vector<neighbor> get_neighbors(urdme_model *model,int voxel_id,int Mspecies);
    
    /* Returns total diffusion rate out of voxel 'voxel_id'. */
    double get_total_D(int voxel_id);
    
    /* Total volume of domain. */
    double get_vol(){return vol;}
    
    int num_voxels(){return Ncells;}
    
    std::vector<voxel>& get_voxels(){return voxels;}
    std::vector<plane>& get_planes(){return boundaries;}

};


#endif
