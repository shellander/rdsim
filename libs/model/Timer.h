#ifndef RDSIM_LIBS_MODEL_TIMER
#define RDSIM_LIBS_MODEL_TIMER

#include <iostream>
#include <vector>

#include <ctime>


class Timer{
    
    
private:
    std::vector<clock_t> tstart;
    std::vector<clock_t> tstopped;
    bool running;
    double t_total;
    double last_start;
    
public:
    Timer(){
        running = false;
        t_total = 0.0;
    }
    
    void start();
    void stop();
    void lap();
    double total_time();
    
    
};


#endif
