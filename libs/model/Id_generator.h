#ifndef RDSIM_LIBS_MODEL_IDGENERATOR
#define RDSIM_LIBS_MODEL_IDGENERATOR


class Id_generator{
private:
    long curr_id;
    //Id_generator(){curr_id=0;}
public:
    static Id_generator& get_instance(){
        static Id_generator idg;
        return idg;
    }
 Id_generator(){curr_id=0;}
//    Id_generator& get_instance(){
//        Id_generator idg;
//        return idg;
//    }
    long get_id(){
        return ++curr_id;
    }
};


#endif
