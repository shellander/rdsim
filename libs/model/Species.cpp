#ifndef RDSIM_LIBS_MODEL_REACTION
#define RDSIM_LIBS_MODEL_REACTION

#include <iostream>
#include <vector>
#include <string>


#include "Species.h"

Species::Species(const Species &sp){
    D = sp.D;
    mesoD = sp.mesoD;
    sigma = sp.sigma;
    dim = sp.dim;
    type = sp.type;
    name = sp.name;
    initial_value = sp.initial_value;
    meso_micro = sp.meso_micro;
    min_micro = sp.min_micro;
    sd = sp.sd;
    urdme_dof_nr = sp.urdme_dof_nr;
}

void Species::print(){
    std::cout << "Name: " << name << std::endl;
    std::cout << "Type: " << type << std::endl;
    std::cout << "sigma=" << sigma << std::endl;
    std::cout << "D=" << D << std::endl;
    std::cout << "Dimension: " << dim << std::endl;
    std::cout << "Initial value: " << initial_value << std::endl;
}


#endif
