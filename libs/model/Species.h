#ifndef RDSIM_LIBS_MODEL_SPECIES
#define RDSIM_LIBS_MODEL_SPECIES

#include <vector>

#include "ModelError.h"

class Species{

private:
    enum scale{meso=0,micro=1};
    double D,mesoD;
    double sigma;
    int dim;
    int type;
    std::string name;
    int initial_value;
    
    int urdme_dof_nr;
    
    
    /* 0 is meso, 1 is micro. */
    scale meso_micro;
    /* Minimum time a newly created mesoscopic molecule stays microscopic before it becomes mesoscopic. */
    double min_micro;
    
    /* A species can be restricted to some subdomains of space. If this vector is of length 0, then the species can diffuse everywhere. */
    std::vector<int> sd;

public:
    Species(const Species &sp);
    
    bool operator==(Species& rh){
        if(name.compare(rh.get_name())==0) return true;
        return false;
    }
    
    Species(double D_in,double sigma_in,int init_val,int dim_in,std::string name_in){
        if(D_in<0){throw ModelError("Species diffusion constant has to be positive. D="+std::to_string(D_in));}
        if(sigma_in<0){throw ModelError("Species radius has to be positive. sigma="+std::to_string(sigma_in));}
        if(dim_in<1 || dim_in>3){throw("Species dimension has to be 1,2, or 3. dim="+std::to_string(dim));}
        if(init_val<0){throw("Species initial value cannot be negative. Initial value="+std::to_string(init_val));}
        if(name_in.empty()){throw("Warning: Species name not specified.");}
        
        D = D_in;
        sigma = sigma_in;
        initial_value = init_val;
        dim = dim_in;
        name = name_in;
    }
    
    
    double get_D(){return D;}
    double get_meso_D(){return mesoD;}
    double get_sigma(){return sigma;}
    int get_dim(){return dim;}
    std::string get_name(){return name;}
    int get_type(){return type;}
    void set_type(int type_in){type = type_in;}
    int get_initial_value(){return initial_value;}
    scale get_scale(){return meso_micro;}
    double get_min_micro(){return min_micro;}
    
    void set_urdme_dof_nr(int ud){urdme_dof_nr = ud;}
    int get_urdme_dof_nr(){return urdme_dof_nr;}
    void initial_value_add_one(){++initial_value;}
    
    void print();

    
};



#endif
