#ifndef RDSIM_LIBS_MODEL_REACTION
#define RDSIM_LIBS_MODEL_REACTION

#include <vector>
#include <string>

#include "Species.h"
#include "ModelError.h"

/* This class defines a reaction. Note that we make assumptions about the units of the reaction rate. */

class Reaction{
    
public:
    enum reaction_t{birth=0,unimol,bimol};
    
private:
    std::string name;
    
    double k; /* Microscopic rate (k, units: bimol: m^3/s, unimol and birth: 1/s). */
    
    /* Arrays that hold list of reactants and products. */
    std::vector<Species> reactants;
    std::vector<Species> products;
    
    /* True if reaction is active, false otherwise. Can be used to turn reactions on and off mid-simulation. The default is true. */
    bool active;
    
    /* A reaction can be active in some subdomains only. */
    /* Default is all subdomains; if this vector is of length 0, then the reaction can occur everywhere. */
    std::vector<int> sd;
    
    reaction_t type;
    
public:
    
    
    
    Reaction(std::vector<Species> reactants_in,std::vector<Species> products_in,double k_in,std::string name_in);
    
    std::string get_name(){return name;}
    std::vector <Species> get_reactants(){return reactants;}
    std::vector <Species> get_products(){return products;}
    int get_type(){return type;}
    void set_reactant_type(int index,long type){reactants[index].set_type(type);}
    void set_product_type(int index,long type){products[index].set_type(type);}
//    void set_type(int type_in){type = type_in;}
    double get_rate(){return k;}
    
    bool isactive(){return active;}
    
    //bool rmatch(std::vector<int> pt);
    
    unsigned int get_nprods(){return products.size();}
    unsigned int get_nreacs(){return reactants.size();}
    
    void print();
};


#endif
