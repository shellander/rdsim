#include "Mesh.h"

FEM_mesh::FEM_mesh(std::string mesh_file_name){
    mesh_file = H5::H5File(mesh_file_name, H5F_ACC_RDONLY );
    
    read_vertex_to_dof(mesh_file);
    read_dof_to_vertex(mesh_file);
    read_p(mesh_file);
    
  
    
    
    read_t(mesh_file);
    
   // vertex_to_dof_t();
    
    read_bnd(mesh_file);
    
    
    vertex_to_dof_p();
    vertex_to_dof_vertices();
    vertex_to_dof_t();
    vertex_to_dof_e();
    
    
    H5::DataSet dataset;
    read_vertex_to_cell(&mesh_file,&dataset);
    
    
    
    /* Initialize the primal/dual mesh format. Do we need this for pure micro?? */
//    std::cout << "Initializing primal/dual mesh format.\n";
    mesh_primal2dual();
    
    mesh_file.close();
    
    
    
    
    
    
    
    

    
    //    cout << "Setting mesh mesh.\n";
    
    
    
    
    
    
    
    
    vol=0.0;
//    std::cout << "Ntets=" << ntet << std::endl;
    for(int i=0;i<ntet;++i){
        tets[i]->vol = tetrahedron_vol(tets[i]);
        vol += tets[i]->vol;
//        print_tet(tets[i]);
    }
//    std::cout << "Total volume is: " << vol << std::endl;
//    std::cout << "Number of vertices: " << Ncells << std::endl;
    
    
    
    
}


void FEM_mesh::vertex_to_dof_p(){
	std::unordered_map<int,std::array<double,3>> temp_holder;
	for(int i=0;i<Ncells;++i){
		temp_holder.insert({vertex2dof[i],{p[3*i],p[3*i+1],p[3*i+2]}});
	}
	std::array<double,3> tpos;
	for(int i=0;i<Ncells;++i){
		tpos = temp_holder.at(i);
		p[3*i] = tpos[0];
		p[3*i+1] = tpos[1];
		p[3*i+2] = tpos[2];
	}
}

void FEM_mesh::vertex_to_dof_vertices(){
	std::unordered_map<int,vertex*> temp_holder;
	for(int i=0;i<Ncells;++i){
		temp_holder.insert({vertex2dof[i],vertices[i]});
	}
	for(int i=0;i<Ncells;++i){
		vertices[i] = temp_holder.at(i);
	}
}

void FEM_mesh::vertex_to_dof_t(){
    
    for(int i=0;i<4*ntet;++i){
        t[i] = vertex2dof[t[i]];
    }
}

void FEM_mesh::vertex_to_dof_e(){
    for(int i=0;i<3*ntri;++i){
        e[i] = vertex2dof[e[i]];
    }
}


/* TODO: Make sure this function works. */
std::vector<FEM_mesh::neighbor> FEM_mesh::get_neighbors(urdme_model *model,int voxel_id,int Mspecies){
    
    std::vector<neighbor> nbors;
    
    size_t *jcD = model->jcD;
    size_t *irD = model->irD;
    double *prD = model->prD;
    
    
//    voxel temp;
    neighbor tn;
    int to_dof;
//    for(int i=0;i<Ncells;i++){
//        temp.id = i;
//        temp.node[0] = boundary[i].p[0];
//        temp.node[1] = boundary[i].p[1];
//        temp.node[2] = boundary[i].p[2];
        
        
        int dof = voxel_id*Mspecies;
        
//        temp.num_conn=0;
//        temp.vol = model->vol[i];
//        temp.sd = model->sd[i];
//        totalD = 0.0;
        nbors.resize(0);
//        temp.isbnd = boundary[i].isbnd;
    
        
        for (int k=jcD[dof]; k<jcD[dof+1]; k++) {
            
            
            to_dof=irD[k];
            if(to_dof!=dof){
//                temp.num_conn++;
                tn.vox = to_dof/Mspecies;
                tn.D = prD[k];///specs[0].D;
//                temp.totalD += tn.D;
                nbors.push_back(tn);
            }
            
        }
//        voxels.push_back(temp);
    
        //        printf("\n");
        //        printf("Ncells=%d,Mspecies=%d\n",Ncells,model->Mspecies);
//    }

    return nbors;
    
}


void FEM_mesh::read_p(H5::H5File mesh_file){
    
    H5::DataSet dataset = mesh_file.openDataSet("/mesh/p");
    H5::DataSpace dataspace = dataset.getSpace();
    /*
     * Get the number of dimensions in the dataspace.
     */
    int rank = dataspace.getSimpleExtentNdims();
    hsize_t dims_out[2];
    dataspace.getSimpleExtentDims( dims_out, NULL);
//    std::cout << "rank " << rank << ", dimensions " <<
//    (unsigned long)(dims_out[0]) << " x " <<
//    (unsigned long)(dims_out[1]) << std::endl;
    
    //    printf("ALLOC P\n");
    double *pin;
    pin=(double *)malloc(dims_out[0]*dims_out[1]*sizeof(double));
    //    double ptemp[dims_out[0]][dims_out[1]];
    
    dataset.read(pin,H5::PredType::NATIVE_DOUBLE);
    
    
    
    Ncells=dims_out[0];
    p=pin;
   /* for(int i=0;i<dims_out[0]*dims_out[1];i=i+3){
        if(p[i]!=p[i]){
            std::cout << "ERROR ERROR ERROR" << std::endl;
            exit(0);
        }
        std::cout << "p_read_" << i/3 << "=(" << p[i] << "," << p[i+1] <<"," << p[i+2] << ")" << std::endl;
    }*/
    // initialize vertices
    int nvox = dims_out[0];
    vertex **verticesin = (vertex **)malloc(nvox*sizeof(vertex *));
//    printf("number of voxels: %d\n",nvox);
    vertex *vtx;
    for (int i=0; i<nvox; i++){
        
        verticesin[i] = (vertex *)calloc(1,sizeof(vertex));
        vtx = verticesin[i];
        vtx->id = vertex2dof[i];
        vtx->cells.resize(0);
        //	vertices[i]->id = i;
    }
    vertices = verticesin;
    
}

void FEM_mesh::read_t(H5::H5File mesh_file){
    
    H5::DataSet dataset = mesh_file.openDataSet("/mesh/t");
    H5::DataSpace dataspace = dataset.getSpace();
    /*
     * Get the number of dimensions in the dataspace.
     */
    int rank = dataspace.getSimpleExtentNdims();
    hsize_t dims_out[2];
    dataspace.getSimpleExtentDims( dims_out, NULL);
//    std::cout << "rank " << rank << ", dimensions " <<
//    (unsigned long)(dims_out[0]) << " x " <<
//    (unsigned long)(dims_out[1]) << std::endl;
    
    int *tin;
    tin=(int *)malloc(dims_out[0]*dims_out[1]*sizeof(int));
    dataset.read(tin,H5::PredType::NATIVE_INT);
    
    ntet = dims_out[0];
    t=tin;
    
}

void FEM_mesh::read_vertex_to_dof(H5::H5File mesh_file){
    
    H5::DataSet dataset = mesh_file.openDataSet("/mesh/vertex2dof");
    H5::DataSpace dataspace = dataset.getSpace();
    /*
     * Get the number of dimensions in the dataspace.
     */
    int rank = dataspace.getSimpleExtentNdims();
    hsize_t dims_out[2];
    dataspace.getSimpleExtentDims( dims_out, NULL);
    
    int *v2dofin;
    v2dofin=(int *)malloc(dims_out[0]*sizeof(int));
    dataset.read(v2dofin,H5::PredType::NATIVE_INT);
    
    vertex2dof=v2dofin;
    
}

void FEM_mesh::read_dof_to_vertex(H5::H5File mesh_file){
    
    H5::DataSet dataset = mesh_file.openDataSet("/mesh/vertex2dof");
    H5::DataSpace dataspace = dataset.getSpace();
    /*
     * Get the number of dimensions in the dataspace.
     */
    int rank = dataspace.getSimpleExtentNdims();
    hsize_t dims_out[2];
    dataspace.getSimpleExtentDims( dims_out, NULL);
    
    int *dof2vin;
    dof2vin=(int *)malloc(dims_out[0]*sizeof(int));
    dataset.read(dof2vin,H5::PredType::NATIVE_INT);
    
    dof2vertex=dof2vin;
    
}



void FEM_mesh::read_bnd(H5::H5File mesh_file){
    
    H5::DataSet dataset = mesh_file.openDataSet("/mesh/boundaryfacets");
    H5::DataSpace dataspace = dataset.getSpace();
    /*
     * Get the number of dimensions in the dataspace.
     */
    int rank = dataspace.getSimpleExtentNdims();
    hsize_t dims_out[2];
    dataspace.getSimpleExtentDims( dims_out, NULL);
//    std::cout << "rank " << rank << ", dimensions " <<
//    (unsigned long)(dims_out[0]) << " x " <<
//    (unsigned long)(dims_out[1]) << std::endl;
    
    int *t;
    t=(int *)malloc(dims_out[0]*dims_out[1]*sizeof(int));
    dataset.read(t,H5::PredType::NATIVE_INT);
    
    /*for (int i=0;i<dims_out[0];i++){
     for (int j=0;j<dims_out[1];j++)
     cout << p[i+j*dims_out[0]] << " ";
     cout << "\n";
     }*/
    ntri = dims_out[0];
    e=t;
    
}

void FEM_mesh::read_vertex_to_cell(H5::H5File *mesh_file,H5::DataSet *dataset){
    *dataset = mesh_file->openDataSet("/mesh/vertex2cells");
    H5::DataSpace dataspace = dataset->getSpace();
    /*
     * Get the number of dimensions in the dataspace.
     */
    int rank = dataspace.getSimpleExtentNdims();
    hsize_t dims_out[2];
    dataspace.getSimpleExtentDims( dims_out, NULL);
//    std::cout << "rank " << rank << ", dimensions " <<
//    (unsigned long)(dims_out[0]) << " x " <<
//    (unsigned long)(dims_out[1]) << std::endl;
    
    
    int *ttemp;
    ttemp = (int *)malloc((int)dims_out[0]*(int)dims_out[1]*sizeof(int));
    //int ttemp[(int)dims_out[0]][(int)dims_out[1]];
    int M =(int)dims_out[0];
    int N =(int)dims_out[1];
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<N;j++)
        {
            ttemp[i*N+j] = 0;
        }
    }
    
    dataset->read(ttemp,H5::PredType::NATIVE_INT);
    
    vertex *vtx;
    for (int i=0; i<M;i++){
        
        vtx = vertices[vertex2dof[i]];
        for (int j=0;j<N;j++){
            //            printf("i=%d,j=%d,t_ij=%d\n",i,j,ttemp[i][j]);
            if(ttemp[i*N+j] >= 0){
                vertices[vertex2dof[i]]->cells.push_back(ttemp[i*N+j]);
                int endp = (int)(vertices[vertex2dof[i]]->cells.size())-1;
                //                printf("just pushed back: %d\n",mesh->vertices[i]->cells[endp]);
            }
        }
    }
    dataspace.close();
    //    dataset.close();
    
    //    printf("12\n");
    
    
}

/* Utility routines related to the primal and dual mesh. */



/* Initialize the variables and data structures relates to the
 dual mesh. This will also compute normals to the facets in
 the dual (so this need not be done again upon assembly) */

void FEM_mesh::mesh_primal2dual()
{
    
    int i;
    tet_index_to_internal();
    tri_index_to_internal();
    for (i=0; i<ntet; i++) {
        primal2dualtet(tets[i]);
    }
    for (i=0; i<ntri; i++) {
        primal2dualtri(bnd[i]);
    }
}

void FEM_mesh::tet_initialize_T(tetrahedron *tet)
{
    
    double *v1,*v2,*v3,*v4;
    double b[3][3];
    double a;
    
    int i,j,k,l,maxi;
    
    int perm[] = {0,1,2};
    
    for(i=0;i<3;i++) {
        for(j=0;j<3;j++) {
            b[i][j] = 0.0;
        }
    }
    b[0][0]=1.0;
    b[1][1]=1.0;
    b[2][2]=1.0;
    
    v1 = tet->v1;
    v2 = tet->v2;
    v3 = tet->v3;
    v4 = tet->v4;
    
    /* Assemble matrix. */
    double T[3][3];
    T[0][0]=v1[0]-v4[0];
    T[1][0]=v1[1]-v4[1];
    T[2][0]=v1[2]-v4[2];
    
    T[0][1]=v2[0]-v4[0];
    T[1][1]=v2[1]-v4[1];
    T[2][1]=v2[2]-v4[2];
    
    T[0][2]=v3[0]-v4[0];
    T[1][2]=v3[1]-v4[1];
    T[2][2]=v3[2]-v4[2];
    
    
    i = 0;
    j = 0;
    while(i<3 && j<3) {
        
        /* Find pivot in column j, starting in row i. */
        maxi = i;
        for(k=maxi+1;k<3;k++) {
            if(fabs(T[k][j])>fabs(T[maxi][j])) {
                maxi = k;
            }
        }
        if(T[maxi][j] != 0) {
            /* Swap rows i and maxi */
            if(i!=maxi) {
                int temp = perm[i];
                perm[i] = perm[maxi];
                perm[maxi] = temp;
                double row_temp[] = {T[i][0],T[i][1],T[i][2]};
                T[i][0] = T[maxi][0];
                T[i][1] = T[maxi][1];
                T[i][2] = T[maxi][2];
                T[maxi][0] = row_temp[0];
                T[maxi][1] = row_temp[1];
                T[maxi][2] = row_temp[2];
                
                double row_temp2[] = {b[i][0],b[i][1],b[i][2]};
                b[i][0] = b[maxi][0];
                b[i][1] = b[maxi][1];
                b[i][2] = b[maxi][2];
                b[maxi][0] = row_temp2[0];
                b[maxi][1] = row_temp2[1];
                b[maxi][2] = row_temp2[2];
            }
            
            /* Divide each element in row i by T[i][j]. */
            a = 1/T[i][j];
            for(k=0;k<3;k++) {
                T[i][k] *= a;
                b[i][k] *= a;
            }
            /* Eliminate */
            for(k=i+1;k<3;k++) {
                /* Subtract T[k][j]*row i from row k. */
                a = T[k][j];
                for(l=0;l<3;l++) {
                    T[k][l] -= a*T[i][l];
                    b[k][l] -= a*b[i][l];
                }
            }
            i+=1;
        }
        else {
            printf("Matrix not invertible.\n");
        }
        
        j+=1;
    }
    
    /* Backward substitution */
    a=T[1][2]/T[2][2];
    for (j=0; j<3; j++) {
        T[1][j]-=(a*T[2][j]);
        b[1][j]-=(a*b[2][j]);
    }
    
    
    a=T[0][2]/T[2][2];
    for (j=0; j<3; j++) {
        T[0][j]-=(a*T[2][j]);
        b[0][j]-=(a*b[2][j]);
    }
    
    a=T[0][1]/T[1][1];
    for (j=0; j<3; j++) {
        T[0][j]-=(a*T[1][j]);
        b[0][j]-=(a*b[1][j]);
    }
    
    /* Store inverse */
    for (j=0; j<3; j++) {
        tet->T[0][j]=b[0][j];
        tet->T[1][j]=b[1][j];
        tet->T[2][j]=b[2][j];
    }
    
}

void FEM_mesh::print_T(tetrahedron *tet)
{
    printf("[");
    for(int i = 0;i<3;i++)
    {
        for(int j = 0;j<3;j++)
        {
            printf("%e ",tet->T[i][j]);
        }
        printf("\n");
    }
    printf("]\n");
}

void FEM_mesh::print_vertex(vertex *vtx){
    printf("vertex, id: %i", vtx->id);
    printf("\tcells:\n\t");
    
    for (int i=0;i<(int)vtx->cells.size();i++)
        std::cout << vtx->cells[i] << " ";
    std::cout << "\n";
}

void FEM_mesh::print_tet(int i)
{
    
    tetrahedron *tet = tets[i];
    printf("v1 = [%.3e %.3e %.3e]; v1p = %i ;\n",tet->v1[0],tet->v1[1],tet->v1[2],tet->v1p);
    printf("v2 = [%.3e %.3e %.3e]; v2p = %i ;\n",tet->v2[0],tet->v2[1],tet->v2[2],tet->v2p);
    printf("v3 = [%.3e %.3e %.3e]; v3p = %i ;\n",tet->v3[0],tet->v3[1],tet->v3[2],tet->v3p);
    printf("v4 = [%.3e %.3e %.3e]; v4p = %i ;\n",tet->v4[0],tet->v4[1],tet->v4[2],tet->v4p);
    printf("center = [%.3e %.3e %.3e];\n",tet->c[0],tet->c[1],tet->c[2]);
    printf("volume = [%.3e];\n",tet->vol);
    printf("barycenter = [%.3e %.3e %.3e]; \n",tet->c[0],tet->c[1],tet->c[2]);
    
    printf("n12 = [%.3e %.3e %.3e]; \n",tet->n12[0],tet->n12[1],tet->n12[2]);
    printf("n13 = [%.3e %.3e %.3e]; \n",tet->n13[0],tet->n13[1],tet->n13[2]);
    printf("n14 = [%.3e %.3e %.3e]; \n",tet->n14[0],tet->n14[1],tet->n14[2]);
    printf("n23 = [%.3e %.3e %.3e]; \n",tet->n23[0],tet->n23[1],tet->n23[2]);
    printf("n24 = [%.3e %.3e %.3e]; \n",tet->n24[0],tet->n24[1],tet->n24[2]);
    printf("n34 = [%.3e %.3e %.3e]; \n",tet->n34[0],tet->n34[1],tet->n34[2]);
    //tet_initialize_T(tet);
    //print_T(tet);
}

void FEM_mesh::print_tri(triangle *tet)
{
    printf("v1 = [%.3e %.3e %.3e]; v1p = %i ;\n",tet->v1[0],tet->v1[1],tet->v1[2],tet->v1p);
    printf("v2 = [%.3e %.3e %.3e]; v2p = %i ;\n",tet->v2[0],tet->v2[1],tet->v2[2],tet->v2p);
    printf("v3 = [%.3e %.3e %.3e]; v3p = %i ;\n",tet->v3[0],tet->v3[1],tet->v3[2],tet->v3p);
    printf("center = [%.3e %.3e %.3e];\n",tet->c[0],tet->c[1],tet->c[2]);
    printf("n12 = [%.3e %.3e %.3e]; \n",tet->n12[0],tet->n12[1],tet->n12[2]);
    printf("n13 = [%.3e %.3e %.3e]; \n",tet->n13[0],tet->n13[1],tet->n13[2]);
    printf("n23 = [%.3e %.3e %.3e]; \n",tet->n23[0],tet->n23[1],tet->n23[2]);
    
}

/* Scalar product */
double FEM_mesh::dot(double *x,double *y)
 {
 return (x[0]*y[0]+x[1]*y[1]+x[2]*y[2]);
}

/* Euclidian distance */
double FEM_mesh::norm(double *x)
{
    return sqrt(dot(x,x));
}

void FEM_mesh::normalize(double *y)
{
    double a = norm(y);
    y[0]/=a;
    y[1]/=a;
    y[2]/=a;
}

/* Barycentric coordinate to point in tetrahedron/triangle. */
void FEM_mesh::bary2tetpoint(double *a,tetrahedron *tet,double *p)
{
    p[0]=0.0;
    p[1]=0.0;
    p[2]=0.0;
    
    double *x;
    
    x = tet->v1;
    
    p[0]+=a[0]*x[0];
    p[1]+=a[0]*x[1];
    p[2]+=a[0]*x[2];
    
    x = tet->v2;
    
    p[0]+=a[1]*x[0];
    p[1]+=a[1]*x[1];
    p[2]+=a[1]*x[2];
    
    x = tet->v3;
    
    p[0]+=a[2]*x[0];
    p[1]+=a[2]*x[1];
    p[2]+=a[2]*x[2];
    
    x = tet->v4;
    
    p[0]+=a[3]*x[0];
    p[1]+=a[3]*x[1];
    p[2]+=a[3]*x[2];
    
    
}

void FEM_mesh::bary2tripoint(double *a,triangle *tri,double *p)
{
    p[0]=0.0;
    p[1]=0.0;
    p[2]=0.0;
    
    double *x;
    
    x = tri->v1;
    
    p[0]+=a[0]*x[0];
    p[1]+=a[0]*x[1];
    p[2]+=a[0]*x[2];
    
    x = tri->v2;
    
    p[0]+=a[1]*x[0];
    p[1]+=a[1]*x[1];
    p[2]+=a[1]*x[2];
    
    x = tri->v3;
    
    p[0]+=a[2]*x[0];
    p[1]+=a[2]*x[1];
    p[2]+=a[2]*x[2];
    
}



void FEM_mesh::tetpoint2bary(tetrahedron *tet,double *x, double *xb)
{
    
    double b[3];
    /*	printf("in tetpoint2bary 1\n");
     for(int i=0;i<3;i++){
     printf("b_%d=%g\n",i,tet->v4[i]);
     }*/
    b[0]=x[0]-tet->v4[0];
    //printf("in tetpoint2bary 2\n");
    b[1]=x[1]-tet->v4[1];
    b[2]=x[2]-tet->v4[2];
    
    xb[0]=tet->T[0][0]*b[0]+tet->T[0][1]*b[1]+tet->T[0][2]*b[2];
    xb[1]=tet->T[1][0]*b[0]+tet->T[1][1]*b[1]+tet->T[1][2]*b[2];
    xb[2]=tet->T[2][0]*b[0]+tet->T[2][1]*b[1]+tet->T[2][2]*b[2];
    xb[3]=1.0-xb[0]-xb[1]-xb[2];
    
}


/* Area of triangle */
double FEM_mesh::triangle_area(triangle *tri)
{
   	return 0.0;
}


void FEM_mesh::tripoint2bary(triangle *tri,double *p,double *pb)
{
    double *v1 = tri->v1;
    double *v2 = tri->v2;
    double *v3 = tri->v3;
    
    double a = (v2[1]-v3[1])*(p[0]-v3[0])+(v3[0]-v2[0])*(p[1]-v3[1]);
    double b = (v2[1]-v3[1])*(v1[0]-v3[0])+(v3[0]-v2[0])*(v1[1]-v3[1]);
    pb[0]=a/b;
    a = (v3[1]-v1[1])*(p[0]-v3[0])+(v1[0]-v3[0])*(p[1]-v3[1]);
    b = (v3[1]-v1[1])*(v2[0]-v3[0])+(v1[0]-v3[0])*(v2[1]-v3[1]);
    pb[1]=a/b;
    pb[2]=1.0-pb[0]-pb[1];
}

int FEM_mesh::tri_inside(triangle *tri,double *x,double *minbary)
{
    double xb[3];
    int i;
    tripoint2bary(tri,x,xb);
    *minbary = 0.0;
    for (i=0; i<3; i++) {
        if (xb[i]<0.0) {
            *minbary += xb[i];
        }
    }
    if (*minbary<0.0)
        return 0;
    
    return 1;
}


void FEM_mesh::vec_diff(double *v1,double *v2,double *out){
    out[0] = v1[0]-v2[0];
    out[1] = v1[1]-v2[1];
    out[2] = v1[2]-v2[2];
}

/* Volume of tetrahedron */
double FEM_mesh::tetrahedron_vol(tetrahedron *tet)
{
    double a[3],b[3],c[3],cvec[3];
    vec_diff(tet->v1,tet->v4,a);
    vec_diff(tet->v2,tet->v4,b);
    vec_diff(tet->v3,tet->v4,c);
    cross_mesh(cvec,b,c);
    
   	return 1.0/6.0*fabs(dot(a,cvec));
}

/* Determine if the point x is inside (or on) the tetrahedron tet. */
int FEM_mesh::tet_inside(tetrahedron *tet, double *x,double *minbary)
{
    double xb[4];
    int i;
    tetpoint2bary(tet,x,xb);
    
    *minbary = 0.0;
    for (i=0; i<4; i++) {
        if (xb[i]<0.0) {
            *minbary += xb[i];
        }
    }
    
    if (*minbary<0.0)
        return 0;
    
    return 1;
    
}

bool FEM_mesh::inside_domain(double *pos){
	/* NOTE: This is a slow function, and is only intended to be used for a few special cases in the preprocessing step. */
    
    double minbary = 0.0;
    tetrahedron *tetloc;
    for(int i=0;i<ntet;++i){
        tetloc = tets[i];
        if(tet_inside(tetloc,pos,&minbary)==1){
            return true;
        }
    }
    
    
	return false;
}

int FEM_mesh::tri_which_macro_element(triangle *tri, double *x)
{
    
    double *c = tri->c;
    double *n;
    
    double v[3];
    double a;
    /* A vector from the centroid to the point x.*/
    v[0]=x[0]-c[0];
    v[1]=x[1]-c[1];
    v[2]=x[2]-c[2];
    normalize(v);
    
    /* Normal for edge 1->2 */
    n = tri->n12;
    a = dot(n,v);
    
    if (a < 0.0) {
        n =  tri->n13;
        a = dot(n,v);
        if (a < 0.0) {
            return tri->v1p;
        }
        else {
            return tri->v3p;
        }
    }
    else {
        n = tri->n23;
        a = dot(n,v);
        if (a < 0.0) {
            return tri->v2p;
        }
        else {
            return tri->v3p;
        }
        
    }
    
    /* error */
    return -1;
    
    
}

void FEM_mesh::set_n(double *n,int i,int j,int tetindex){
    tetrahedron *tet = tets[tetindex];
    if(i==1 && j==2){
        n[0] = tet->n12[0];
        n[1] = tet->n12[1];
        n[2] = tet->n12[2];
    }
    else if(i==1 && j==3){
        n[0] = tet->n13[0];
        n[1] = tet->n13[1];
        n[2] = tet->n13[2];
    }
    else if(i==1 && j==4){
        n[0] = tet->n14[0];
        n[1] = tet->n14[1];
        n[2] = tet->n14[2];
    }
    else if(i==2 && j==3){
        n[0] = tet->n23[0];
        n[1] = tet->n23[1];
        n[2] = tet->n23[2];
    }
    else if(i==2 && j==4){
        n[0] = tet->n24[0];
        n[1] = tet->n24[1];
        n[2] = tet->n24[2];
    }
    else if(i==3 && j==4){
        n[0] = tet->n34[0];
        n[1] = tet->n34[1];
        n[2] = tet->n34[2];
    }
}

void FEM_mesh::reverse_vec(double *v){
    v[0] = -v[0];
    v[1] = -v[1];
    v[2] = -v[2];
}

/* Knowing that a point x is inside the tetrahedron tet,
 find which nodes macroelement it belongs to */
int FEM_mesh::tet_which_macro_element(int tetindex,double *x)
{
    tetrahedron *tet = tets[tetindex];
    
    /* 1.) Check which side of the plane separating
     node 1 and 2 the point is. */
    //printf("OK\n");
    /* The centroid is a point in all the planes. */
    double *c = tet->c;
    double n[3];
    
    double v[3];
    double a;
    
    /* A vector from the centroid to the point.*/
    v[0]=x[0]-c[0];
    v[1]=x[1]-c[1];
    v[2]=x[2]-c[2];
    
    normalize(v);
    
    /* If the scalar product of the vector and the normal to the
     plane is negative, the point is on the side belonging to
     vertex 1. */
    
    /* the normal for plane 1->2 */
    set_n(n,1,2,tetindex);
//    n = tet->n12;
//    if(tetindex==30697){
//        reverse_vec(n);
//    }
    a = dot(n,v);
    
    if (a < 0.0){
        
        
//        n = tet->n13;
        set_n(n,1,3,tetindex);
//        if(tetindex==30697){
//            reverse_vec(n);
//
//        }
        
        /* Now we check which side of node 3 it is on */
        a = dot(n,v);
        if (a < 0.0){
            /* check point 4 */
//            n = tet->n14;
            set_n(n,1,4,tetindex);
//            if(tetindex==30697){
//                reverse_vec(n);
//            }
            
            a = dot(n,v);
            if (a < 0.0){
//                if(x[0]>0.425 && x[1]>0.005 && x[2]<0.25){
//                    if(tetindex==30697){
    //                    std::cout << "tet_index = " << tetindex << std::endl;
//                        printf("v=(%g,%g,%g)\n",v[0],v[1],v[2]);
//                        std::cout << x[0] << " " << x[1] << " " << x[2] << std::endl;
//                        print_tet(tetindex);
//                    }
                
//                }
                return tet->v1p;
            }
            else {
                return tet->v4p;
            }
            
        }
        else {
            /* On node 3's side. Check if in 3 or 4 */
//            n = tet->n34;
            set_n(n,3,4,tetindex);
            a = dot(n,v);
            if (a < 0.0) {
                return tet->v3p;
            }
            else{
                return tet->v4p;
            }
        }
    }
    else {
        /* If on nodes 2:s side, check if it is on
         node 3's side. */
//        n = tet->n23;
        set_n(n,2,3,tetindex);
        a = dot(n,v);
        if (a < 0.0) {
            /*Check if it on 4's side*/
//            n = tet->n24;
            set_n(n,2,4,tetindex);
            a = dot(n,v);
            if (a < 0.0) {
                return tet->v2p;
            }
            else {
                return tet->v4p;
            }
            
        }
        else {
            /* Check if on node 4's side */
//            n = tet->n34;
            set_n(n,3,4,tetindex);
            a = dot(n,v);
            if (a < 0.0){
                return tet->v3p;
            }
            else {
                return tet->v4p;
            }
            
        }
        
        
    }
    
    /* error */
    return -1;
    
}

/* Initialize internal tetrahedron format */
void FEM_mesh::tet_index_to_internal()
{
    
//    printf("Entering tet_to_internal\n");
    int i;//,ntet;
//    double *p=mesh->p;
//    int *t=mesh->t;
    
    double *x;
//    ntet = mesh->ntet;
//    std::cout << "Allocating tets." << std::endl;
    tetrahedron **tets_in =(tetrahedron **)malloc(ntet*sizeof(tetrahedron*));
    for (i=0; i<ntet; i++) {
        tets_in[i]=(tetrahedron *)malloc(sizeof(tetrahedron));
    }
//    printf("allocated\n");
    
    tetrahedron *temp;
    
    for (i=0; i<ntet; i++) {
        
        temp=tets_in[i];
        
        temp->v1p=t[4*i];
        x=&p[3*temp->v1p];
        temp->v1[0]=x[0];
        temp->v1[1]=x[1];
        temp->v1[2]=x[2];
        
        temp->v2p=t[4*i+1];
        x=&p[3*temp->v2p];
        temp->v2[0]=x[0];
        temp->v2[1]=x[1];
        temp->v2[2]=x[2];
        
        temp->v3p=t[4*i+2];
        x=&p[3*temp->v3p];
        temp->v3[0]=x[0];
        temp->v3[1]=x[1];
        temp->v3[2]=x[2];
        
        temp->v4p=t[4*i+3];
        x=&p[3*temp->v4p];
        temp->v4[0]=x[0];
        temp->v4[1]=x[1];
        temp->v4[2]=x[2];
        
    }
    
    tets = tets_in;
    
}

/* Initialize internal triangle format */
void FEM_mesh::tri_index_to_internal()
{
    
    int i;//,ntri;
//    double *p=mesh->p;
//    int *e=mesh->e;
    
    double *x;
//    ntri = mesh->ntri;
    
    triangle **tri_in =(triangle **)malloc(ntri*sizeof(triangle *));
    for (i=0; i<ntri; i++) {
        tri_in[i]=(triangle *)malloc(sizeof(triangle));
    }
    triangle *temp;
    
    for (i=0; i<ntri; i++) {
        temp=tri_in[i];
        
        temp->v1p=e[3*i];
        x=&p[3*temp->v1p];
        temp->v1[0]=x[0];
        temp->v1[1]=x[1];
        temp->v1[2]=x[2];
        
        
        temp->v2p = e[3*i+1];
        x=&p[3*temp->v2p];
        temp->v2[0]=x[0];
        temp->v2[1]=x[1];
        temp->v2[2]=x[2];
        
        temp->v3p=e[3*i+2];
        x=&p[3*temp->v3p];
        temp->v3[0]=x[0];
        temp->v3[1]=x[1];
        temp->v3[2]=x[2];
        
    }
    
    bnd = tri_in;
    
}

/* Uniform random point in triangle. */
void FEM_mesh::triangle_randunif(triangle *tri,double *ru)
{
    
    ru[0]=0.0;
    ru[1]=0.0;
    ru[2]=0.0;
    
    double bc[3];
    bc[0] = drand48();
    bc[1] = drand48();
    
    if (bc[0]+bc[1]>1.0) {
        bc[0] =1.0-bc[0];
        bc[1] =1.0-bc[1];
    }
    bc[2] = 1.0-bc[0]-bc[1];
    bary2tripoint(bc,tri,ru);
    
}


/* Sample point uniformly in a tetrahedron */
void FEM_mesh::tetrahedron_randunif(int i,double *ru)
{
    
    tetrahedron *tet = tets[i];
    
    double *x;
    
    ru[0]=0.0;
    ru[1]=0.0;
    ru[2]=0.0;
    
    double s,t,u,a;
    s = drand48();
    t = drand48();
    u = drand48();
    
    if(s+t>1.0) { // cut'n fold the cube into a prism
        
        s = 1.0 - s;
        t = 1.0 - t;
        
    }
    if(t+u>1.0) { // cut'n fold the prism into a tetrahedron
        
        double tmp = u;
        u = 1.0 - s - t;
        t = 1.0 - tmp;
        
    } else if(s+t+u>1.0) {
        
        double tmp = u;
        u = s + t + u - 1.0;
        s = 1.0 - t - tmp;
        
    }
    a=1.0-s-t-u; // a,s,t,u are the barycentric coordinates of the random point.
    
    x = tet->v1;
    ru[0]+=(a*x[0]);
    ru[1]+=(a*x[1]);
    ru[2]+=(a*x[2]);
    
    
    x = tet->v2;
    ru[0]+=(s*x[0]);
    ru[1]+=(s*x[1]);
    ru[2]+=(s*x[2]);
    
    
    x = tet->v3;
    ru[0]+=(t*x[0]);
    ru[1]+=(t*x[1]);
    ru[2]+=(t*x[2]);
    
    
    x = tet->v4;
    ru[0]+=(u*x[0]);
    ru[1]+=(u*x[1]);
    ru[2]+=(u*x[2]);
    
}





/* Sample point uniformly in a tetrahedron */
void FEM_mesh::tetrahedron_randunif(tetrahedron *tet,double *ru)
{
    double *x;
    
    ru[0]=0.0;
    ru[1]=0.0;
    ru[2]=0.0;
    
    double s,t,u,a;
    s = drand48();
    t = drand48();
    u = drand48();
    
    if(s+t>1.0) { // cut'n fold the cube into a prism
        
        s = 1.0 - s;
        t = 1.0 - t;
        
    }
    if(t+u>1.0) { // cut'n fold the prism into a tetrahedron
        
        double tmp = u;
        u = 1.0 - s - t;
        t = 1.0 - tmp;
        
    } else if(s+t+u>1.0) {
        
        double tmp = u;
        u = s + t + u - 1.0;
        s = 1.0 - t - tmp;
        
    }
    a=1.0-s-t-u; // a,s,t,u are the barycentric coordinates of the random point.
    
    x = tet->v1;
    ru[0]+=(a*x[0]);
    ru[1]+=(a*x[1]);
    ru[2]+=(a*x[2]);
    
    
    x = tet->v2;
    ru[0]+=(s*x[0]);
    ru[1]+=(s*x[1]);
    ru[2]+=(s*x[2]);
    
    
    x = tet->v3;
    ru[0]+=(t*x[0]);
    ru[1]+=(t*x[1]);
    ru[2]+=(t*x[2]);
    
    
    x = tet->v4;
    ru[0]+=(u*x[0]);
    ru[1]+=(u*x[1]);
    ru[2]+=(u*x[2]);
    
}

/* Cross product between vectors x,y. Result stored in v. */
void FEM_mesh::cross_mesh(double *v,double *x, double *y)
{
    
    v[0]=x[1]*y[2]-y[1]*x[2];
    v[1]=-x[0]*y[2]+y[0]*x[2];
    v[2]=x[0]*y[1]-y[0]*x[1];
    
}

/* Returns the surface area of a quadilateral specified by points v1,v2,v3,v4.
 The surface normal is computed and stored in n */
double FEM_mesh::quadarea(double *v1,double *v2,double *v3, double *v4,double *n)
{
    
    double d1[3],d2[3];
    double a;
    
    d1[0]=v3[0]-v1[0];
    d1[1]=v3[1]-v1[1];
    d1[2]=v3[2]-v1[2];
   	
    d2[0]=v4[0]-v2[0];
    d2[1]=v4[1]-v2[1];
    d2[2]=v4[2]-v2[2];
    
    cross_mesh(n,d1,d2);
    
    /* Normalize n */
    a = sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
    n[0]/=a;
    n[1]/=a;
    n[2]/=a;
    
    return 0.5*a;
    
    
}

/* From an initialized tetrahedra, calculate and initialize the part related to the dual mesh. */
void FEM_mesh::primal2dualtri(triangle *tri)
{
    
    /* Barycentric coordinates of all points that we need to compute the dual contributions. */
    
    /* Centroid of tetrahedron*/
    double cb[] = {tr,tr,tr};
    
    /* midpoints of edges the primal tetrahedron. */
    double mid12b[] = {0.5,0.5,0.0};
    double mid13b[] = {0.5,0.0,0.5};
    double mid23b[] = {0.0,0.5,0.5};
    
    double v1[3],v2[3],t[3],e[3];
    
    /* Centroid of primal triangle*/
    double c[3];
    bary2tripoint(cb,tri,c);
    tri->c[0]=c[0];
    tri->c[1]=c[1];
    tri->c[2]=c[2];
    
    /* A vector in the tangent plane (normal to the primal triangle). */
    v1[0]=tri->v3[0]-tri->v1[0];
    v1[1]=tri->v3[1]-tri->v1[1];
    v1[2]=tri->v3[2]-tri->v1[2];
    
    v2[0]=tri->v2[0]-tri->v1[0];
    v2[1]=tri->v2[1]-tri->v1[1];
    v2[2]=tri->v2[2]-tri->v1[2];
    
    cross_mesh(t,v2,v1);
    
    /* Edge 12 */
    bary2tripoint(mid12b,tri,v1);
    e[0]=v1[0]-c[0];
    e[1]=v1[1]-c[1];
    e[2]=v1[2]-c[2];
    cross_mesh(tri->n12,e,t);
    normalize(tri->n12);
    /* Check if normal is pointing in the right direction. */
    v1[0]=tri->v2[0]-tri->v1[0];
    v1[1]=tri->v2[1]-tri->v1[1];
    v1[2]=tri->v2[2]-tri->v1[2];
    if (dot(v1,tri->n12)<0.0) {
        tri->n12[0]*=-1.0;
        tri->n12[1]*=-1.0;
        tri->n12[2]*=-1.0;
    }
    
    /* Edge 13 */
    bary2tripoint(mid13b,tri,v1);
    e[0]=v1[0]-c[0];
    e[1]=v1[1]-c[1];
    e[2]=v1[2]-c[2];
    cross_mesh(tri->n13,e,t);
    normalize(tri->n13);
    
    /* Check if normal is pointing in the right direction. */
    v1[0]=tri->v3[0]-tri->v1[0];
    v1[1]=tri->v3[1]-tri->v1[1];
    v1[2]=tri->v3[2]-tri->v1[2];
    if (dot(v1,tri->n13)<0.0) {
        tri->n13[0]*=-1.0;
        tri->n13[1]*=-1.0;
        tri->n13[2]*=-1.0;
    }
    
    /* Edge 23 */
    bary2tripoint(mid23b,tri,v1);
    e[0]=v1[0]-c[0];
    e[1]=v1[1]-c[1];
    e[2]=v1[2]-c[2];
    cross_mesh(tri->n23,e,t);
    normalize(tri->n23);
    
    /* Check if normal is pointing in the right direction. */
    v1[0]=tri->v3[0]-tri->v2[0];
    v1[1]=tri->v3[1]-tri->v2[1];
    v1[2]=tri->v3[2]-tri->v2[2];
    if (dot(v1,tri->n23)<0.0) {
        tri->n23[0]*=-1.0;
        tri->n23[1]*=-1.0;
        tri->n23[2]*=-1.0;
    }
    
}

//void FEM_mesh::vec_diff(double *v1,double *v2,double *res){
//    res[0] = v1[0]-v2[0];
//    res[1] = v1[1]-v2[1];
//    res[2] = v1[2]-v2[2];
//}

void FEM_mesh::primal2dualtet(tetrahedron *tet)
{
    
    /* Barycentric coordinates of all points that we need to compute the dual contributions. */	
    
    /* Centroid of tetrahedron*/
    double cb[] = {0.25,0.25,0.25,0.25};	
    
    /* Centroid of triangle faces in the primal tetrahedron. */
    double c134b[] = {tr, 0.0, tr, tr};	
    double c124b[] = {tr, tr, 0, tr};
    double c123b[] = {tr, tr, tr, 0};
    double c234b[] = {0.0, tr, tr, tr};
    
    /* Midpoint on edges of the primal tetrahedron. */	
    double mid12b[] = {0.5, 0.5, 0.0, 0.0};  	
    double mid13b[] = {0.5, 0.0, 0.5, 0.0};
    double mid23b[] = {0.0, 0.5, 0.5, 0.0};
    double mid14b[] = {0.5, 0.0, 0.0, 0.5};
    double mid24b[] = {0.0, 0.5, 0.0, 0.5};
    double mid34b[] = {0.0, 0.0, 0.5, 0.5};
    
    double v1[3],v2[3],v3[3];	
    /* Centroid */ 	
    double c[3];
    bary2tetpoint(cb,tet,c);
    
    tet->c[0]=c[0];
    tet->c[1]=c[1];
    tet->c[2]=c[2];
    
    double a;	
    
    
    double test_vec[3];
    
    /* Facet 12 */
    bary2tetpoint(mid12b,tet,v1);
    bary2tetpoint(c123b,tet,v2);
    bary2tetpoint(c124b,tet,v3);
    
    a = quadarea(v1,v2,c,v3,tet->n12);
    vec_diff(tet->v1,tet->v2,test_vec);
    if(dot(test_vec,tet->n12)>0.0)
        reverse_vec(tet->n12);
    
   	
    tet->a12 = a;
    
    /* Facet 13 */
    bary2tetpoint(mid13b,tet,v1);
    bary2tetpoint(c134b,tet,v2);
    bary2tetpoint(c123b,tet,v3);
    
    
    a = quadarea(v1,v2,c,v3,tet->n13);
    vec_diff(tet->v1,tet->v3,test_vec);
    if(dot(test_vec,tet->n13)>0.0)
        reverse_vec(tet->n13);
    
    tet->a13 = a;
    
    /* Facet 23 */
    bary2tetpoint(mid23b,tet,v1);
    bary2tetpoint(c123b,tet,v2);
    bary2tetpoint(c234b,tet,v3);
    
    a = quadarea(v1,v2,c,v3,tet->n23);
    vec_diff(tet->v2,tet->v3,test_vec);
    if(dot(test_vec,tet->n23)>0.0)
        reverse_vec(tet->n23);
    
    tet->a23 = a;
    
    /* Facet 14 */
    bary2tetpoint(mid14b,tet,v1);
    bary2tetpoint(c124b,tet,v2);
    bary2tetpoint(c134b,tet,v3);
    
    a = quadarea(v1,v2,c,v3,tet->n14);
    vec_diff(tet->v1,tet->v4,test_vec);
    if(dot(test_vec,tet->n14)>0.0)
        reverse_vec(tet->n14);
    
    tet->a14 = a;
    
    /* Facet 24 */
    bary2tetpoint(mid24b,tet,v1);
    bary2tetpoint(c234b,tet,v2);
    bary2tetpoint(c124b,tet,v3);
    
    a = quadarea(v1,v2,c,v3,tet->n24);
    vec_diff(tet->v2,tet->v4,test_vec);
    if(dot(test_vec,tet->n24)>0.0)
        reverse_vec(tet->n24);
    tet->a24 = a;
    
    /* Facet 34 */
    bary2tetpoint(mid34b,tet,v1);
    bary2tetpoint(c134b,tet,v2);
    bary2tetpoint(c234b,tet,v3);
    
    a = quadarea(v1,v2,c,v3,tet->n34);
    vec_diff(tet->v3,tet->v4,test_vec);
    if(dot(test_vec,tet->n34)>0.0)
        reverse_vec(tet->n34);
    
    tet->a34 = a;
    
    tet_initialize_T(tet);
    
}

void FEM_mesh::set_connectivity_matrix(urdme_model *model){
    jcK = model->jcK;
    irK = model->irK;
}

void FEM_mesh::set_meso_mesh(urdme_model *model)
{
//    int Ncells = mesh->Ncells;
    int Mspecies = model->Mspecies;
//    double *p = mesh->p;
    size_t *jcD = model->jcD;
    size_t *irD = model->irD;
    double *prD = model->prD;
    
    
         
    voxel temp;
    neighbor tn;
    int to_dof;
    for(int i=0;i<Ncells;i++){
        temp.id = i;
        temp.node[0] = p[3*i];
        temp.node[1] = p[3*i+1];
        temp.node[2] = p[3*i+2];
        
        
        int dof = i*Mspecies;
        
        temp.num_conn=0;
        temp.vol = model->vol[i];
        temp.sd = model->sd[i];
        temp.totalD = 0.0;
        temp.neighbors.resize(0);
        temp.isbnd = boundaries[i].isbnd;
    //    std::cout << ">>>>>>>>>>>>>>>>>>>>>>" << std::endl;
    //    std::cout << "Voxel main: " << i << std::endl;
    //    std::cout << "Neighbors: ";
        for (int k=jcD[dof]; k<jcD[dof+1]; k++) {
            
            
            to_dof=irD[k];
            if(to_dof!=dof){
                temp.num_conn++;
                tn.vox = to_dof/Mspecies;
          //      std::cout << tn.vox << ", ";
                tn.D = prD[k];///specs[0].D;
                temp.totalD += tn.D;
                temp.neighbors.push_back(tn);
            }
        }
     //   std::cout << std::endl << ">>>>>>>>>>>>>>>>>>>>>>" << std::endl;
        //std::cout << std::endl;
        voxels.push_back(temp);
    }
    
    
}

void FEM_mesh::voxel_boundaries(urdme_model *model)
{
	
	//voxel boundary id 457
    
//    int Ncells   = Ncells;
    //	int Mspecies = model->Mspecies;
    
    /* Boundary triangles */
//    double *p = mesh->p;
    //	int *sd = model->sd;
    
//    vector<plane> boundaries;
    
//    int ntri=mesh->ntri;
    int i;//,j;
    
    double a;
    
    /* Initialize all normals and vectors */
    plane temp;
    
    temp.n[0]=0.0;
    temp.n[1]=0.0;
    temp.n[2]=0.0;
    temp.v1[0]=0.0;
    temp.v1[1]=0.0;
    temp.v1[2]=0.0;
    temp.v2[0]=0.0;
    temp.v2[1]=0.0;
    temp.v2[2]=0.0;
    temp.p[0]=0.0;
    temp.p[1]=0.0;
    temp.p[2]=0.0;
    temp.isbnd = 0;
    
    for (i=0; i<Ncells; i++) {
        /* Point in plane, the vertex itself. */
        temp.id = i;
        boundaries.push_back(temp);
    }
    
    /* Compute the point in the plane as the mean value of the vertex iteself and the
     centroid of the surrounding triangles. */
    for (i=0; i<Ncells; i++) {
        boundaries[i].p[0]=p[3*i];
        boundaries[i].p[1]=p[3*i+1];
        boundaries[i].p[2]=p[3*i+2];
    }
    
    
    /* Loop over the boundary triangles and assemble their contribution to the normals */
    triangle *tri;
    double n[3];
    double vec[3];
    
    int sv;
    //   double radius=0;
    
    /* TODO: Should we use jcK, irK here instead? */
    //size_t *jcD = model->jcD;
    //size_t *irD = model->irD;
    jcK = model->jcK;
    irK = model->irK;
	jcD = model->jcD;
	irD = model->irD;
    
    
    int dof,to_dof;
    size_t k;
    double y[3];
    double alpha;
    
    /* We will need to be able determine whether a dof is on the boundary/surface or not. */
    int *onboundary;
    onboundary = (int *)calloc(Ncells,sizeof(int));
    
    for (i=0; i<ntri; i++) {
        tri = bnd[i];
        onboundary[tri->v1p]=1;
        onboundary[tri->v2p]=1;
        onboundary[tri->v3p]=1;
    }
    
    double amax;
    
    for (i=0; i<ntri; i++) {
        
        tri = bnd[i];
        
        /* Compute the unit normal of that triangle.*/
        trinormal(tri,n);
        
        /* TODO: Need to check that all normals that we assemble point in a direction that
         the species may move. */
        //		radius = norm(tri->v1);
        a = dot(vec,n);
        
        /* Add that normal to accumulated mean normals of the vertices of the triangle.*/
        sv = tri->v1p;
        
        
        alpha = 1.0;
        dof = sv;
        
        /* Find the edge with the largest dot-product with the normal. */
        a=0.0;
        amax =0.0;
        dof = sv;
        /* If the point has neighbors in the interior */
        for (k=jcD[dof]; k<jcD[dof+1]; k++) {
            
            to_dof=irD[k];
            if(dof!=to_dof){
                y[0]=p[3*to_dof]  -p[3*dof];
                y[1]=p[3*to_dof+1]-p[3*dof+1];
                y[2]=p[3*to_dof+2]-p[3*dof+2];
//                if(dof==457){
//					std::cout << "y=(" << y[0] << "," << y[1] << "," << y[2] << ")" << std::endl;
//				}
            
                normalize(y);
                a = dot(n,y);
                if(fabs(a)>fabs(amax)){
                    amax = a;
                }
            }
        }
        
        
        
        /* If the edge in D with the largest component in the normals direction
         points in the opposite direction, we flip the normal. */
        if (amax < 0.0) {
            alpha = -1.0;
        }
        else if(amax==0){
			/* The point had no neighbors in the interior. We need to find the correct
			 * direction in a different way. */
//			 std::cout << "No interior neighbors..." << std::endl;
			 //std::cout << "Mspecies: " << model->Mspecies;
			 /* Move the point a tiny amount. */
			 double pint[]={p[3*dof],p[3*dof+1],p[3*dof+2]};
			 /* eps defines a "tiny amount". */
			 double eps = sqrt(pint[0]*pint[0]+pint[1]*pint[1]+pint[2]*pint[2])/100.0;
			 for(int l=0;l<3;++l){
				pint[l] += eps;
			 }
			 int pvox = micro2meso2(pint,dof);
            bool inside_dom = inside_domain(pint);
            
            if(!inside_dom){
                reverse_vec(pint);
                
            }
            y[0]=pint[0]-p[3*dof];
            y[1]=pint[1]-p[3*dof+1];
            y[2]=pint[2]-p[3*dof+2];
            amax = dot(n,y);
            
            if (amax < 0.0) {
                alpha = -1.0;
            }

            
//            std::cout << "Point is in voxel: " << pvox << std::endl;
//            std::cout << "Point is in domain after shift: " << inside_dom << std::endl;
		}
        
        
//        if(sv==457){
//            std::cout << ">>>>>>>>>>>>>>>>>>" << std::endl;
//			std::cout << "1" << std::endl;
//            std::cout << "sv=" << sv << std::endl;
//            std::cout << "y=(" << y[0] << "," << y[1] << "," << y[2] << ")" << std::endl;
//            std::cout << "n=(" << n[0] << "," << n[1] << "," << n[2] << ")" << std::endl;
//            std::cout << "alpha=" << alpha << std::endl;
//            std::cout << "dof=" << dof << " todof=" << to_dof << std::endl;
//            std::cout << ">>>>>>>>>>>>>>>>>>" << std::endl;
//        }
        boundaries[sv].n[0]+=alpha*n[0];
        boundaries[sv].n[1]+=alpha*n[1];
        boundaries[sv].n[2]+=alpha*n[2];
        boundaries[sv].isbnd=1;
        
        
        sv = tri->v2p;
        
        alpha = 1.0;
        
        /* Find the edge with the largest dot-product with the normal. */
        a=0.0;
        amax =0.0;
        dof = sv;
        
        for (k=jcD[dof]; k<jcD[dof+1]; k++) {
            
            to_dof=irD[k];
            if(dof!=to_dof){
                y[0]=p[3*to_dof]  -p[3*dof];
                y[1]=p[3*to_dof+1]-p[3*dof+1];
                y[2]=p[3*to_dof+2]-p[3*dof+2];
//                if(dof==457){
//					std::cout << "y=(" << y[0] << "," << y[1] << "," << y[2] << ")" << std::endl;
//				}
                normalize(y);
                a = dot(n,y);
                if(fabs(a)>fabs(amax))
                    amax = a;
            }
            
        }
        
        /* If the edge in D with the larges component in the normals direction
         points in the opposite direction, we flip the normal. */
        if (amax < 0.0) {
            alpha = -1.0;
        }
        else if(amax==0){
            /* The point had no neighbors in the interior. We need to find the correct
             * direction in a different way. */
//            std::cout << "No interior neighbors..." << std::endl;
            //std::cout << "Mspecies: " << model->Mspecies;
            /* Move the point a tiny amount. */
            double pint[]={p[3*dof],p[3*dof+1],p[3*dof+2]};
            /* eps defines a "tiny amount". */
            double eps = sqrt(pint[0]*pint[0]+pint[1]*pint[1]+pint[2]*pint[2])/100.0;
            for(int l=0;l<3;++l){
                pint[l] += eps;
            }
            int pvox = micro2meso2(pint,dof);
            bool inside_dom = inside_domain(pint);
            
            if(!inside_dom){
                reverse_vec(pint);
                
            }
            y[0]=pint[0]-p[3*dof];
            y[1]=pint[1]-p[3*dof+1];
            y[2]=pint[2]-p[3*dof+2];
            amax = dot(n,y);
            
            if (amax < 0.0) {
                alpha = -1.0;
            }
            
            
//            std::cout << "Point is in voxel: " << pvox << std::endl;
//            std::cout << "Point is in domain after shift: " << inside_dom << std::endl;
        }
//        if(sv==457){
//            std::cout << ">>>>>>>>>>>>>>>>>>" << std::endl;
//            std::cout << "2" << std::endl;
//            std::cout << "sv=" << sv << std::endl;
//            std::cout << "y=(" << y[0] << "," << y[1] << "," << y[2] << ")" << std::endl;
//            std::cout << "n=(" << n[0] << "," << n[1] << "," << n[2] << ")" << std::endl;
//            std::cout << "alpha=" << alpha << std::endl;
//            std::cout << "amax=" << amax << std::endl;
//            std::cout << ">>>>>>>>>>>>>>>>>>" << std::endl;
//        }
        //			printf("n3=[%g %g %g];\n",alpha*n[0],alpha*n[1],alpha*n[2]);
        
        boundaries[dof].n[0]+=alpha*n[0];
        boundaries[dof].n[1]+=alpha*n[1];
        boundaries[dof].n[2]+=alpha*n[2];
        boundaries[dof].isbnd=1;
        
        
        sv = tri->v3p;
        
        /* Find the edge with the largest dot-product with the normal. */
        alpha = 1.0;
        dof = sv;
        a=0.0;
        amax =0.0;
        
        for (k=jcD[dof]; k<jcD[dof+1]; k++) {
            
            to_dof=irD[k];
            
            /* TODO: Should it be possible for to_dof to equal dof here? It seems to happen occasionally. */
            if(dof!=to_dof){
            
                y[0]=p[3*to_dof]  -p[3*dof];
                y[1]=p[3*to_dof+1]-p[3*dof+1];
                y[2]=p[3*to_dof+2]-p[3*dof+2];
//                if(dof==457){
//					std::cout << "y=(" << y[0] << "," << y[1] << "," << y[2] << ")" << std::endl;
//				}

                normalize(y);
                a = dot(n,y);
                if(fabs(a)>fabs(amax))
                    amax = a;
            }
            
        }
        
        /* If the edge in D with the largest component in the normals direction
         points in the opposite direction, we flip the normal. */
        if (amax < 0.0) {
            alpha = -1.0;
        }
        else if(amax==0){
            /* The point had no neighbors in the interior. We need to find the correct
             * direction in a different way. */
//            std::cout << "No interior neighbors..." << std::endl;
            //std::cout << "Mspecies: " << model->Mspecies;
            /* Move the point a tiny amount. */
            double pint[]={p[3*dof],p[3*dof+1],p[3*dof+2]};
            /* eps defines a "tiny amount". */
            double eps = sqrt(pint[0]*pint[0]+pint[1]*pint[1]+pint[2]*pint[2])/100.0;
            for(int l=0;l<3;++l){
                pint[l] += eps;
            }
            int pvox = micro2meso2(pint,dof);
            bool inside_dom = inside_domain(pint);
            
            if(!inside_dom){
                reverse_vec(pint);
                
            }
            y[0]=pint[0]-p[3*dof];
            y[1]=pint[1]-p[3*dof+1];
            y[2]=pint[2]-p[3*dof+2];
            amax = dot(n,y);
            
            if (amax < 0.0) {
                alpha = -1.0;
            }
            
            
//            std::cout << "Point is in voxel: " << pvox << std::endl;
//            std::cout << "Point is in domain after shift: " << inside_dom << std::endl;
        }
//        if(sv==457){
//			
//            std::cout << ">>>>>>>>>>>>>>>>>>" << std::endl;
//			std::cout << "3" << std::endl;
//            std::cout << "sv=" << sv << std::endl;
//            std::cout << "y=(" << y[0] << "," << y[1] << "," << y[2] << ")" << std::endl;
//            std::cout << "n=(" << n[0] << "," << n[1] << "," << n[2] << ")" << std::endl;
//            std::cout << "alpha=" << alpha << std::endl;
//            std::cout << "dof=" << dof << " todof=" << to_dof << std::endl;
//            std::cout << "p[dof]=" << p[3*dof] << "," << p[3*dof+1] << "," << p[3*dof+2] << std::endl;
//            std::cout << "p[to_dof]=" << p[3*to_dof] << "," << p[3*to_dof+1] << "," << p[3*to_dof+2] << std::endl;
//            std::cout << ">>>>>>>>>>>>>>>>>>" << std::endl;
////            exit(0);
//        }
        boundaries[sv].n[0]+=alpha*n[0];
        boundaries[sv].n[1]+=alpha*n[1];
        boundaries[sv].n[2]+=alpha*n[2];
        boundaries[sv].isbnd=1;
//        if(i==182){
//            std::cout << "FINAL NORMAL: " <<
//        }
        
        /* CHECK. */
        /*plane *tmp;
        tmp = &boundaries[sv];
        if(tmp->n[0]==0 && tmp->n[1]==0 && tmp->n[2]==0){
            std::cout << "ERROR. NORMAL" << std::endl;
            std::cout << "DOF IS " << sv << std::endl;
            std::cout << "isbnd=" << tmp->isbnd << std::endl;
            std::cout << "i=" << i << std::endl;
            std::cout << tmp->p[0] << " " << tmp->p[1] << " " << tmp->p[2] << std::endl;
            std::cout << tmp->n[0] << " " << tmp->n[1] << " " << tmp->n[2] << std::endl;
            std::cout << n[0] << " " << n[1] << " " << n[2] << std::endl;
            std::cout << "a=" << a << " norm=" << norm(tmp->n) << std::endl;
            std::cout << "alpha = " << alpha << std::endl;
            exit(0);
        }*/
        
    }
    
    
    
    
    /* Normalize the mean normals */
    plane *tmp;
    for (i=0; i<Ncells; i++) {
        
        tmp = &boundaries[i];
        
        a = norm(tmp->n);
        if (a>0.0){
            tmp->n[0]/=a;
            tmp->n[1]/=a;
            tmp->n[2]/=a;
        }
        
        
        
    }
    
//    for (int k=0; k<Ncells; k++){
//        tmp = &boundaries[k];
//        if(tmp->isbnd==1){
//            std::cout << tmp->p[0] << " " << tmp->p[1] << " " << tmp->p[2] << " ";
//            std::cout << tmp->n[0] << " " << tmp->n[1] << " " << tmp->n[2] << std::endl;
//        }
//    }
    
    /* Get vectors in the tangent plane */
//    for (i=0; i<Ncells;i++) {
//        if (boundaries[i].isbnd){
//            vec_in_plane(boundaries[i].n,boundaries[i].v1,boundaries[i].v2);
//        }
//        boundaries[i].type.push_back(0);
//    }
    
    free(onboundary);
    
//    return boundaries;
    
}
//
///* Normalized surface normal to the plane defined by the triangle tri. */
double* FEM_mesh::tri_surfnormal(int *tri,double *p)
{
    double a[3];
    double b[3];
    double *n;
    n = (double *)malloc(3*sizeof(double));
    
    double *x1,*x2,*x3;
    x1 = &p[tri[0]];
    x2 = &p[tri[1]];
    x3 = &p[tri[2]];
    
    a[0]=x2[0]-x1[0];
    a[1]=x2[1]-x1[1];
    a[2]=x2[2]-x1[2];
    
    b[0]=x3[0]-x1[0];
    b[1]=x3[1]-x1[1];
    b[2]=x3[2]-x1[2];
    
    n[0]=a[1]*b[2]-b[1]*a[2];
    n[1]=-(a[0]*b[2]-b[0]*a[2]);
    n[2]=a[0]*b[1]-b[0]*a[1];
    
    double d = sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
    n[0]=n[0]/d;
    n[1]=n[1]/d;
    n[2]=n[2]/d;
    
    return n;
    
}
//
//
//
int FEM_mesh::cmp_point(double *v1,double *v2)
{
    int i;
    for (i=0; i<3; i++) {
        if (v1[i]!=v2[i]) {
            return -1;
        }
    }
    return 1;
    
}
//
void FEM_mesh::project(double *p,triangle *tri) {
    double *v1 = tri->v1;
    double *v2 = tri->v2;
    double *v3 = tri->v3;
    double *vec1 = (double *)malloc(3*sizeof(double));
    double *vec2 = (double *)malloc(3*sizeof(double));
    vec1[0] = v2[0]-v1[0];
    vec1[1] = v2[1]-v1[1];
    vec1[2] = v2[2]-v1[2];
    vec2[0] = v3[0]-v2[0];
    vec2[1] = v3[1]-v2[1];
    vec2[2] = v3[2]-v2[2];
    double *normal = (double *)malloc(3*sizeof(double));
    cross_mesh(normal,vec1,vec2);
    normalize(normal);
    double dist = normal[0]*(p[0]-v3[0])+normal[1]*(p[1]-v3[1])+normal[2]*(p[2]-v3[2]);
    p[0] -= dist*normal[0];
    p[1] -= dist*normal[1];
    p[2] -= dist*normal[2];
    free(normal);
    free(vec1);
    free(vec2);
}
//

double FEM_mesh::distance(double *x,double *y) {
    return pow(x[0]-y[0],2)+pow(x[1]-y[1],2)+pow(x[2]-y[2],2);
}

int FEM_mesh::nn(double *pos, int pv,int spec,int Mspecies){
    
    int node,node2,nv;
    size_t j,k;
    double d,dn;
    double *x;
    
    //nv=pv*Mspecies+spec;
    //d = INFINITY;
//    std::cout << "Mspecies: " << Mspecies << " spec: " << spec << std::endl;
    int dof = pv*Mspecies + spec;
    nv = dof;
//    std::cout << "dof: " << dof << std::endl;
    /* Distance from the particle to the current guess for the voxel. */
    x = &p[3*pv];
    d = distance(x,pos);
//    printf("d: %.2e\n",d);
    //printf("x: (%.2e %.2e %.2e)\n",x[0],x[1],x[2]);
//    printf("pos: (%.2e %.2e %.2e)\n",pos[0],pos[1],pos[2]);
    /* To be sure to find the nearest voxels in 3D we need to look two layers out from the voxel. */
//    printf("1\n");
//    std::cout << jcK[dof] << std::endl;
//    printf("2\n");
//    std::cout << jcK[dof+1] << std::endl;
//    printf("3\n");
    
    for (j=jcK[dof]; j<jcK[dof+1]; j++) {
//        printf("looping...\n");
        node = irK[j];
        
        /* Check distance to all neighbours to node */
        /* TODO: New particle class does not have an integer type. */
        x  = &p[3*(node-spec)/Mspecies];
        dn = distance(x,pos);
        
        //printf("dn: %.2e\n",dn);
        
        if (dn < d) {
            nv = node;
            d = dn;
        }
        
        /* And to all the neighbours of the neighbour (we are comparing to the same point many times this way...). */
        for (k=jcK[node]; k<jcK[node+1]; k++) {
            
            node2 = irK[k];
            
            // Check distance to all neighbours to node
            x = &p[3*(node2-spec)/Mspecies];
            dn = distance(x,pos);
            //printf("dn: %.2e\n",dn);
            
            if (dn < d) {
                nv = node2;
                d = dn;
            }
        }
        
        
    }
    return (nv-spec)/Mspecies;
}

void FEM_mesh::print_dual_voxel_matlab(int voxel){
    vertex *vtx;
    vtx = vertices[voxel];
    int ntet = (int)(vtx->cells.size());
    printf("figure\n");
    printf("hold on\n");
    for(int i=0;i<ntet;i++){
        int tetind = vtx->cells[i];
        
        tetrahedron *tet = tets[tetind];
        printf("v1 = [%.3e %.3e %.3e];\n",tet->v1[0],tet->v1[1],tet->v1[2]);
        printf("v2 = [%.3e %.3e %.3e];\n",tet->v2[0],tet->v2[1],tet->v2[2]);
        printf("v3 = [%.3e %.3e %.3e];\n",tet->v3[0],tet->v3[1],tet->v3[2]);
        printf("v4 = [%.3e %.3e %.3e];\n",tet->v4[0],tet->v4[1],tet->v4[2]);
        printf("M%d = [v1 ;v2;v3;v4;v1;v3;v4;v2];\n",i);
        printf("plot3(M%d(:,1),M%d(:,2),M%d(:,3),'linewidth',2)\n",i,i,i);
    }
}

std::array<double,3> FEM_mesh::sample_mesh_uniform(gsl_rng* rng,int dimin){
    
    std::array<double,3> pout;
    
    if(dimin==3){

        
        double r = gsl_rng_uniform(rng);
        while(r==1){
            r = gsl_rng_uniform(rng);
        }
        r *= vol;
        
        double acc_vol = 0.0;
        double ru[3];
        for(int i=0;i<ntet;++i){
            acc_vol += tets[i]->vol;
            if(acc_vol>=r){
                /* Sample continuous position. */
                tetrahedron_randunif(i,ru);
                pout[0] = ru[0];
                pout[1] = ru[1];
                pout[2] = ru[2];
                return pout;
            }
        
        }
    }
    
    
    std::cout << "Could not sample position." << std::endl;
    
    return {0.0,0.0,0.0};
    
}


//
//
//
///* Exact sampling of points on dual elements. */
std::array<double,3> FEM_mesh::meso2micro(int voxin,int dimin){
    
    int voxel = voxin;
    
    
    
    int dim = dimin;
    
    vertex *vtx;
    triangle **tris = bnd;
    
    double ru[3];
    int wm;
    tetrahedron *temp;
    double minbary[1];
    
    /* If the particle is a 3D species */
    if (1==1){//dim == 3){
        /* Number of tetrahedra around that vertex. */
   

        vtx = vertices[voxel];
        int ntet = (int)(vtx->cells.size());

        
        /* TODO: Seed built in RNG. */
        

        double tets_tot_vol = 0.0;;
        for(int i=0;i<vtx->cells.size();++i){
			//std::cout << "cell vol: " << tets[vtx->cells[i]]->vol;
			tets_tot_vol += tets[vtx->cells[i]]->vol;
		}
		//std::cout << "Total vol: " << tets_tot_vol << std::endl;
        double rrvol = drand48()*tets_tot_vol;
        int tet = vtx->cells[0];
        double tet_cum_vol = tets[vtx->cells[0]]->vol;
        int j=0;
        while(tet_cum_vol<rrvol){
			++j;
			tet = vtx->cells[j];
			tet_cum_vol += tets[tet]->vol;
		}
        
        
        //int tet = vtx->cells[(int)(drand48()*ntet)];

        /* Randomly in that tetrahedron */
        tetrahedron_randunif(tet,ru);
        wm = tet_which_macro_element(tet,ru);

        if (wm==-1) {
            printf("Problem in whichmacroelement.\n");
        }
        
        /* Rejection sampling */
        while (wm!=voxel) {
            tetrahedron_randunif(tet,ru);
            if (!tet_inside(tets[tet],ru,minbary)) {
                printf("Sampled point not inside tetrahedron. This is bad.\n");
            }
            wm = tet_which_macro_element(tet,ru);

            if (wm==-1) {
                printf("Problem in whichmacroelement.\n");
            }
        }
        std::array<double,3> pout = {ru[0],ru[1],ru[2]};
        return pout;
        
    }
    /* If 2D species, place randomly on the surface mesh */
    else if (dim == 2){
        
        int ntri = jcv2e[voxel+1]-jcv2e[voxel];
        int tri  = (int) prv2e[jcv2e[voxel]+(int)(drand48()*ntri)]-1;
        
        triangle_randunif(tris[tri],ru);
        
        if (!tri_inside(tris[tri],ru,minbary)) {
            printf("Sampled point not inside tetrahedron. This is bad.\n");
        }
        wm = tri_which_macro_element(tris[tri],ru);
        if (wm==-1) {
            printf("Problem in whichmacroelement.\n");
        }
        
        while (wm!=voxel) {
            triangle_randunif(tris[tri],ru);
            if (!tri_inside(tris[tri],ru,minbary)) {
                printf("Sampled point not inside tetrahedron. This is bad.\n");
            }
            wm = tri_which_macro_element(tris[tri],ru);
            if (wm==-1) {
                printf("Problem in whichmacroelement.\n");
            }
        }
        std::array<double,3> pout = {ru[0],ru[1],ru[2]};
        return pout;
      
        
    }
    else {
        printf("1D species are not yet supported. dim = %d\n",dim);
        exit(-1);
    }
    
}



///* A new, faster micro2meso. */
/* We should not use this, not always accurate. */

/* NEVER USE THIS FUNCTION, DOES NOT SEEM TO WORK!! */
//int FEM_mesh::micro2meso(std::array<double,3> ppos,int vox_init,int Mspecies)//Particle *part, int Mspecies)
//{
    
    //// The connectivity matrix
////    size_t *irK,*jcK;
////    irK = mesh->irK;
////    jcK = mesh->jcK;
    
    //// mesh vetrices
////    double *p;
////    p = mesh->p;
    
    //// The particles current position
    
    ///* TODO: Clean this up. */
////    std::array<double,3> ppos = part->get_pos();
    
    //double pos[] = {ppos[0],ppos[1],ppos[2]};
    
    //// Check which vertex the particle was nearest in the previus step.
    //int pv = vox_init;//part->get_voxel();
    
    //// Find the nearest neighbour among pv's nearest neighbours in the mesh.
    //int nv;
    //int spec = 0;//part->get_type();
    //nv = nn(pos,pv,spec,Mspecies);
    ////printf("pv %i nv %i\n",pv,nv);
    
    //// Slide towards the nearest voxel.
    //while (nv != pv) {
        //pv = nv;
        //nv = nn(pos,pv,spec,Mspecies);
        ////	printf("pv %i nv %i\n",pv,nv);
        
    //}
    
    //return nv;
    
//}
//
void FEM_mesh::check_inside_surrounding_cells(double *pos, vertex *vtx, int *out)
{
    
//    tetrahedron **tets = mesh->tets;
    double minbary[1];
    int wn = -1;
    tetrahedron *tet;
    double mindist=-INFINITY;
    int closest_tet=-1;
    
    //		printf("Number of cells: %d\n",(int)(vtx->cells.size()));
    
    for (int k=0; k<(int)(vtx->cells.size()); k++) {
        //			printf("numtets=%d, k=%d\n",mesh->ntet,k);
        //			printf("index=%d\n",vtx->cells[k]);
        tet = tets[vtx->cells[k]];
        //			printf("tet=%d\n",tet);
        // We need to check if there is a non-zero edge in the connectivity matrix
        // between our vertex and the others in that tetrahedron,
        // otherwise the particle should be restricted from moving there (Not yet implemented).
        
        if (tet_inside(tet,pos,minbary)) {
            wn=tet_which_macro_element(vtx->cells[k],pos);
            break;
        }
        else {
            if (*minbary > mindist) {
                mindist = *minbary;
                closest_tet = vtx->cells[k];
            }
        }
        
    }
    
    out[0] = wn;
    out[1] = closest_tet;
    
}
//
///* "Exact" version of micro2meso. */
int FEM_mesh::micro2meso2(double *pos,int voxel)
{
    /* If a 3D species, start looking in the tetrahedra that
     covers the macroelement associated with the vertex the particle
     belonged to in the previous timestep.  */

    int wn = -1;
    tetrahedron *tet;
    double mindist=INFINITY;
    double dist;
    
    double minbary[1];
    
    vertex *vtx;
    int out[2];
    //if (particle->dim==3 || particle->dim==1) {
    
    /* TODO: All particles are 3D at the moment. Update this when we introduce membrane-bound particles. */
    
    if (1==1){//specs[particle->type].dim==3){
        
        vtx = vertices[voxel];
        
        // Check if particle is inside one of the tetrahedra surrounding the previous voxel.
        check_inside_surrounding_cells(pos, vtx, out);
        
        if (out[0] > 0){
            voxel=out[0];
            return voxel;
        }
        
        // If it is not inside one of those, we search to find the closest vertex in the mesh,
        // TODO: THIS IS JUST TEMPORARY, WE NEED TO RE-INTEGRATE THE STRATEGY WITH SLIDING FORM MICRO2MESO2
        for (int i=0; i<Ncells; i++){
            dist = distance(&p[i*3],pos);
            /*if(pos[0]<-0.3 || pos[1]<-0.3 || pos[2]<-0.3){
				std::cout << "dist=" << dist << std::endl;
				std::cout << "i=" << i << std::endl;
			
			}*/
            if (dist<mindist){
                mindist = dist;
                wn = i;
            }
        }
      /*  if(pos[2]<-0.15){
			std::cout << "pin= " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
			std::cout << "ppl= " << p[wn*3] << " " << p[wn*3+1] << " " << p[wn*3+2] << std::endl;
			std::cout << "voxel=" << wn << std::endl;
			//exit(0);
		}*/
        //
        voxel = wn;
        
        //In this case we return the voxel for which the distance between the point and the node
        //is minimized. This is an approximation, but likely good enough.
        //Sometimes particles can end up outside the domain (for instance if the domain has corners),
        //and then this approach should still give reasonably results.
        return voxel;
        
        
        // Now we check again if the point is contained in one of the tetrahedra surrounding the closest vertex.
        vtx=vertices[wn];
        
        check_inside_surrounding_cells(pos, vtx, out);
        if (out[0] > 0){
            voxel = out[0];
            return out[0];
        }
        
        // If not, we are in trouble. We now resort to looking in every tetrahedron in the mesh.
        // This is very expensive, so the timestep of the micro sovler should be chosen so that this only happens rarely.
        for (int i=0; i<ntet;i++){
            tet = tets[i];
            if (tet_inside(tet,pos,minbary)) {
				std::cout << "Warning. We should not be here." << std::endl;
				exit(0);
                wn=tet_which_macro_element(i,pos);
                break;
            }
        }
        voxel=wn;
        // At this point, this should not happen.
        if (wn == -1)
            std::cout << "Warning, did not find exact voxel for particle.\n";
        
        return voxel;
        
    }
    
    /*else if (particle->dim==2) {
     
     triangle **tris = mesh->bnd;
     voxel = particle->voxel;
     triangle *tri;
     int closest_tri;
     double *p_temp = (double *)malloc(3*sizeof(double));
     // Check if particle is inside one of the surrounding tetrahedra.
     for (k=jcv2e[voxel]; k<jcv2e[voxel+1]; k++) {
     
     tri = tris[(int)prv2e[k]-1];
     
     // We need to check if there is a non-zero edge in the connectivity matrix
     // between our vertex and the others in that tetrahedron,
     // otherwise the particle should be restricted from moving there (Not yet implemented).//
     p_temp[0] = particle->p[0];
     p_temp[1] = particle->p[1];
     p_temp[2] = particle->p[2];
     project(p_temp,tri);
     if (tri_inside(tri,p_temp,minbary)) {
     wn=tri_which_macro_element(tri,p_temp);
     free(p_temp);
     return wn;
     }
     else {
     if (*minbary > mindist) {
					mindist = *minbary;
					closest_tri = (int)prv2e[k]-1;
     }
     }
     }
     
     nv = micro2meso2(particle,mesh,Mspecies);
     
     for (k=jcv2e[nv]; k<jcv2e[nv+1]; k++) {
     
     tri = tris[(int)prv2e[k]-1];
     
     p_temp[0] = particle->p[0];
     p_temp[1] = particle->p[1];
     p_temp[2] = particle->p[2];
     project(p_temp,tri);
     if (tri_inside(tri,p_temp,minbary)){
     wn=tri_which_macro_element(tri,p_temp);
     free(p_temp);
     return wn;
     }
     else {
     if (*minbary > mindist) {
					mindist = *minbary;
					closest_tri = (int)prv2e[k]-1;
     }
     }
     
     }
     
     // If this too failed, look in the tetrahedra around the neighbouring vertices.
     int spec = particle->type;
     int dof = Mspecies*nv+spec;
     for (j=jcK[dof]; j<jcK[dof+1]; j++) {
     
     for (k=jcv2e[(irK[j]-spec)/Mspecies]; k<jcv2e[(irK[j]-spec)/Mspecies+1]; k++) {
     
     tri = tris[(int)prv2e[k]-1];
     p_temp[0] = particle->p[0];
     p_temp[1] = particle->p[1];
     p_temp[2] = particle->p[2];
     project(p_temp,tri);
     if (tri_inside(tri,p_temp,minbary)){
					wn=tri_which_macro_element(tri,p_temp);
					free(p_temp);
					return wn;
     }
     
     }
     
     }
     
     // If this failed, assign the particle to the "closest" tetrahedron,
     // as determinied by the sum of the negative components of the barycentric
     // coordinates.
     
     tri = tris[closest_tri];
     p_temp[0] = particle->p[0];
     p_temp[1] = particle->p[1];
     p_temp[2] = particle->p[2];
     project(p_temp,tri);
     wn = tri_which_macro_element(tri,p_temp);
     free(p_temp);
     return wn;
     
     }
     */
    else {
        printf("Only 3D species are supported in micro2meso.\n");
//        printf("Dimension is: %d. Particle type is: %d\n",specs[particle->type].dim,particle->type);
    }
    
    return -1;
    
	   
}







//double* FEM_mesh::tet_randunif(int *tet,double *p)
//{
//    double *ru,*x;
//    ru = (double *)malloc(3*sizeof(double));
//    ru[0]=0.0;
//    ru[1]=0.0;
//    ru[2]=0.0;
//    
//    double s,t,u,a;
//    s = drand48();
//    t = drand48();
//    u = drand48();
//    
//    if(s+t>1.0) { // cut'n fold the cube into a prism
//        
//        s = 1.0 - s;
//        t = 1.0 - t;
//        
//    }
//    if(t+u>1.0) { // cut'n fold the prism into a tetrahedron
//        
//        double tmp = u;
//        u = 1.0 - s - t;
//        t = 1.0 - tmp;
//        
//    } else if(s+t+u>1.0) {
//        
//        double tmp = u;
//        u = s + t + u - 1.0;
//        s = 1.0 - t - tmp;
//        
//    }
//    a=1.0-s-t-u; // a,s,t,u are the barycentric coordinates of the random point.
//    
//    x = &p[3*tet[0]];
//    ru[0]+=(a*x[0]);
//    ru[1]+=(a*x[1]);
//    ru[2]+=(a*x[2]);
//    
//    
//    x = &p[3*tet[1]];
//    ru[0]+=(s*x[0]);
//    ru[1]+=(s*x[1]);
//    ru[2]+=(s*x[2]);
//    
//    
//    x = &p[3*tet[2]];
//    ru[0]+=(t*x[0]);
//    ru[1]+=(t*x[1]);
//    ru[2]+=(t*x[2]);
//    
//    
//    x = &p[3*tet[3]];
//    ru[0]+=(u*x[0]);
//    ru[1]+=(u*x[1]);
//    ru[2]+=(u*x[2]);
//    
//    return ru;
//    
//}
//
///* Make sure that the vectors in the plane make up an orthonormal system. */
//void FEM_mesh::orthonormal_plane(plane *pl)
//{
//    double x[3];
//    double a,b;
//    /* First vector. */
//    
//    /* The normal vector is already normalized. Projection of normal on v1 */
//    a = dot(pl->n,pl->v1);
//    
//    x[0]=pl->v1[0]-a*pl->n[0];
//    x[1]=pl->v1[1]-a*pl->n[1];
//    x[2]=pl->v1[2]-a*pl->n[2];
//    
//    a = sqrt(dot(x,x));
//    
//    pl->v1[0]=x[0]/a;
//    pl->v1[1]=x[1]/a;
//    pl->v1[2]=x[2]/a;
//    
//    /* Other vector */
//    a = dot(pl->n,pl->v2);
//    b = dot(pl->v1,pl->v2);
//    
//    x[0]=pl->v2[0]-a*pl->n[0]-b*pl->v1[0];
//    x[1]=pl->v2[1]-a*pl->n[1]-b*pl->v1[1];
//    x[2]=pl->v2[2]-a*pl->n[2]-b*pl->v1[2];
//    
//    a = sqrt(dot(x,x));
//    
//    pl->v2[0]=x[0]/a;
//    pl->v2[1]=x[1]/a;
//    pl->v2[2]=x[2]/a;
//    
//}
//
//
void FEM_mesh::trinormal(triangle *tri,double *n)
{
    
    double a=0.0;
    double v1[3],v2[3];
    
    /* Make sure that n is zero. */
    n[0]=0.0;
    n[1]=0.0;
    n[2]=0.0;
    
    /* Vectors in plane */
    v1[0]=tri->v2[0]-tri->v1[0];
    v1[1]=tri->v2[1]-tri->v1[1];
    v1[2]=tri->v2[2]-tri->v1[2];
    
    v2[0]=tri->v3[0]-tri->v2[0];
    v2[1]=tri->v3[1]-tri->v2[1];
    v2[2]=tri->v3[2]-tri->v2[2];
    
    /* Normal: cross product of v1 and v2. */
    cross_mesh(n,v1,v2);
    
    /* normalize */
    a = sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
    n[0]/=a;
    n[1]/=a;
    n[2]/=a;
    
}
//
void FEM_mesh::vec_in_plane(double *normal, double *v1, double *v2)
{
    double r_dir[] = {normal[0]+1,2*normal[1]+2,3*normal[2]+3};
    cross_mesh(v1,normal,r_dir);
    cross_mesh(v2,normal,v1);
    double norm_n = 1.0/norm(normal);
    double norm_v1 = 1.0/norm(v1);
    double norm_v2 = 1.0/norm(v2);
    normal[0] = norm_n*normal[0];
    normal[1] = norm_n*normal[1];
    normal[2] = norm_n*normal[2];
    v1[0] = norm_v1*v1[0];
    v1[1] = norm_v1*v1[1];
    v1[2] = norm_v1*v1[2];
    v2[0] = norm_v2*v2[0];
    v2[1] = norm_v2*v2[1];
    v2[2] = norm_v2*v2[2];
}
