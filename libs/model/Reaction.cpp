#include <iostream>
#include <vector>

#include "Reaction.h"


Reaction::Reaction(std::vector<Species> reactants_in,std::vector<Species> products_in,double k_in,std::string name_in){
    
    int type_in = reactants_in.size();
    
    /* TODO: Throw error. */
    if(type_in<0 || type_in>2){throw ModelError("Type of reaction has to be >0 and <=2. (Birth process, unimolecular, or bimolecular reaction.");}
    if(k_in<0){throw ModelError("Reaction rate has to be positive.");}
    if(name_in.empty()){throw("Warning: Reaction name not specified.");}
    if(reactants_in.size()!=type_in){throw ModelError("Number of reactants does not match reaction type.");}
    
    type = (reaction_t)type_in;
    k = k_in;
    reactants = reactants_in;
    products = products_in;
    active = true; /* A reaction is active by default. It is not possible to create in inactive reaction. */
    name = name_in;
}
/*
bool Reaction::rmatch(std::vector<int> pt){
	unsigned int pts = pt.size();
	if(pts!=get_nreacs()) return false;
	
	if(pts==1){
		if(pt[0]==reactants[0]) return true;
		else return false;
	}
	else if(pts==2){
		if((pt[0]==reactants[0] && pt[1]==reactants[1]) || (pt[1]==reactants[0] && pt[0]==reactants[1])) return true
		else return false;
	}
	else{
		std::cout << "Checking for reaction with incorrect number of molecules" << std::endl;
		std::cout << "Number of molecules is: " << pts << std::endl;
		return false;
	}
}
*/

void Reaction::print(){
    std::cout << "Name: " << name << std::endl;
    std::cout << "Rate: " << k << std::endl;
    int nproducts = (int)products.size();
    try{
        if(type==0){
            std::cout << "emptyset --> ";
        }
        else if(type==1){
            std::cout << reactants[0].get_name() << " -->";
        }
        else{
            std::cout << reactants[0].get_name() << " + " << reactants[1].get_name() << " -->";
        }
        
        if(nproducts == 0){
            std::cout << " emptyset";
        }
        else{
            for(unsigned int i=0;i<nproducts;i++){
                if(i<nproducts-1)
                    std::cout << " " << products[i].get_name() << " +";
                else
                    std::cout << " " << products[i].get_name();
            }
        }
        std::cout << std::endl;
        
    }catch (const std::out_of_range& oor) {
        std::cerr << "Cannot print model (fatal error). Out of Range error: " << oor.what() << '\n';
        exit(1);
    }

    
}
