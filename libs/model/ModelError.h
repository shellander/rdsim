#ifndef RDSIM_LIBS_MODEL_ERROR
#define RDSIM_LIBS_MODEL_ERROR

#include <iostream>

class ModelError{
    
    std::string msg;
    
public:
    ModelError(){msg = "Unknown model error.";};
    ModelError(std::string msg_err){msg = msg_err;}
    void print_msg(){std::cerr << msg << std::endl;}
};




#endif