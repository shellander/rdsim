#ifndef RDSIM_LIBS_MODEL_PARTICLE
#define RDSIM_LIBS_MODEL_PARTICLE

#include <array>
#include <math.h>

#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"

#include "Species.h"
#include "Id_generator.h"

class Particle{
   
private:
    
    std::string name; /* Type of particle. */
    std::array<double,3> pos; /* Current position of particle. */
    std::array<double,3> prev_pos; /* Previous (known) position of  particle. */
    std::array<double,3> vec1; /* Basis of space in which the particle lives. */
    std::array<double,3> vec2;
    std::array<double,3> vec3;
    bool active; /* A particle can be active or inactive. */
    long unique_id; /* Unique id of particle. */
    int voxel; /* The voxel that the particle currently occupies. */
    bool updated; /* 0 if the particle diffused (RDME) during the time step, 1 otherwise. */
    
    /* Sigma and D are usually determined by the type of species. For generality, each particle carries this data, as they could be functions of the simulation trajectory. */
    double D;
    double sigma;
    int type;
    
    bool sorted_flag; /* If false not sorted, else sorted. Used for initializing system. */
    
    int dim; /* Dimension that the particle lives in. */
    int meso_micro;/* Sets the particle to either meso or micro. 1 - meso, 0 - micro. */
    double clock; /* Duration of particle's life. */
    
public:
    enum mm_t{micro=0,meso=1};
    void set_vec1(double x,double y,double z){vec1[0]=x;vec1[1]=y;vec1[2]=z;}
    void set_vec2(double x,double y,double z){vec2[0]=x;vec2[1]=y;vec2[2]=z;}
    void set_vec3(double x,double y,double z){vec3[0]=x;vec3[1]=y;vec3[2]=z;}
    Particle(Id_generator& idg){
        set_vec1(1.0,0.0,0.0);
        set_vec2(0.0,1.0,0.0);
        set_vec3(0.0,0.0,1.0);
        active=true;
        set_pos(0.0,0.0,0.0);
        voxel = 0;
        dim=3;
        sorted_flag = false;
        meso_micro = micro;
        unique_id = idg.get_id();
        updated = true;
        
    }
    
    Particle(std::array<double,3> pin,double sigma_in,double D_in,std::string namein,Id_generator& idg){
        set_vec1(1.0,0.0,0.0);
        set_vec2(0.0,1.0,0.0);
        set_vec3(0.0,0.0,1.0);
        active = true;
        set_pos(pin[0],pin[1],pin[2]);
        sigma = sigma_in;
        D = D_in;
        name = namein;
        dim=3;
        sorted_flag = false;
        meso_micro = micro;
        unique_id = idg.get_id();
        updated = true;
    }
    
    Particle(std::array<double,3> pin,Species sp,Id_generator& idg);
    
    void set_pos(double x,double y,double z){pos[0]=x;pos[1]=y;pos[2]=z;}
    void set_pos(std::array<double,3> pin){pos = pin;}
    std::array<double,3> get_pos(){
        return pos;
    }
    
    void set_updated(bool upd){updated = upd;}
    bool is_updated(){return updated;}
    
    int get_voxel(){return voxel;}
    void set_voxel(int vox_in){voxel=vox_in;}
    
    int get_dim(){return dim;}
    
    double get_D(){return D;}
    double get_sigma(){return sigma;}
    std::string get_name(){return name;}
    void set_name(std::string name_in){name=name_in;}
    
    int get_type(){return type;}
    void set_type(int type_in){type=type_in;}
    
    int get_meso_micro(){return meso_micro;}
    void set_meso_micro(int meso_micro_in){meso_micro=meso_micro_in;}
    
    double get_clock(){return clock;}
    void set_clock(double clock_in){clock=clock_in;}
    void add_to_time(double time_in){clock += time_in;}
    
    std::array<double,3> get_vec1(){return vec1;}
    std::array<double,3> get_vec2(){return vec2;}
    std::array<double,3> get_vec3(){return vec3;}
    
    long get_id(){return unique_id;}
    
    
    void print();
    
    void sorted(){sorted_flag = true;}
    void unsort(){sorted_flag = false;}
    bool issorted(){
        return sorted_flag;
    
    }
    
    void diffuse(double dt,gsl_rng *rng);
    
    
};



#endif
