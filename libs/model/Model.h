#ifndef STOCHSIM_LIBS_MODEL_MODEL
#define STOCHSIM_LIBS_MODEL_MODEL

#include <vector>
#include <unordered_map>

#include "Species.h"
#include "Reaction.h"
#include "ModelError.h"

class Model{
protected:
    std::string name;
    std::unordered_map <long,Species> specs;
    std::unordered_map <long,Reaction> reactions;
    double volume;
    
    long specs_id;
    long reaction_id;
    
    
    
public:
    
    Model(std::string name_in){
        if(name_in.empty()){throw("Warning: Model name not specified.");}
        name = name_in;
        specs_id = 0;
        reaction_id = 0;
    }
    
    static bool compare_species(std::vector<Species> s1,std::vector<Species> s2);
//    bool rmatch(std::vector<long> pids);
    Model(std::string name_in,std::vector<Reaction> reacs_in,double vol);
    
    Model(std::string name_in,std::vector<Species> specs_in,std::vector <Reaction> reacs_in,double vol);
    
//    void add_species(double D,double sigma,int init_val,int dim,std::string name);
    /* TODO: It should not be possible to add two species with same name. */
    long add_species(Species s){specs.insert({specs_id,s});++specs_id;return specs_id-1;}
    std::vector<long> add_species(std::vector<Species> s);
    
    void add_reaction(Reaction r);
    void add_reaction(std::vector<Reaction> reacs_in);
    void print();
    
    std::unordered_map<long,Species> get_species(){return specs;}
    std::unordered_map<long,Reaction> get_reactions(){return reactions;}
    
    Reaction get_reaction(long reaction_id){return reactions.at(reaction_id);}
    
    int get_nreacs(){return (int)(reactions.size());}
    int get_nspecs(){return (int)(specs.size());}
    
    double get_volume(){return volume;}
    void set_volume(double volume_in){volume=volume_in;}
    
    bool reversible(Reaction rin);
    bool reversible_bimol(Reaction rin);
    bool rmatch(std::vector<Species> spin,Reaction rin);
    double get_rev_rate(Reaction rin);
    
    std::vector<long> get_tentative_reactions(std::vector<long> pids);
    
};


#endif
