//...

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

#include "Smollinalg.h"
#include "Smol.h"
#include "RDME.h"
#include "Hybrid.h"
#include "Mesh.h"
#include "Model.h"
#include "Spatial_model.h"
#include "SSA.h"
//#include "../libs/solver/Smol.h"
#include "Geometry.h"
#include "OutputWriter.h"



#include "json.hpp"

void doSimulation(Solver *sol,std::vector <double>& sample_t,std::string file_out){
	
    
    unsigned int i=0;
	sol->initialize();
	sol->print_state();
    for(i=1;i<sample_t.size();i++){
        sol->run(sample_t[i]);
        sol->print_state();
    }
}


Spatial_model parse_model(char **argv){
    
    std::string input_file = argv[1];
    std::string urdme_model_file_name = argv[2];
    std::string mesh_file_name = argv[3];
    
    std::ifstream i(input_file);
    nlohmann::json j;
    i >> j;

    
    std::unordered_map<std::string,Species> specs_list;
    
    
    auto specs = j["species_list"];
    for(auto s=specs.begin();s!=specs.end();++s){
        auto stemp = *s;
        Species temp{stemp["diffusion_constant"],stemp["reaction_radius"],0,stemp["dimension"],stemp["name"]};
        std::string spname = stemp["name"];
        temp.set_urdme_dof_nr(stemp["urdme_dof_nr"]);
        specs_list.insert({spname,temp});
    }
    
    
    std::vector<Species> reactants;
    std::vector<Species> products;
    auto reacs = j["reaction_list"];
    
    std::vector<Reaction> reactions;
    
    for(auto r=reacs.begin();r!=reacs.end();++r){
        
        reactants.clear();
        products.clear();
        
        auto rtemp = *r;
        
        auto rnames = rtemp["reactants"];
        auto pnames = rtemp["products"];
        std::string rname_temp = rtemp["name"];
        
        
        for(auto rname=rnames.begin();rname!=rnames.end();++rname){
            std::string rname_temp = rname.key();
            int numm = rname.value();
            for(int k=0;k<numm;++k){
                reactants.push_back(specs_list.at(rname_temp));
            }
        }
        for(auto pname=pnames.begin();pname!=pnames.end();++pname){
            std::string pname_temp = pname.key();
            int numm = pname.value();
            for(int k=0;k<numm;++k){
                products.push_back(specs_list.at(pname_temp));
            }
        }
        double rate = rtemp["marate"];
        Reaction temp{reactants,products,rate,rname_temp};
        reactions.push_back(temp);
    }
    
    char *urdme_input_file = const_cast<char*>(urdme_model_file_name.c_str());
    
    Spatial_model spm{"TestModel",reactions,mesh_file_name,urdme_input_file};
    auto spl = spm.get_species();
    for(int i=0;i<spm.num_voxels();++i){
        for(auto sp=spl.begin();sp!=spl.end();++sp){
            int j = sp->second.get_urdme_dof_nr();
            for(int k=0;k<spm.get_init_voxel(j,i);++k){
                spm.initial_value_add_one(sp->first);
//                pid = spm.add_particle({0.0,0.0,0.0},sp->second);
//                spm.set_particle_voxel(pid,i);
//                pos = spm.meso2micro(pid);
//                spm.set_particle_pos(pos,pid);
//                /* Set all particles to mesoparticles. */
//                spm.set_meso_micro(Particle::meso,pid);
            }
        }
    }
    
    return spm;
}



int main(int argc,char **argv){
	
	/* The solver assumes that the first time point in urdme_model is the starting time. */
	
	/* Input:
	 * #1 model.json
	 * #2 urdmemodel.mat
	 * #3 mesh.h5
	 * #4 output.h5
     * #5 Solver (SSA, RDME, Smol, Hybrid). Default is RDME.
	 * */
	
	
    try{
		
        clock_t begin_t,end_t;
        
        std::string output_file_name = argv[4];

        Spatial_model spm = parse_model(argv);
        spm.print();
        std::vector<double> sample_t;
        sample_t = spm.get_time_points();
     
        
        int num_traj = 1;
        
        begin_t = clock();
        
        if(argc>=6){
            std::string sol_inp = argv[5];
            if(sol_inp=="RDME"){
                
//                std::cout << "starting meso sim" << std::endl;
                RDME sol{spm,output_file_name};
//                std::cout << "Initialized solver" << std::endl;
                for(int i=0;i<num_traj;++i){
                    doSimulation(&sol,sample_t,"traj"+std::to_string(i));
                }
//                std::cout << "Num diff events: " << sol.get_num_diff_events() << std::endl;
            }
            else if(sol_inp=="Smol"){

                Smol sol{spm,output_file_name};
                for(int i=0;i<num_traj;++i){
                    doSimulation(&sol,sample_t,"traj"+std::to_string(i));
                }
            }
            else if(sol_inp=="SSA"){

                SSA sol{spm,output_file_name};
                for(int i=0;i<num_traj;++i){
                    doSimulation(&sol,sample_t,"traj"+std::to_string(i));
                }
            }
            else if(sol_inp=="Hybrid"){

                Hybrid sol{spm,output_file_name};



                for(int i=0;i<num_traj;++i){
                    doSimulation(&sol,sample_t,"traj"+std::to_string(i));
                    std::cout << "Finished trajectory: " << i << std::endl;
                }
                
                sol.print_time_stats_clean();
            }
            else{
                std::cout << "Input solver does not exist. Options are SSA, RDME, Hybrid, and Smol." << std::endl;
            }
        }
        else{
            std::cout << "Solver not specified. Default is RDME." << std::endl;
            std::cout << "Input is:" << argv[5] << "." << std::endl;
            RDME sol{spm,output_file_name};
            for(int i=0;i<num_traj;++i){
                doSimulation(&sol,sample_t,"traj"+std::to_string(i));
            }

//
        }
        end_t = clock();
        std::cout << (double)(end_t-begin_t)/((double)CLOCKS_PER_SEC) << std::endl;
        
		
        

    }catch(ModelError &merr){
        merr.print_msg();
    }
    
    
    
    return 0;
}
