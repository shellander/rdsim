# README #

### RDSIM hybrid microscopic-mesoscopic hybrid solver for reaction-diffusion kinetics models ###

### Installation ###

RDSIM is designed to work as a stand alone program or as a plugin solver to PyURDME. 

#### Installing on Ubuntu 16.04 LTS (recommended) ####
Clone the repository:
    git clone http://bitbucket.org/shellander/rdsim

Build:

    cd rdsim
    mkdir build
    cd build
    cmake ..
    sudo make install
    
#### RDSIM with PyURDME ####
Install RDSIM according to the instructions above. For instructions on how to install PyURDME, and start hybrid 
simulations using the PyURDME model format, see

https://github.com/ahellander/pyurdme/tree/develop

